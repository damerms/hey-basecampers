!(function (t) {
    function e(e) {
        for (var n, i, o = e[0], s = e[1], a = 0, l = []; a < o.length; a++) (i = o[a]), Object.prototype.hasOwnProperty.call(r, i) && r[i] && l.push(r[i][0]), (r[i] = 0);
        for (n in s) Object.prototype.hasOwnProperty.call(s, n) && (t[n] = s[n]);
        for (c && c(e); l.length;) l.shift()();
    }
    var n = {},
        r = { 3: 0 };
    function i(e) {
        if (n[e]) return n[e].exports;
        var r = (n[e] = { i: e, l: !1, exports: {} });
        return t[e].call(r.exports, r, r.exports, i), (r.l = !0), r.exports;
    }
    (i.e = function (t) {
        var e = [],
            n = r[t];
        if (0 !== n)
            if (n) e.push(n[2]);
            else {
                var o = new Promise(function (e, i) {
                    n = r[t] = [e, i];
                });
                e.push((n[2] = o));
                var s,
                    a = document.createElement("script");
                (a.charset = "utf-8"),
                    (a.timeout = 120),
                    i.nc && a.setAttribute("nonce", i.nc),
                    (a.src = (function (t) {
                        return (
                            i.p +
                            "js/" +
                            ({ 0: "braintree-web", 1: "bridge", 2: "cable", 4: "rich_text", 5: "smoothscroll-polyfill" }[t] || t) +
                            "-" +
                            { 0: "582791b9ce05337a5ed4", 1: "f07b0923e76a3003416f", 2: "76f313ee2a4c65f378f7", 4: "d1b2f11e47eb10a80eb8", 5: "7277d0720b23ac1e44dc" }[t] +
                            ".chunk.js"
                        );
                    })(t));
                var c = new Error();
                s = function (e) {
                    (a.onerror = a.onload = null), clearTimeout(l);
                    var n = r[t];
                    if (0 !== n) {
                        if (n) {
                            var i = e && ("load" === e.type ? "missing" : e.type),
                                o = e && e.target && e.target.src;
                            (c.message = "Loading chunk " + t + " failed.\n(" + i + ": " + o + ")"), (c.name = "ChunkLoadError"), (c.type = i), (c.request = o), n[1](c);
                        }
                        r[t] = void 0;
                    }
                };
                var l = setTimeout(function () {
                    s({ type: "timeout", target: a });
                }, 12e4);
                (a.onerror = a.onload = s), document.head.appendChild(a);
            }
        return Promise.all(e);
    }),
        (i.m = t),
        (i.c = n),
        (i.d = function (t, e, n) {
            i.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: n });
        }),
        (i.r = function (t) {
            "undefined" !== typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(t, "__esModule", { value: !0 });
        }),
        (i.t = function (t, e) {
            if ((1 & e && (t = i(t)), 8 & e)) return t;
            if (4 & e && "object" === typeof t && t && t.__esModule) return t;
            var n = Object.create(null);
            if ((i.r(n), Object.defineProperty(n, "default", { enumerable: !0, value: t }), 2 & e && "string" != typeof t))
                for (var r in t)
                    i.d(
                        n,
                        r,
                        function (e) {
                            return t[e];
                        }.bind(null, r)
                    );
            return n;
        }),
        (i.n = function (t) {
            var e =
                t && t.__esModule
                    ? function () {
                        return t.default;
                    }
                    : function () {
                        return t;
                    };
            return i.d(e, "a", e), e;
        }),
        (i.o = function (t, e) {
            return Object.prototype.hasOwnProperty.call(t, e);
        }),
        (i.p = "https://production.haystack-assets.com/packs/"),
        (i.oe = function (t) {
            throw (console.error(t), t);
        });
    var o = (window.webpackJsonp = window.webpackJsonp || []),
        s = o.push.bind(o);
    (o.push = e), (o = o.slice());
    for (var a = 0; a < o.length; a++) e(o[a]);
    var c = s;
    i((i.s = 14));
})([
    function (t, e, n) {
        "use strict";
        n.d(e, "k", function () {
            return i;
        }),
            n.d(e, "m", function () {
                return o;
            }),
            n.d(e, "f", function () {
                return s;
            }),
            n.d(e, "d", function () {
                return a;
            }),
            n.d(e, "h", function () {
                return h;
            }),
            n.d(e, "g", function () {
                return d;
            }),
            n.d(e, "i", function () {
                return f;
            }),
            n.d(e, "j", function () {
                return m;
            }),
            n.d(e, "e", function () {
                return g;
            }),
            n.d(e, "l", function () {
                return b;
            }),
            n.d(e, "c", function () {
                return w;
            }),
            n.d(e, "b", function () {
                return E;
            }),
            n.d(e, "a", function () {
                return S;
            });
        var r = n(3);
        async function i(t, e, n) {
            const i = new r.a(t, e, n),
                o = await i.perform();
            if (!o.ok) throw new Error(o.statusCode);
            return "json" == i.responseKind ? o.json : o.text;
        }
        async function o(t, e) {
            if (d) return;
            return (await s()).subscriptions.create(t, e);
        }
        async function s() {
            const { consumer: t } = await n.e(2).then(n.bind(null, 122));
            return t;
        }
        function a(t, e = 10) {
            let n = null;
            return (...r) => {
                clearTimeout(n), (n = setTimeout(() => t.apply(this, r), e));
            };
        }
        ["get", "post", "put", "delete"].forEach((t) => {
            i[t] = (...e) => i(t, ...e);
        }),
            (i.getJSON = (t, e = {}) => i.get(t, { responseKind: "json", ...e }));
        const { userAgent: c } = window.navigator,
            l = /iPhone|iPad/.test(c),
            u = /Android/.test(c),
            h = l || u,
            d = (/Mac/.test(c), /HEY iOS/.test(c)),
            p = /Haystack Android/.test(c),
            f = d || p;
        function m() {
            return new Promise(requestAnimationFrame);
        }
        function g(t = 1) {
            return new Promise((e) => setTimeout(e, t));
        }
        const y = "scrollBehavior" in document.documentElement.style;
        async function b(t, { behavior: e = "smooth", block: r = "start", inline: i = "nearest" } = {}) {
            "smooth" != e ||
                y ||
                (await (async function () {
                    const { polyfill: t } = await n.e(5).then(n.t.bind(null, 123, 7));
                    if (v) return;
                    (v = !0), t();
                })()),
                t.scrollIntoView({ behavior: e, block: r, inline: i });
        }
        let v;
        const w = new Proxy(
            {},
            {
                get(t, e) {
                    const n = {},
                        r = "current-" + e + "-";
                    for (const { name: i, content: o } of document.head.querySelectorAll("meta[name^=" + r + "]")) {
                        n[i.slice(r.length).replace(/(?:[_-])([a-z0-9])/g, (t, e) => e.toUpperCase())] = o;
                    }
                    return n;
                },
            }
        );
        async function E(t) {
            if ("clipboard" in navigator)
                try {
                    return await navigator.clipboard.writeText(t), !0;
                } catch { }
            const e = (function (t) {
                const e = document.createElement("pre");
                return (e.style = "width: 1px; height: 1px; position: fixed; top: 50%"), (e.textContent = t), e;
            })(t);
            document.body.append(e);
            const n = (function (t) {
                const e = document.getSelection(),
                    n = document.createRange();
                return n.selectNodeContents(t), e.removeAllRanges(), e.addRange(n), document.execCommand("copy");
            })(e);
            return e.remove(), n;
        }
        const T = [null, null];
        class S {
            constructor() {
                (this.stash = (t) => {
                    this.stack.push([this.pathname, t.map((t) => (null == t ? void 0 : t.id))]);
                }),
                    (this.restore = () => {
                        const [t, e] = this.stack.pop() || T;
                        if (t && this.pathname === t) {
                            const t = e.map((t) => document.getElementById(t)).find((t) => !!t);
                            if (t) return t.focus(), t;
                        }
                    }),
                    (this.stack = []);
            }
            get pathname() {
                return window.location.pathname;
            }
        }
    },
    function (t, e, n) {
        "use strict";
        n.r(e);
        var r = n(5);
        e.default = class extends r.b {
            get classList() {
                return this.element.classList;
            }
            dispatch(t, { target: e = this.element, detail: n = {}, bubbles: r = !0, cancelable: i = !0 } = {}) {
                const o = this.identifier + ":" + t,
                    s = new CustomEvent(o, { detail: n, bubbles: r, cancelable: i });
                return e.dispatchEvent(s), s;
            }
            observeMutations(t, e = this.element, n = { childList: !0, subtree: !0 }) {
                const r = new MutationObserver((e) => {
                    r.disconnect(), Promise.resolve().then(i), t.call(this, e);
                });
                function i() {
                    e.isConnected && r.observe(e, n);
                }
                i();
            }
            get pageIsTurbolinksPreview() {
                return document.documentElement.hasAttribute("data-turbolinks-preview");
            }
        };
    },
    function (t, e, n) {
        "use strict";
        function r(t) {
            const e = document.cookie ? document.cookie.split("; ") : [],
                n = encodeURIComponent(t) + "=",
                r = e.find((t) => t.startsWith(n));
            if (r) {
                const t = r.split("=").slice(1).join("=");
                return t ? decodeURIComponent(t) : void 0;
            }
        }
        n.d(e, "a", function () {
            return r;
        }),
            n.d(e, "b", function () {
                return i;
            });
        function i(t, e) {
            const n = [t, e].map(encodeURIComponent).join("=") + "; path=/; expires=" + new Date(Date.now() + 63072e7).toUTCString();
            document.cookie = n;
        }
    },
    function (t, e, n) {
        "use strict";
        n.d(e, "a", function () {
            return o;
        });
        class r {
            constructor(t) {
                this.response = t;
            }
            get statusCode() {
                return this.response.status;
            }
            get ok() {
                return this.response.ok;
            }
            get unauthenticated() {
                return 401 == this.statusCode;
            }
            get authenticationURL() {
                return this.response.headers.get("WWW-Authenticate");
            }
            get contentType() {
                return (this.response.headers.get("Content-Type") || "").replace(/;.*$/, "");
            }
            get headers() {
                return this.response.headers;
            }
            get html() {
                return this.contentType.match(/^(application|text)\/(html|xhtml\+xml)$/) ? this.response.text() : Promise.reject('Expected an HTML response but got "' + this.contentType + '" instead');
            }
            get json() {
                return this.contentType.match(/^application\/json/) ? this.response.json() : Promise.reject('Expected a JSON response but got "' + this.contentType + '" instead');
            }
            get text() {
                return this.response.text();
            }
        }
        var i = n(2);
        window.breakAllFetchRequestsForTests = !1;
        class o {
            constructor(t, e, n = {}) {
                (this.method = t), (this.url = window.breakAllFetchRequestsForTests ? "about:blank" : e), (this.options = n);
            }
            async perform() {
                const t = new r(await fetch(this.url, this.fetchOptions));
                return t.unauthenticated && t.authenticationURL ? Promise.reject((window.location.href = t.authenticationURL)) : t;
            }
            get fetchOptions() {
                return { method: this.method, headers: this.headers, body: this.body, signal: this.signal, credentials: "same-origin", redirect: "follow" };
            }
            get headers() {
                return (function (t) {
                    const e = {};
                    for (const n in t) {
                        const r = t[n];
                        void 0 !== r && (e[n] = r);
                    }
                    return e;
                })({ "X-Requested-With": "XMLHttpRequest", "X-CSRF-Token": this.csrfToken, "Content-Type": this.contentType, Accept: this.accept });
            }
            get csrfToken() {
                var t;
                const e = null == (t = document.head.querySelector("meta[name=csrf-param]")) ? void 0 : t.content;
                return e ? Object(i.a)(e) : void 0;
            }
            get contentType() {
                return this.options.contentType ? this.options.contentType : null == this.body || this.body instanceof FormData ? void 0 : this.body instanceof File ? this.body.type : "application/octet-stream";
            }
            get accept() {
                switch (this.responseKind) {
                    case "html":
                        return "text/html, application/xhtml+xml";
                    case "json":
                        return "application/json";
                    default:
                        return "*/*";
                }
            }
            get body() {
                return this.options.body;
            }
            get responseKind() {
                return this.options.responseKind || "html";
            }
            get signal() {
                return this.options.signal;
            }
        }
    },
    function (t, e, n) {
        "use strict";
        function r(t) {
            return Array.prototype.slice.call(t);
        }
        n.r(e),
            n.d(e, "Controller", function () {
                return dt;
            }),
            n.d(e, "FetchMethod", function () {
                return g;
            }),
            n.d(e, "fetchMethodFromString", function () {
                return E;
            }),
            n.d(e, "FetchRequest", function () {
                return T;
            }),
            n.d(e, "FetchResponse", function () {
                return y;
            }),
            n.d(e, "FormSubmissionState", function () {
                return N;
            }),
            n.d(e, "FormSubmission", function () {
                return J;
            }),
            n.d(e, "Location", function () {
                return m;
            }),
            n.d(e, "TimingMetric", function () {
                return L;
            }),
            n.d(e, "VisitState", function () {
                return A;
            }),
            n.d(e, "SystemStatusCode", function () {
                return I;
            }),
            n.d(e, "Visit", function () {
                return B;
            }),
            n.d(e, "controller", function () {
                return pt;
            }),
            n.d(e, "supported", function () {
                return ft;
            }),
            n.d(e, "visit", function () {
                return mt;
            }),
            n.d(e, "clearCache", function () {
                return gt;
            }),
            n.d(e, "setProgressBarDelay", function () {
                return yt;
            }),
            n.d(e, "start", function () {
                return bt;
            });
        var i = (function () {
            var t = document.documentElement,
                e = t.matches || t.webkitMatchesSelector || t.msMatchesSelector || t.mozMatchesSelector,
                n =
                    t.closest ||
                    function (t) {
                        for (var n = this; n;) {
                            if (e.call(n, t)) return n;
                            n = n.parentElement;
                        }
                    };
            return function (t, e) {
                return n.call(t, e);
            };
        })();
        function o(t) {
            setTimeout(t, 1);
        }
        function s(t, e) {
            var n = void 0 === e ? {} : e,
                r = n.target,
                i = n.cancelable,
                o = n.data,
                s = document.createEvent("Events");
            if ((s.initEvent(t, !0, 1 == i), (s.data = o || {}), s.cancelable && !c)) {
                var a = s.preventDefault;
                s.preventDefault = function () {
                    this.defaultPrevented ||
                        Object.defineProperty(this, "defaultPrevented", {
                            get: function () {
                                return !0;
                            },
                        }),
                        a.call(this);
                };
            }
            return (r || document).dispatchEvent(s), s;
        }
        var a,
            c = ((a = document.createEvent("Events")).initEvent("test", !0, !0), a.preventDefault(), a.defaultPrevented);
        function l(t) {
            return t.replace(/^\n/, "");
        }
        function u(t, e) {
            return t.reduce(function (t, n, r) {
                return t + n + (void 0 == e[r] ? "" : e[r]);
            }, "");
        }
        function h() {
            return Array.apply(null, { length: 36 })
                .map(function (t, e) {
                    return 8 == e || 13 == e || 18 == e || 23 == e ? "-" : 14 == e ? "4" : 19 == e ? (Math.floor(4 * Math.random()) + 8).toString(16) : Math.floor(15 * Math.random()).toString(16);
                })
                .join("");
        }
        var d,
            p = function (t, e) {
                return Object.defineProperty ? Object.defineProperty(t, "raw", { value: e }) : (t.raw = e), t;
            },
            f = (function () {
                function t() {
                    var t = this;
                    (this.hiding = !1),
                        (this.value = 0),
                        (this.visible = !1),
                        (this.trickle = function () {
                            t.setValue(t.value + Math.random() / 100);
                        }),
                        (this.stylesheetElement = this.createStylesheetElement()),
                        (this.progressElement = this.createProgressElement()),
                        this.installStylesheetElement(),
                        this.setValue(0);
                }
                return (
                    Object.defineProperty(t, "defaultCSS", {
                        get: function () {
                            return (function (t) {
                                for (var e = [], n = 1; n < arguments.length; n++) e[n - 1] = arguments[n];
                                var r = l(u(t, e)).split("\n"),
                                    i = r[0].match(/^\s+/),
                                    o = i ? i[0].length : 0;
                                return r
                                    .map(function (t) {
                                        return t.slice(o);
                                    })
                                    .join("\n");
                            })(
                                d ||
                                (d = p(
                                    [
                                        "\n      .turbolinks-progress-bar {\n        position: fixed;\n        display: block;\n        top: 0;\n        left: 0;\n        height: 3px;\n        background: #0076ff;\n        z-index: 9999;\n        transition:\n          width ",
                                        "ms ease-out,\n          opacity ",
                                        "ms ",
                                        "ms ease-in;\n        transform: translate3d(0, 0, 0);\n      }\n    ",
                                    ],
                                    [
                                        "\n      .turbolinks-progress-bar {\n        position: fixed;\n        display: block;\n        top: 0;\n        left: 0;\n        height: 3px;\n        background: #0076ff;\n        z-index: 9999;\n        transition:\n          width ",
                                        "ms ease-out,\n          opacity ",
                                        "ms ",
                                        "ms ease-in;\n        transform: translate3d(0, 0, 0);\n      }\n    ",
                                    ]
                                )),
                                t.animationDuration,
                                t.animationDuration / 2,
                                t.animationDuration / 2
                            );
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    (t.prototype.show = function () {
                        this.visible || ((this.visible = !0), this.installProgressElement(), this.startTrickling());
                    }),
                    (t.prototype.hide = function () {
                        var t = this;
                        this.visible &&
                            !this.hiding &&
                            ((this.hiding = !0),
                                this.fadeProgressElement(function () {
                                    t.uninstallProgressElement(), t.stopTrickling(), (t.visible = !1), (t.hiding = !1);
                                }));
                    }),
                    (t.prototype.setValue = function (t) {
                        (this.value = t), this.refresh();
                    }),
                    (t.prototype.installStylesheetElement = function () {
                        document.head.insertBefore(this.stylesheetElement, document.head.firstChild);
                    }),
                    (t.prototype.installProgressElement = function () {
                        (this.progressElement.style.width = "0"), (this.progressElement.style.opacity = "1"), document.documentElement.insertBefore(this.progressElement, document.body), this.refresh();
                    }),
                    (t.prototype.fadeProgressElement = function (e) {
                        (this.progressElement.style.opacity = "0"), setTimeout(e, 1.5 * t.animationDuration);
                    }),
                    (t.prototype.uninstallProgressElement = function () {
                        this.progressElement.parentNode && document.documentElement.removeChild(this.progressElement);
                    }),
                    (t.prototype.startTrickling = function () {
                        this.trickleInterval || (this.trickleInterval = window.setInterval(this.trickle, t.animationDuration));
                    }),
                    (t.prototype.stopTrickling = function () {
                        window.clearInterval(this.trickleInterval), delete this.trickleInterval;
                    }),
                    (t.prototype.refresh = function () {
                        var t = this;
                        requestAnimationFrame(function () {
                            t.progressElement.style.width = 10 + 90 * t.value + "%";
                        });
                    }),
                    (t.prototype.createStylesheetElement = function () {
                        var e = document.createElement("style");
                        return (e.type = "text/css"), (e.textContent = t.defaultCSS), e;
                    }),
                    (t.prototype.createProgressElement = function () {
                        var t = document.createElement("div");
                        return (t.className = "turbolinks-progress-bar"), t;
                    }),
                    (t.animationDuration = 300),
                    t
                );
            })(),
            m = (function () {
                function t(t) {
                    var e = document.createElement("a");
                    (e.href = t), (this.absoluteURL = e.href);
                    var n = e.hash.length;
                    n < 2 ? (this.requestURL = this.absoluteURL) : ((this.requestURL = this.absoluteURL.slice(0, -n)), (this.anchor = e.hash.slice(1)));
                }
                return (
                    Object.defineProperty(t, "currentLocation", {
                        get: function () {
                            return this.wrap(window.location.toString());
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    (t.wrap = function (t) {
                        return "string" == typeof t ? new this(t) : null != t ? t : void 0;
                    }),
                    (t.prototype.getOrigin = function () {
                        return this.absoluteURL.split("/", 3).join("/");
                    }),
                    (t.prototype.getPath = function () {
                        return (this.requestURL.match(/\/\/[^/]*(\/[^?;]*)/) || [])[1] || "/";
                    }),
                    (t.prototype.getPathComponents = function () {
                        return this.getPath().split("/").slice(1);
                    }),
                    (t.prototype.getLastPathComponent = function () {
                        return this.getPathComponents().slice(-1)[0];
                    }),
                    (t.prototype.getExtension = function () {
                        return (this.getLastPathComponent().match(/\.[^.]*$/) || [])[0] || "";
                    }),
                    (t.prototype.isHTML = function () {
                        return !!this.getExtension().match(/^(?:|\.(?:htm|html|xhtml))$/);
                    }),
                    (t.prototype.isPrefixedBy = function (t) {
                        var e,
                            n,
                            r = (function (t) {
                                return (e = t.getOrigin() + t.getPath()), (n = e), (r = "/"), n.slice(-r.length) === r ? e : e + "/";
                                var e, n, r;
                            })(t);
                        return this.isEqualTo(t) || ((e = this.absoluteURL), (n = r), e.slice(0, n.length) === n);
                    }),
                    (t.prototype.isEqualTo = function (t) {
                        return t && this.absoluteURL === t.absoluteURL;
                    }),
                    (t.prototype.toCacheKey = function () {
                        return this.requestURL;
                    }),
                    (t.prototype.toJSON = function () {
                        return this.absoluteURL;
                    }),
                    (t.prototype.toString = function () {
                        return this.absoluteURL;
                    }),
                    (t.prototype.valueOf = function () {
                        return this.absoluteURL;
                    }),
                    t
                );
            })();
        var g,
            y = (function () {
                function t(t) {
                    this.response = t;
                }
                return (
                    Object.defineProperty(t.prototype, "succeeded", {
                        get: function () {
                            return this.response.ok;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "failed", {
                        get: function () {
                            return !this.succeeded;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "redirected", {
                        get: function () {
                            return this.response.redirected;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "location", {
                        get: function () {
                            return m.wrap(this.response.url);
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "isHTML", {
                        get: function () {
                            return this.contentType && this.contentType.match(/^text\/html|^application\/xhtml\+xml/);
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "statusCode", {
                        get: function () {
                            return this.response.status;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "contentType", {
                        get: function () {
                            return this.header("Content-Type");
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "responseText", {
                        get: function () {
                            return this.response.text();
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "responseHTML", {
                        get: function () {
                            return this.isHTML ? this.response.text() : Promise.resolve(void 0);
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    (t.prototype.header = function (t) {
                        return this.response.headers.get(t);
                    }),
                    t
                );
            })(),
            b = function () {
                return (b =
                    Object.assign ||
                    function (t) {
                        for (var e, n = 1, r = arguments.length; n < r; n++) for (var i in (e = arguments[n])) Object.prototype.hasOwnProperty.call(e, i) && (t[i] = e[i]);
                        return t;
                    }).apply(this, arguments);
            },
            v = function (t, e, n, r) {
                return new (n || (n = Promise))(function (i, o) {
                    function s(t) {
                        try {
                            c(r.next(t));
                        } catch (e) {
                            o(e);
                        }
                    }
                    function a(t) {
                        try {
                            c(r.throw(t));
                        } catch (e) {
                            o(e);
                        }
                    }
                    function c(t) {
                        var e;
                        t.done
                            ? i(t.value)
                            : ((e = t.value),
                                e instanceof n
                                    ? e
                                    : new n(function (t) {
                                        t(e);
                                    })).then(s, a);
                    }
                    c((r = r.apply(t, e || [])).next());
                });
            },
            w = function (t, e) {
                var n,
                    r,
                    i,
                    o,
                    s = {
                        label: 0,
                        sent: function () {
                            if (1 & i[0]) throw i[1];
                            return i[1];
                        },
                        trys: [],
                        ops: [],
                    };
                return (
                    (o = { next: a(0), throw: a(1), return: a(2) }),
                    "function" === typeof Symbol &&
                    (o[Symbol.iterator] = function () {
                        return this;
                    }),
                    o
                );
                function a(o) {
                    return function (a) {
                        return (function (o) {
                            if (n) throw new TypeError("Generator is already executing.");
                            for (; s;)
                                try {
                                    if (((n = 1), r && (i = 2 & o[0] ? r.return : o[0] ? r.throw || ((i = r.return) && i.call(r), 0) : r.next) && !(i = i.call(r, o[1])).done)) return i;
                                    switch (((r = 0), i && (o = [2 & o[0], i.value]), o[0])) {
                                        case 0:
                                        case 1:
                                            i = o;
                                            break;
                                        case 4:
                                            return s.label++, { value: o[1], done: !1 };
                                        case 5:
                                            s.label++, (r = o[1]), (o = [0]);
                                            continue;
                                        case 7:
                                            (o = s.ops.pop()), s.trys.pop();
                                            continue;
                                        default:
                                            if (!(i = (i = s.trys).length > 0 && i[i.length - 1]) && (6 === o[0] || 2 === o[0])) {
                                                s = 0;
                                                continue;
                                            }
                                            if (3 === o[0] && (!i || (o[1] > i[0] && o[1] < i[3]))) {
                                                s.label = o[1];
                                                break;
                                            }
                                            if (6 === o[0] && s.label < i[1]) {
                                                (s.label = i[1]), (i = o);
                                                break;
                                            }
                                            if (i && s.label < i[2]) {
                                                (s.label = i[2]), s.ops.push(o);
                                                break;
                                            }
                                            i[2] && s.ops.pop(), s.trys.pop();
                                            continue;
                                    }
                                    o = e.call(t, s);
                                } catch (a) {
                                    (o = [6, a]), (r = 0);
                                } finally {
                                    n = i = 0;
                                }
                            if (5 & o[0]) throw o[1];
                            return { value: o[0] ? o[1] : void 0, done: !0 };
                        })([o, a]);
                    };
                }
            };
        function E(t) {
            switch (t.toLowerCase()) {
                case "get":
                    return g.get;
                case "post":
                    return g.post;
                case "put":
                    return g.put;
                case "patch":
                    return g.patch;
                case "delete":
                    return g.delete;
            }
        }
        !(function (t) {
            (t[(t.get = 0)] = "get"), (t[(t.post = 1)] = "post"), (t[(t.put = 2)] = "put"), (t[(t.patch = 3)] = "patch"), (t[(t.delete = 4)] = "delete");
        })(g || (g = {}));
        var T = (function () {
            function t(t, e, n, r) {
                (this.abortController = new AbortController()), (this.delegate = t), (this.method = e), (this.location = n), (this.body = r);
            }
            return (
                Object.defineProperty(t.prototype, "url", {
                    get: function () {
                        var t = this.location.absoluteURL,
                            e = this.params.toString();
                        return this.isIdempotent && e.length ? [t, e].join(t.includes("?") ? "&" : "?") : t;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "params", {
                    get: function () {
                        return this.entries.reduce(function (t, e) {
                            var n = e[0],
                                r = e[1];
                            return t.append(n, r.toString()), t;
                        }, new URLSearchParams());
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "entries", {
                    get: function () {
                        return this.body ? Array.from(this.body.entries()) : [];
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                (t.prototype.cancel = function () {
                    this.abortController.abort();
                }),
                (t.prototype.perform = function () {
                    return v(this, void 0, void 0, function () {
                        var t, e, n;
                        return w(this, function (r) {
                            switch (r.label) {
                                case 0:
                                    s("turbolinks:before-fetch-request", { data: { fetchOptions: (t = this.fetchOptions) } }), (r.label = 1);
                                case 1:
                                    return r.trys.push([1, 4, 5, 6]), this.delegate.requestStarted(this), [4, fetch(this.url, t)];
                                case 2:
                                    return (e = r.sent()), [4, this.receive(e)];
                                case 3:
                                    return [2, r.sent()];
                                case 4:
                                    throw ((n = r.sent()), this.delegate.requestErrored(this, n), n);
                                case 5:
                                    return this.delegate.requestFinished(this), [7];
                                case 6:
                                    return [2];
                            }
                        });
                    });
                }),
                (t.prototype.receive = function (t) {
                    return v(this, void 0, void 0, function () {
                        var e;
                        return w(this, function (n) {
                            return (
                                (e = new y(t)),
                                s("turbolinks:before-fetch-response", { cancelable: !0, data: { fetchResponse: e } }).defaultPrevented
                                    ? this.delegate.requestPreventedHandlingResponse(this, e)
                                    : e.succeeded
                                        ? this.delegate.requestSucceededWithResponse(this, e)
                                        : this.delegate.requestFailedWithResponse(this, e),
                                [2, e]
                            );
                        });
                    });
                }),
                Object.defineProperty(t.prototype, "fetchOptions", {
                    get: function () {
                        return { method: g[this.method].toUpperCase(), credentials: "same-origin", headers: this.headers, redirect: "follow", body: this.isIdempotent ? void 0 : this.body, signal: this.abortSignal };
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "isIdempotent", {
                    get: function () {
                        return this.method == g.get;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "headers", {
                    get: function () {
                        return b({ Accept: "text/html, application/xhtml+xml" }, this.additionalHeaders);
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "additionalHeaders", {
                    get: function () {
                        return "function" == typeof this.delegate.additionalHeadersForRequest ? this.delegate.additionalHeadersForRequest(this) : {};
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "abortSignal", {
                    get: function () {
                        return this.abortController.signal;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                t
            );
        })(),
            S = function () {
                return (S =
                    Object.assign ||
                    function (t) {
                        for (var e, n = 1, r = arguments.length; n < r; n++) for (var i in (e = arguments[n])) Object.prototype.hasOwnProperty.call(e, i) && (t[i] = e[i]);
                        return t;
                    }).apply(this, arguments);
            },
            C = function () {
                for (var t = 0, e = 0, n = arguments.length; e < n; e++) t += arguments[e].length;
                var r = Array(t),
                    i = 0;
                for (e = 0; e < n; e++) for (var o = arguments[e], s = 0, a = o.length; s < a; s++, i++) r[i] = o[s];
                return r;
            },
            k = (function () {
                function t(t) {
                    this.detailsByOuterHTML = t.reduce(function (t, e) {
                        var n,
                            r = e.outerHTML,
                            i = r in t ? t[r] : { type: x(e), tracked: O(e), elements: [] };
                        return S(S({}, t), (((n = {})[r] = S(S({}, i), { elements: C(i.elements, [e]) })), n));
                    }, {});
                }
                return (
                    (t.fromHeadElement = function (t) {
                        return new this(t ? r(t.children) : []);
                    }),
                    (t.prototype.getTrackedElementSignature = function () {
                        var t = this;
                        return Object.keys(this.detailsByOuterHTML)
                            .filter(function (e) {
                                return t.detailsByOuterHTML[e].tracked;
                            })
                            .join("");
                    }),
                    (t.prototype.getScriptElementsNotInDetails = function (t) {
                        return this.getElementsMatchingTypeNotInDetails("script", t);
                    }),
                    (t.prototype.getStylesheetElementsNotInDetails = function (t) {
                        return this.getElementsMatchingTypeNotInDetails("stylesheet", t);
                    }),
                    (t.prototype.getElementsMatchingTypeNotInDetails = function (t, e) {
                        var n = this;
                        return Object.keys(this.detailsByOuterHTML)
                            .filter(function (t) {
                                return !(t in e.detailsByOuterHTML);
                            })
                            .map(function (t) {
                                return n.detailsByOuterHTML[t];
                            })
                            .filter(function (e) {
                                return e.type == t;
                            })
                            .map(function (t) {
                                return t.elements[0];
                            });
                    }),
                    (t.prototype.getProvisionalElements = function () {
                        var t = this;
                        return Object.keys(this.detailsByOuterHTML).reduce(function (e, n) {
                            var r = t.detailsByOuterHTML[n],
                                i = r.type,
                                o = r.tracked,
                                s = r.elements;
                            return null != i || o ? (s.length > 1 ? C(e, s.slice(1)) : e) : C(e, s);
                        }, []);
                    }),
                    (t.prototype.getMetaValue = function (t) {
                        var e = this.findMetaElementByName(t);
                        return e ? e.getAttribute("content") : null;
                    }),
                    (t.prototype.findMetaElementByName = function (t) {
                        var e = this;
                        return Object.keys(this.detailsByOuterHTML).reduce(function (n, r) {
                            var i = e.detailsByOuterHTML[r].elements[0];
                            return (function (t, e) {
                                return "meta" == t.tagName.toLowerCase() && t.getAttribute("name") == e;
                            })(i, t)
                                ? i
                                : n;
                        }, void 0);
                    }),
                    t
                );
            })();
        function x(t) {
            return (function (t) {
                return "script" == t.tagName.toLowerCase();
            })(t)
                ? "script"
                : (function (t) {
                    var e = t.tagName.toLowerCase();
                    return "style" == e || ("link" == e && "stylesheet" == t.getAttribute("rel"));
                })(t)
                    ? "stylesheet"
                    : void 0;
        }
        function O(t) {
            return "reload" == t.getAttribute("data-turbolinks-track");
        }
        var L,
            A,
            P = (function () {
                function t(t, e) {
                    (this.headDetails = t), (this.bodyElement = e);
                }
                return (
                    (t.wrap = function (t) {
                        return t instanceof this ? t : "string" == typeof t ? this.fromHTMLString(t) : this.fromHTMLElement(t);
                    }),
                    (t.fromHTMLString = function (t) {
                        var e = new DOMParser().parseFromString(t, "text/html").documentElement;
                        return this.fromHTMLElement(e);
                    }),
                    (t.fromHTMLElement = function (t) {
                        var e = t.querySelector("head"),
                            n = t.querySelector("body") || document.createElement("body");
                        return new this(k.fromHeadElement(e), n);
                    }),
                    (t.prototype.clone = function () {
                        var e = t.fromHTMLString(this.bodyElement.outerHTML).bodyElement;
                        return new t(this.headDetails, e);
                    }),
                    (t.prototype.getRootLocation = function () {
                        var t = this.getSetting("root", "/");
                        return new m(t);
                    }),
                    (t.prototype.getCacheControlValue = function () {
                        return this.getSetting("cache-control");
                    }),
                    (t.prototype.getElementForAnchor = function (t) {
                        try {
                            return this.bodyElement.querySelector("[id='" + t + "'], a[name='" + t + "']");
                        } catch (e) {
                            return null;
                        }
                    }),
                    (t.prototype.getPermanentElements = function () {
                        return r(this.bodyElement.querySelectorAll("[id][data-turbolinks-permanent]"));
                    }),
                    (t.prototype.getPermanentElementById = function (t) {
                        return this.bodyElement.querySelector("#" + t + "[data-turbolinks-permanent]");
                    }),
                    (t.prototype.getPermanentElementsPresentInSnapshot = function (t) {
                        return this.getPermanentElements().filter(function (e) {
                            var n = e.id;
                            return t.getPermanentElementById(n);
                        });
                    }),
                    (t.prototype.findFirstAutofocusableElement = function () {
                        return this.bodyElement.querySelector("[autofocus]");
                    }),
                    (t.prototype.hasAnchor = function (t) {
                        return null != this.getElementForAnchor(t);
                    }),
                    (t.prototype.isPreviewable = function () {
                        return "no-preview" != this.getCacheControlValue();
                    }),
                    (t.prototype.isCacheable = function () {
                        return "no-cache" != this.getCacheControlValue();
                    }),
                    (t.prototype.isVisitable = function () {
                        return "reload" != this.getSetting("visit-control");
                    }),
                    (t.prototype.getSetting = function (t, e) {
                        var n = this.headDetails.getMetaValue("turbolinks-" + t);
                        return null == n ? e : n;
                    }),
                    t
                );
            })(),
            F = function () {
                return (F =
                    Object.assign ||
                    function (t) {
                        for (var e, n = 1, r = arguments.length; n < r; n++) for (var i in (e = arguments[n])) Object.prototype.hasOwnProperty.call(e, i) && (t[i] = e[i]);
                        return t;
                    }).apply(this, arguments);
            },
            j = function (t, e, n, r) {
                return new (n || (n = Promise))(function (i, o) {
                    function s(t) {
                        try {
                            c(r.next(t));
                        } catch (e) {
                            o(e);
                        }
                    }
                    function a(t) {
                        try {
                            c(r.throw(t));
                        } catch (e) {
                            o(e);
                        }
                    }
                    function c(t) {
                        var e;
                        t.done
                            ? i(t.value)
                            : ((e = t.value),
                                e instanceof n
                                    ? e
                                    : new n(function (t) {
                                        t(e);
                                    })).then(s, a);
                    }
                    c((r = r.apply(t, e || [])).next());
                });
            },
            M = function (t, e) {
                var n,
                    r,
                    i,
                    o,
                    s = {
                        label: 0,
                        sent: function () {
                            if (1 & i[0]) throw i[1];
                            return i[1];
                        },
                        trys: [],
                        ops: [],
                    };
                return (
                    (o = { next: a(0), throw: a(1), return: a(2) }),
                    "function" === typeof Symbol &&
                    (o[Symbol.iterator] = function () {
                        return this;
                    }),
                    o
                );
                function a(o) {
                    return function (a) {
                        return (function (o) {
                            if (n) throw new TypeError("Generator is already executing.");
                            for (; s;)
                                try {
                                    if (((n = 1), r && (i = 2 & o[0] ? r.return : o[0] ? r.throw || ((i = r.return) && i.call(r), 0) : r.next) && !(i = i.call(r, o[1])).done)) return i;
                                    switch (((r = 0), i && (o = [2 & o[0], i.value]), o[0])) {
                                        case 0:
                                        case 1:
                                            i = o;
                                            break;
                                        case 4:
                                            return s.label++, { value: o[1], done: !1 };
                                        case 5:
                                            s.label++, (r = o[1]), (o = [0]);
                                            continue;
                                        case 7:
                                            (o = s.ops.pop()), s.trys.pop();
                                            continue;
                                        default:
                                            if (!(i = (i = s.trys).length > 0 && i[i.length - 1]) && (6 === o[0] || 2 === o[0])) {
                                                s = 0;
                                                continue;
                                            }
                                            if (3 === o[0] && (!i || (o[1] > i[0] && o[1] < i[3]))) {
                                                s.label = o[1];
                                                break;
                                            }
                                            if (6 === o[0] && s.label < i[1]) {
                                                (s.label = i[1]), (i = o);
                                                break;
                                            }
                                            if (i && s.label < i[2]) {
                                                (s.label = i[2]), s.ops.push(o);
                                                break;
                                            }
                                            i[2] && s.ops.pop(), s.trys.pop();
                                            continue;
                                    }
                                    o = e.call(t, s);
                                } catch (a) {
                                    (o = [6, a]), (r = 0);
                                } finally {
                                    n = i = 0;
                                }
                            if (5 & o[0]) throw o[1];
                            return { value: o[0] ? o[1] : void 0, done: !0 };
                        })([o, a]);
                    };
                }
            };
        !(function (t) {
            (t.visitStart = "visitStart"), (t.requestStart = "requestStart"), (t.requestEnd = "requestEnd"), (t.visitEnd = "visitEnd");
        })(L || (L = {})),
            (function (t) {
                (t.initialized = "initialized"), (t.started = "started"), (t.canceled = "canceled"), (t.failed = "failed"), (t.completed = "completed");
            })(A || (A = {}));
        var I,
            R = { action: "advance", historyChanged: !1 };
        !(function (t) {
            (t[(t.networkFailure = 0)] = "networkFailure"), (t[(t.timeoutFailure = -1)] = "timeoutFailure"), (t[(t.contentTypeMismatch = -2)] = "contentTypeMismatch");
        })(I || (I = {}));
        var B = (function () {
            function t(t, e, n, r) {
                var i = this;
                void 0 === r && (r = {}),
                    (this.identifier = h()),
                    (this.timingMetrics = {}),
                    (this.followedRedirect = !1),
                    (this.historyChanged = !1),
                    (this.scrolled = !1),
                    (this.snapshotCached = !1),
                    (this.state = A.initialized),
                    (this.performScroll = function () {
                        i.scrolled || ("restore" == i.action ? i.scrollToRestoredPosition() || i.scrollToTop() : i.scrollToAnchor() || i.scrollToTop(), (i.scrolled = !0));
                    }),
                    (this.delegate = t),
                    (this.location = e),
                    (this.restorationIdentifier = n || h());
                var o = F(F({}, R), r),
                    s = o.action,
                    a = o.historyChanged,
                    c = o.referrer,
                    l = o.snapshotHTML,
                    u = o.response;
                (this.action = s), (this.historyChanged = a), (this.referrer = c), (this.snapshotHTML = l), (this.response = u);
            }
            return (
                Object.defineProperty(t.prototype, "adapter", {
                    get: function () {
                        return this.delegate.adapter;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "view", {
                    get: function () {
                        return this.delegate.view;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "history", {
                    get: function () {
                        return this.delegate.history;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "restorationData", {
                    get: function () {
                        return this.history.getRestorationDataForIdentifier(this.restorationIdentifier);
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                (t.prototype.start = function () {
                    this.state == A.initialized && (this.recordTimingMetric(L.visitStart), (this.state = A.started), this.adapter.visitStarted(this), this.delegate.visitStarted(this));
                }),
                (t.prototype.cancel = function () {
                    this.state == A.started && (this.request && this.request.cancel(), this.cancelRender(), (this.state = A.canceled));
                }),
                (t.prototype.complete = function () {
                    this.state == A.started && (this.recordTimingMetric(L.visitEnd), (this.state = A.completed), this.adapter.visitCompleted(this), this.delegate.visitCompleted(this));
                }),
                (t.prototype.fail = function () {
                    this.state == A.started && ((this.state = A.failed), this.adapter.visitFailed(this));
                }),
                (t.prototype.changeHistory = function () {
                    if (!this.historyChanged) {
                        var t = this.location.isEqualTo(this.referrer) ? "replace" : this.action,
                            e = this.getHistoryMethodForAction(t);
                        this.history.update(e, this.location, this.restorationIdentifier), (this.historyChanged = !0);
                    }
                }),
                (t.prototype.issueRequest = function () {
                    this.hasPreloadedResponse() ? this.simulateRequest() : this.shouldIssueRequest() && !this.request && ((this.request = new T(this, g.get, this.location)), this.request.perform());
                }),
                (t.prototype.simulateRequest = function () {
                    this.response && (this.startRequest(), this.recordResponse(), this.finishRequest());
                }),
                (t.prototype.startRequest = function () {
                    this.recordTimingMetric(L.requestStart), this.adapter.visitRequestStarted(this);
                }),
                (t.prototype.recordResponse = function (t) {
                    if ((void 0 === t && (t = this.response), (this.response = t), t)) {
                        var e = t.statusCode;
                        D(e) ? this.adapter.visitRequestCompleted(this) : this.adapter.visitRequestFailedWithStatusCode(this, e);
                    }
                }),
                (t.prototype.finishRequest = function () {
                    this.recordTimingMetric(L.requestEnd), this.adapter.visitRequestFinished(this);
                }),
                (t.prototype.loadResponse = function () {
                    var t = this;
                    if (this.response) {
                        var e = this.response,
                            n = e.statusCode,
                            r = e.responseHTML;
                        this.render(function () {
                            t.cacheSnapshot(),
                                D(n) && null != r
                                    ? (t.view.render({ snapshot: P.fromHTMLString(r) }, t.performScroll), t.adapter.visitRendered(t), t.complete())
                                    : (t.view.render({ error: r }, t.performScroll), t.adapter.visitRendered(t), t.fail());
                        });
                    }
                }),
                (t.prototype.getCachedSnapshot = function () {
                    var t = this.view.getCachedSnapshotForLocation(this.location) || this.getPreloadedSnapshot();
                    if (t && (!this.location.anchor || t.hasAnchor(this.location.anchor)) && ("restore" == this.action || t.isPreviewable())) return t;
                }),
                (t.prototype.getPreloadedSnapshot = function () {
                    if (this.snapshotHTML) return P.wrap(this.snapshotHTML);
                }),
                (t.prototype.hasCachedSnapshot = function () {
                    return null != this.getCachedSnapshot();
                }),
                (t.prototype.loadCachedSnapshot = function () {
                    var t = this,
                        e = this.getCachedSnapshot();
                    if (e) {
                        var n = this.shouldIssueRequest();
                        this.render(function () {
                            t.cacheSnapshot(), t.view.render({ snapshot: e, isPreview: n }, t.performScroll), t.adapter.visitRendered(t), n || t.complete();
                        });
                    }
                }),
                (t.prototype.followRedirect = function () {
                    this.redirectedToLocation && !this.followedRedirect && ((this.location = this.redirectedToLocation), this.history.replace(this.redirectedToLocation, this.restorationIdentifier), (this.followedRedirect = !0));
                }),
                (t.prototype.requestStarted = function () {
                    this.startRequest();
                }),
                (t.prototype.requestPreventedHandlingResponse = function (t, e) { }),
                (t.prototype.requestSucceededWithResponse = function (t, e) {
                    return j(this, void 0, void 0, function () {
                        var t;
                        return M(this, function (n) {
                            switch (n.label) {
                                case 0:
                                    return [4, e.responseHTML];
                                case 1:
                                    return (
                                        void 0 == (t = n.sent())
                                            ? this.recordResponse({ statusCode: I.contentTypeMismatch })
                                            : ((this.redirectedToLocation = e.redirected ? e.location : void 0), this.recordResponse({ statusCode: e.statusCode, responseHTML: t })),
                                        [2]
                                    );
                            }
                        });
                    });
                }),
                (t.prototype.requestFailedWithResponse = function (t, e) {
                    return j(this, void 0, void 0, function () {
                        var t;
                        return M(this, function (n) {
                            switch (n.label) {
                                case 0:
                                    return [4, e.responseHTML];
                                case 1:
                                    return void 0 == (t = n.sent()) ? this.recordResponse({ statusCode: I.contentTypeMismatch }) : this.recordResponse({ statusCode: e.statusCode, responseHTML: t }), [2];
                            }
                        });
                    });
                }),
                (t.prototype.requestErrored = function (t, e) {
                    this.recordResponse({ statusCode: I.networkFailure });
                }),
                (t.prototype.requestFinished = function () {
                    this.finishRequest();
                }),
                (t.prototype.scrollToRestoredPosition = function () {
                    var t = this.restorationData.scrollPosition;
                    if (t) return this.view.scrollToPosition(t), !0;
                }),
                (t.prototype.scrollToAnchor = function () {
                    if (null != this.location.anchor) return this.view.scrollToAnchor(this.location.anchor), !0;
                }),
                (t.prototype.scrollToTop = function () {
                    this.view.scrollToPosition({ x: 0, y: 0 });
                }),
                (t.prototype.recordTimingMetric = function (t) {
                    this.timingMetrics[t] = new Date().getTime();
                }),
                (t.prototype.getTimingMetrics = function () {
                    return F({}, this.timingMetrics);
                }),
                (t.prototype.getHistoryMethodForAction = function (t) {
                    switch (t) {
                        case "replace":
                            return history.replaceState;
                        case "advance":
                        case "restore":
                            return history.pushState;
                    }
                }),
                (t.prototype.hasPreloadedResponse = function () {
                    return "object" == typeof this.response;
                }),
                (t.prototype.shouldIssueRequest = function () {
                    return "restore" != this.action || !this.hasCachedSnapshot();
                }),
                (t.prototype.cacheSnapshot = function () {
                    this.snapshotCached || (this.view.cacheSnapshot(), (this.snapshotCached = !0));
                }),
                (t.prototype.render = function (t) {
                    var e = this;
                    this.cancelRender(),
                        (this.frame = requestAnimationFrame(function () {
                            delete e.frame, t.call(e);
                        }));
                }),
                (t.prototype.cancelRender = function () {
                    this.frame && (cancelAnimationFrame(this.frame), delete this.frame);
                }),
                t
            );
        })();
        function D(t) {
            return t >= 200 && t < 300;
        }
        var N,
            V = (function () {
                function t(t) {
                    var e = this;
                    (this.progressBar = new f()),
                        (this.showProgressBar = function () {
                            e.progressBar.show();
                        }),
                        (this.controller = t);
                }
                return (
                    (t.prototype.visitProposedToLocation = function (t, e) {
                        var n = h();
                        this.controller.startVisitToLocation(t, n, e);
                    }),
                    (t.prototype.visitStarted = function (t) {
                        t.issueRequest(), t.changeHistory(), t.loadCachedSnapshot();
                    }),
                    (t.prototype.visitRequestStarted = function (t) {
                        this.progressBar.setValue(0), t.hasCachedSnapshot() || "restore" != t.action ? this.showProgressBarAfterDelay() : this.showProgressBar();
                    }),
                    (t.prototype.visitRequestCompleted = function (t) {
                        t.loadResponse();
                    }),
                    (t.prototype.visitRequestFailedWithStatusCode = function (t, e) {
                        switch (e) {
                            case I.networkFailure:
                            case I.timeoutFailure:
                            case I.contentTypeMismatch:
                                return this.reload();
                            default:
                                return t.loadResponse();
                        }
                    }),
                    (t.prototype.visitRequestFinished = function (t) {
                        this.progressBar.setValue(1), this.hideProgressBar();
                    }),
                    (t.prototype.visitCompleted = function (t) {
                        t.followRedirect();
                    }),
                    (t.prototype.pageInvalidated = function () {
                        this.reload();
                    }),
                    (t.prototype.visitFailed = function (t) { }),
                    (t.prototype.visitRendered = function (t) { }),
                    (t.prototype.showProgressBarAfterDelay = function () {
                        this.progressBarTimeout = window.setTimeout(this.showProgressBar, this.controller.progressBarDelay);
                    }),
                    (t.prototype.hideProgressBar = function () {
                        this.progressBar.hide(), null != this.progressBarTimeout && (window.clearTimeout(this.progressBarTimeout), delete this.progressBarTimeout);
                    }),
                    (t.prototype.reload = function () {
                        window.location.reload();
                    }),
                    t
                );
            })(),
            _ = (function () {
                function t(t) {
                    var e = this;
                    (this.started = !1),
                        (this.submitCaptured = function () {
                            removeEventListener("submit", e.submitBubbled, !1), addEventListener("submit", e.submitBubbled, !1);
                        }),
                        (this.submitBubbled = function (t) {
                            var n = t.target instanceof HTMLFormElement ? t.target : void 0;
                            n && e.delegate.willSubmitForm(n) && (t.preventDefault(), e.delegate.formSubmitted(n));
                        }),
                        (this.delegate = t);
                }
                return (
                    (t.prototype.start = function () {
                        this.started || (addEventListener("submit", this.submitCaptured, !0), (this.started = !0));
                    }),
                    (t.prototype.stop = function () {
                        this.started && (removeEventListener("submit", this.submitCaptured, !0), (this.started = !1));
                    }),
                    t
                );
            })(),
            q = function () {
                return (q =
                    Object.assign ||
                    function (t) {
                        for (var e, n = 1, r = arguments.length; n < r; n++) for (var i in (e = arguments[n])) Object.prototype.hasOwnProperty.call(e, i) && (t[i] = e[i]);
                        return t;
                    }).apply(this, arguments);
            },
            H = (function () {
                function t(t) {
                    var e = this;
                    (this.restorationData = {}),
                        (this.started = !1),
                        (this.pageLoaded = !1),
                        (this.onPopState = function (t) {
                            if (e.shouldHandlePopState()) {
                                var n = t.state.turbolinks;
                                if (n) {
                                    var r = m.currentLocation;
                                    e.location = r;
                                    var i = n.restorationIdentifier;
                                    (e.restorationIdentifier = i), e.delegate.historyPoppedToLocationWithRestorationIdentifier(r, i);
                                }
                            }
                        }),
                        (this.onPageLoad = function (t) {
                            o(function () {
                                e.pageLoaded = !0;
                            });
                        }),
                        (this.delegate = t);
                }
                return (
                    (t.prototype.start = function () {
                        this.started ||
                            ((this.previousScrollRestoration = history.scrollRestoration),
                                (history.scrollRestoration = "manual"),
                                addEventListener("popstate", this.onPopState, !1),
                                addEventListener("load", this.onPageLoad, !1),
                                (this.started = !0),
                                this.replace(m.currentLocation));
                    }),
                    (t.prototype.stop = function () {
                        var t;
                        this.started &&
                            ((history.scrollRestoration = null !== (t = this.previousScrollRestoration) && void 0 !== t ? t : "auto"),
                                removeEventListener("popstate", this.onPopState, !1),
                                removeEventListener("load", this.onPageLoad, !1),
                                (this.started = !1));
                    }),
                    (t.prototype.push = function (t, e) {
                        this.update(history.pushState, t, e);
                    }),
                    (t.prototype.replace = function (t, e) {
                        this.update(history.replaceState, t, e);
                    }),
                    (t.prototype.update = function (t, e, n) {
                        void 0 === n && (n = h());
                        var r = { turbolinks: { restorationIdentifier: n } };
                        t.call(history, r, "", e.absoluteURL), (this.location = e), (this.restorationIdentifier = n);
                    }),
                    (t.prototype.getRestorationDataForIdentifier = function (t) {
                        return this.restorationData[t] || {};
                    }),
                    (t.prototype.updateRestorationData = function (t) {
                        var e = this.restorationIdentifier,
                            n = this.restorationData[e];
                        this.restorationData[e] = q(q({}, n), t);
                    }),
                    (t.prototype.shouldHandlePopState = function () {
                        return this.pageIsLoaded();
                    }),
                    (t.prototype.pageIsLoaded = function () {
                        return this.pageLoaded || "complete" == document.readyState;
                    }),
                    t
                );
            })(),
            z = (function () {
                function t(t) {
                    var e = this;
                    (this.started = !1),
                        (this.clickCaptured = function () {
                            removeEventListener("click", e.clickBubbled, !1), addEventListener("click", e.clickBubbled, !1);
                        }),
                        (this.clickBubbled = function (t) {
                            if (e.clickEventIsSignificant(t)) {
                                var n = e.findLinkFromClickTarget(t.target);
                                if (n) {
                                    var r = e.getLocationForLink(n);
                                    e.delegate.willFollowLinkToLocation(n, r) && (t.preventDefault(), e.delegate.followedLinkToLocation(n, r));
                                }
                            }
                        }),
                        (this.delegate = t);
                }
                return (
                    (t.prototype.start = function () {
                        this.started || (addEventListener("click", this.clickCaptured, !0), (this.started = !0));
                    }),
                    (t.prototype.stop = function () {
                        this.started && (removeEventListener("click", this.clickCaptured, !0), (this.started = !1));
                    }),
                    (t.prototype.clickEventIsSignificant = function (t) {
                        return !((t.target && t.target.isContentEditable) || t.defaultPrevented || t.which > 1 || t.altKey || t.ctrlKey || t.metaKey || t.shiftKey);
                    }),
                    (t.prototype.findLinkFromClickTarget = function (t) {
                        if (t instanceof Element) return i(t, "a[href]:not([target^=_]):not([download])");
                    }),
                    (t.prototype.getLocationForLink = function (t) {
                        return new m(t.getAttribute("href") || "");
                    }),
                    t
                );
            })(),
            U = function () {
                return (U =
                    Object.assign ||
                    function (t) {
                        for (var e, n = 1, r = arguments.length; n < r; n++) for (var i in (e = arguments[n])) Object.prototype.hasOwnProperty.call(e, i) && (t[i] = e[i]);
                        return t;
                    }).apply(this, arguments);
            },
            W = function (t, e, n, r) {
                return new (n || (n = Promise))(function (i, o) {
                    function s(t) {
                        try {
                            c(r.next(t));
                        } catch (e) {
                            o(e);
                        }
                    }
                    function a(t) {
                        try {
                            c(r.throw(t));
                        } catch (e) {
                            o(e);
                        }
                    }
                    function c(t) {
                        var e;
                        t.done
                            ? i(t.value)
                            : ((e = t.value),
                                e instanceof n
                                    ? e
                                    : new n(function (t) {
                                        t(e);
                                    })).then(s, a);
                    }
                    c((r = r.apply(t, e || [])).next());
                });
            },
            K = function (t, e) {
                var n,
                    r,
                    i,
                    o,
                    s = {
                        label: 0,
                        sent: function () {
                            if (1 & i[0]) throw i[1];
                            return i[1];
                        },
                        trys: [],
                        ops: [],
                    };
                return (
                    (o = { next: a(0), throw: a(1), return: a(2) }),
                    "function" === typeof Symbol &&
                    (o[Symbol.iterator] = function () {
                        return this;
                    }),
                    o
                );
                function a(o) {
                    return function (a) {
                        return (function (o) {
                            if (n) throw new TypeError("Generator is already executing.");
                            for (; s;)
                                try {
                                    if (((n = 1), r && (i = 2 & o[0] ? r.return : o[0] ? r.throw || ((i = r.return) && i.call(r), 0) : r.next) && !(i = i.call(r, o[1])).done)) return i;
                                    switch (((r = 0), i && (o = [2 & o[0], i.value]), o[0])) {
                                        case 0:
                                        case 1:
                                            i = o;
                                            break;
                                        case 4:
                                            return s.label++, { value: o[1], done: !1 };
                                        case 5:
                                            s.label++, (r = o[1]), (o = [0]);
                                            continue;
                                        case 7:
                                            (o = s.ops.pop()), s.trys.pop();
                                            continue;
                                        default:
                                            if (!(i = (i = s.trys).length > 0 && i[i.length - 1]) && (6 === o[0] || 2 === o[0])) {
                                                s = 0;
                                                continue;
                                            }
                                            if (3 === o[0] && (!i || (o[1] > i[0] && o[1] < i[3]))) {
                                                s.label = o[1];
                                                break;
                                            }
                                            if (6 === o[0] && s.label < i[1]) {
                                                (s.label = i[1]), (i = o);
                                                break;
                                            }
                                            if (i && s.label < i[2]) {
                                                (s.label = i[2]), s.ops.push(o);
                                                break;
                                            }
                                            i[2] && s.ops.pop(), s.trys.pop();
                                            continue;
                                    }
                                    o = e.call(t, s);
                                } catch (a) {
                                    (o = [6, a]), (r = 0);
                                } finally {
                                    n = i = 0;
                                }
                            if (5 & o[0]) throw o[1];
                            return { value: o[0] ? o[1] : void 0, done: !0 };
                        })([o, a]);
                    };
                }
            };
        !(function (t) {
            (t[(t.initialized = 0)] = "initialized"), (t[(t.requesting = 1)] = "requesting"), (t[(t.waiting = 2)] = "waiting"), (t[(t.receiving = 3)] = "receiving"), (t[(t.stopping = 4)] = "stopping"), (t[(t.stopped = 5)] = "stopped");
        })(N || (N = {}));
        var J = (function () {
            function t(t, e, n) {
                void 0 === n && (n = !1),
                    (this.state = N.initialized),
                    (this.delegate = t),
                    (this.formElement = e),
                    (this.formData = new FormData(e)),
                    (this.fetchRequest = new T(this, this.method, this.location, this.formData)),
                    (this.mustRedirect = n);
            }
            return (
                Object.defineProperty(t.prototype, "method", {
                    get: function () {
                        return E((this.formElement.getAttribute("method") || "").toLowerCase()) || g.get;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "location", {
                    get: function () {
                        return m.wrap(this.formElement.action);
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                (t.prototype.start = function () {
                    return W(this, void 0, void 0, function () {
                        var t, e;
                        return K(this, function (n) {
                            return (t = N.initialized), (e = N.requesting), this.state == t ? ((this.state = e), [2, this.fetchRequest.perform()]) : [2];
                        });
                    });
                }),
                (t.prototype.stop = function () {
                    var t = N.stopping,
                        e = N.stopped;
                    if (this.state != t && this.state != e) return (this.state = t), this.fetchRequest.cancel(), !0;
                }),
                (t.prototype.additionalHeadersForRequest = function (t) {
                    var e = {};
                    if (this.method != g.get) {
                        var n =
                            (function (t) {
                                if (null != t) {
                                    var e = (document.cookie ? document.cookie.split("; ") : []).find(function (e) {
                                        return e.startsWith(t);
                                    });
                                    if (e) {
                                        var n = e.split("=").slice(1).join("=");
                                        return n ? decodeURIComponent(n) : void 0;
                                    }
                                }
                            })(Y("csrf-param")) || Y("csrf-token");
                        n && (e["X-CSRF-Token"] = n);
                    }
                    return e;
                }),
                (t.prototype.requestStarted = function (t) {
                    (this.state = N.waiting), s("turbolinks:submit-start", { target: this.formElement, data: { formSubmission: this } }), this.delegate.formSubmissionStarted(this);
                }),
                (t.prototype.requestPreventedHandlingResponse = function (t, e) {
                    this.result = { success: e.succeeded, fetchResponse: e };
                }),
                (t.prototype.requestSucceededWithResponse = function (t, e) {
                    if (this.requestMustRedirect(t) && !e.redirected) {
                        var n = new Error("Form responses must redirect to another location");
                        this.delegate.formSubmissionErrored(this, n);
                    } else (this.state = N.receiving), (this.result = { success: !0, fetchResponse: e }), this.delegate.formSubmissionSucceededWithResponse(this, e);
                }),
                (t.prototype.requestFailedWithResponse = function (t, e) {
                    (this.result = { success: !1, fetchResponse: e }), this.delegate.formSubmissionFailedWithResponse(this, e);
                }),
                (t.prototype.requestErrored = function (t, e) {
                    (this.result = { success: !1, error: e }), this.delegate.formSubmissionErrored(this, e);
                }),
                (t.prototype.requestFinished = function (t) {
                    (this.state = N.stopped), s("turbolinks:submit-end", { target: this.formElement, data: U({ formSubmission: this }, this.result) }), this.delegate.formSubmissionFinished(this);
                }),
                (t.prototype.requestMustRedirect = function (t) {
                    return !t.isIdempotent && this.mustRedirect;
                }),
                t
            );
        })();
        function Y(t) {
            var e = document.querySelector('meta[name="' + t + '"]');
            return e && e.content;
        }
        var Q,
            $ = function () {
                return ($ =
                    Object.assign ||
                    function (t) {
                        for (var e, n = 1, r = arguments.length; n < r; n++) for (var i in (e = arguments[n])) Object.prototype.hasOwnProperty.call(e, i) && (t[i] = e[i]);
                        return t;
                    }).apply(this, arguments);
            },
            X = function (t, e, n, r) {
                return new (n || (n = Promise))(function (i, o) {
                    function s(t) {
                        try {
                            c(r.next(t));
                        } catch (e) {
                            o(e);
                        }
                    }
                    function a(t) {
                        try {
                            c(r.throw(t));
                        } catch (e) {
                            o(e);
                        }
                    }
                    function c(t) {
                        var e;
                        t.done
                            ? i(t.value)
                            : ((e = t.value),
                                e instanceof n
                                    ? e
                                    : new n(function (t) {
                                        t(e);
                                    })).then(s, a);
                    }
                    c((r = r.apply(t, e || [])).next());
                });
            },
            Z = function (t, e) {
                var n,
                    r,
                    i,
                    o,
                    s = {
                        label: 0,
                        sent: function () {
                            if (1 & i[0]) throw i[1];
                            return i[1];
                        },
                        trys: [],
                        ops: [],
                    };
                return (
                    (o = { next: a(0), throw: a(1), return: a(2) }),
                    "function" === typeof Symbol &&
                    (o[Symbol.iterator] = function () {
                        return this;
                    }),
                    o
                );
                function a(o) {
                    return function (a) {
                        return (function (o) {
                            if (n) throw new TypeError("Generator is already executing.");
                            for (; s;)
                                try {
                                    if (((n = 1), r && (i = 2 & o[0] ? r.return : o[0] ? r.throw || ((i = r.return) && i.call(r), 0) : r.next) && !(i = i.call(r, o[1])).done)) return i;
                                    switch (((r = 0), i && (o = [2 & o[0], i.value]), o[0])) {
                                        case 0:
                                        case 1:
                                            i = o;
                                            break;
                                        case 4:
                                            return s.label++, { value: o[1], done: !1 };
                                        case 5:
                                            s.label++, (r = o[1]), (o = [0]);
                                            continue;
                                        case 7:
                                            (o = s.ops.pop()), s.trys.pop();
                                            continue;
                                        default:
                                            if (!(i = (i = s.trys).length > 0 && i[i.length - 1]) && (6 === o[0] || 2 === o[0])) {
                                                s = 0;
                                                continue;
                                            }
                                            if (3 === o[0] && (!i || (o[1] > i[0] && o[1] < i[3]))) {
                                                s.label = o[1];
                                                break;
                                            }
                                            if (6 === o[0] && s.label < i[1]) {
                                                (s.label = i[1]), (i = o);
                                                break;
                                            }
                                            if (i && s.label < i[2]) {
                                                (s.label = i[2]), s.ops.push(o);
                                                break;
                                            }
                                            i[2] && s.ops.pop(), s.trys.pop();
                                            continue;
                                    }
                                    o = e.call(t, s);
                                } catch (a) {
                                    (o = [6, a]), (r = 0);
                                } finally {
                                    n = i = 0;
                                }
                            if (5 & o[0]) throw o[1];
                            return { value: o[0] ? o[1] : void 0, done: !0 };
                        })([o, a]);
                    };
                }
            },
            G = (function () {
                function t(t) {
                    this.delegate = t;
                }
                return (
                    (t.prototype.proposeVisit = function (t, e) {
                        void 0 === e && (e = {}), this.delegate.allowsVisitingLocation(t) && this.delegate.visitProposedToLocation(t, e);
                    }),
                    (t.prototype.startVisit = function (t, e, n) {
                        void 0 === n && (n = {}), this.stop(), (this.currentVisit = new B(this, t, e, $({ referrer: this.location }, n))), this.currentVisit.start();
                    }),
                    (t.prototype.submitForm = function (t) {
                        this.stop(), (this.formSubmission = new J(this, t, !0)), this.formSubmission.start();
                    }),
                    (t.prototype.stop = function () {
                        this.formSubmission && (this.formSubmission.stop(), delete this.formSubmission), this.currentVisit && (this.currentVisit.cancel(), delete this.currentVisit);
                    }),
                    (t.prototype.reload = function () { }),
                    (t.prototype.goBack = function () { }),
                    Object.defineProperty(t.prototype, "adapter", {
                        get: function () {
                            return this.delegate.adapter;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "view", {
                        get: function () {
                            return this.delegate.view;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "history", {
                        get: function () {
                            return this.delegate.history;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    (t.prototype.formSubmissionStarted = function (t) { }),
                    (t.prototype.formSubmissionSucceededWithResponse = function (t, e) {
                        return X(this, void 0, void 0, function () {
                            var n, r, i;
                            return Z(this, function (o) {
                                switch (o.label) {
                                    case 0:
                                        return console.log("Form submission succeeded", t), t != this.formSubmission ? [3, 2] : [4, e.responseHTML];
                                    case 1:
                                        (n = o.sent()) &&
                                            (t.method != g.get && (console.log("Clearing snapshot cache after successful form submission"), this.view.clearSnapshotCache()),
                                                (r = e.statusCode),
                                                (i = { response: { statusCode: r, responseHTML: n } }),
                                                console.log("Visiting", e.location, i),
                                                this.proposeVisit(e.location, i)),
                                            (o.label = 2);
                                    case 2:
                                        return [2];
                                }
                            });
                        });
                    }),
                    (t.prototype.formSubmissionFailedWithResponse = function (t, e) {
                        console.error("Form submission failed", t, e);
                    }),
                    (t.prototype.formSubmissionErrored = function (t, e) {
                        console.error("Form submission failed", t, e);
                    }),
                    (t.prototype.formSubmissionFinished = function (t) { }),
                    (t.prototype.visitStarted = function (t) {
                        this.delegate.visitStarted(t);
                    }),
                    (t.prototype.visitCompleted = function (t) {
                        this.delegate.visitCompleted(t);
                    }),
                    Object.defineProperty(t.prototype, "location", {
                        get: function () {
                            return this.history.location;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    t
                );
            })();
        !(function (t) {
            (t[(t.initial = 0)] = "initial"), (t[(t.loading = 1)] = "loading"), (t[(t.interactive = 2)] = "interactive"), (t[(t.complete = 3)] = "complete"), (t[(t.invalidated = 4)] = "invalidated");
        })(Q || (Q = {}));
        var tt = (function () {
            function t(t) {
                var e = this;
                (this.stage = Q.initial),
                    (this.started = !1),
                    (this.interpretReadyState = function () {
                        var t = e.readyState;
                        "interactive" == t ? e.pageIsInteractive() : "complete" == t && e.pageIsComplete();
                    }),
                    (this.delegate = t);
            }
            return (
                (t.prototype.start = function () {
                    this.started || (this.stage == Q.initial && (this.stage = Q.loading), document.addEventListener("readystatechange", this.interpretReadyState, !1), (this.started = !0));
                }),
                (t.prototype.stop = function () {
                    this.started && (document.removeEventListener("readystatechange", this.interpretReadyState, !1), (this.started = !1));
                }),
                (t.prototype.invalidate = function () {
                    this.stage != Q.invalidated && ((this.stage = Q.invalidated), this.delegate.pageInvalidated());
                }),
                (t.prototype.pageIsInteractive = function () {
                    this.stage == Q.loading && ((this.stage = Q.interactive), this.delegate.pageBecameInteractive());
                }),
                (t.prototype.pageIsComplete = function () {
                    this.pageIsInteractive(), this.stage == Q.interactive && ((this.stage = Q.complete), this.delegate.pageLoaded());
                }),
                Object.defineProperty(t.prototype, "readyState", {
                    get: function () {
                        return document.readyState;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                t
            );
        })(),
            et = (function () {
                function t(t) {
                    var e = this;
                    (this.started = !1),
                        (this.onScroll = function () {
                            e.updatePosition({ x: window.pageXOffset, y: window.pageYOffset });
                        }),
                        (this.delegate = t);
                }
                return (
                    (t.prototype.start = function () {
                        this.started || (addEventListener("scroll", this.onScroll, !1), this.onScroll(), (this.started = !0));
                    }),
                    (t.prototype.stop = function () {
                        this.started && (removeEventListener("scroll", this.onScroll, !1), (this.started = !1));
                    }),
                    (t.prototype.updatePosition = function (t) {
                        this.delegate.scrollPositionChanged(t);
                    }),
                    t
                );
            })();
        var nt = (function () {
            function t() { }
            return (
                (t.prototype.renderView = function (t) {
                    this.delegate.viewWillRender(this.newBody), t(), this.delegate.viewRendered(this.newBody);
                }),
                (t.prototype.invalidateView = function () {
                    this.delegate.viewInvalidated();
                }),
                (t.prototype.createScriptElement = function (t) {
                    if ("false" == t.getAttribute("data-turbolinks-eval")) return t;
                    var e = document.createElement("script");
                    return (
                        (e.textContent = t.textContent),
                        (e.async = !1),
                        (function (t, e) {
                            for (var n = 0, i = r(e.attributes); n < i.length; n++) {
                                var o = i[n],
                                    s = o.name,
                                    a = o.value;
                                t.setAttribute(s, a);
                            }
                        })(e, t),
                        e
                    );
                }),
                t
            );
        })();
        var rt,
            it =
                ((rt = function (t, e) {
                    return (rt =
                        Object.setPrototypeOf ||
                        ({ __proto__: [] } instanceof Array &&
                            function (t, e) {
                                t.__proto__ = e;
                            }) ||
                        function (t, e) {
                            for (var n in e) e.hasOwnProperty(n) && (t[n] = e[n]);
                        })(t, e);
                }),
                    function (t, e) {
                        function n() {
                            this.constructor = t;
                        }
                        rt(t, e), (t.prototype = null === e ? Object.create(e) : ((n.prototype = e.prototype), new n()));
                    }),
            ot = (function (t) {
                function e(e, n) {
                    var r,
                        i = t.call(this) || this;
                    return (
                        (i.delegate = e),
                        (i.htmlElement = (((r = document.createElement("html")).innerHTML = n), r)),
                        (i.newHead = i.htmlElement.querySelector("head") || document.createElement("head")),
                        (i.newBody = i.htmlElement.querySelector("body") || document.createElement("body")),
                        i
                    );
                }
                return (
                    it(e, t),
                    (e.render = function (t, e, n) {
                        return new this(t, n).render(e);
                    }),
                    (e.prototype.render = function (t) {
                        var e = this;
                        this.renderView(function () {
                            e.replaceHeadAndBody(), e.activateBodyScriptElements(), t();
                        });
                    }),
                    (e.prototype.replaceHeadAndBody = function () {
                        var t = document.documentElement,
                            e = document.head,
                            n = document.body;
                        t.replaceChild(this.newHead, e), t.replaceChild(this.newBody, n);
                    }),
                    (e.prototype.activateBodyScriptElements = function () {
                        for (var t = 0, e = this.getScriptElements(); t < e.length; t++) {
                            var n = e[t],
                                r = n.parentNode;
                            if (r) {
                                var i = this.createScriptElement(n);
                                r.replaceChild(i, n);
                            }
                        }
                    }),
                    (e.prototype.getScriptElements = function () {
                        return r(document.documentElement.querySelectorAll("script"));
                    }),
                    e
                );
            })(nt),
            st = (function () {
                function t(t) {
                    (this.keys = []), (this.snapshots = {}), (this.size = t);
                }
                return (
                    (t.prototype.has = function (t) {
                        return t.toCacheKey() in this.snapshots;
                    }),
                    (t.prototype.get = function (t) {
                        if (this.has(t)) {
                            var e = this.read(t);
                            return this.touch(t), e;
                        }
                    }),
                    (t.prototype.put = function (t, e) {
                        return this.write(t, e), this.touch(t), e;
                    }),
                    (t.prototype.clear = function () {
                        this.snapshots = {};
                    }),
                    (t.prototype.read = function (t) {
                        return this.snapshots[t.toCacheKey()];
                    }),
                    (t.prototype.write = function (t, e) {
                        this.snapshots[t.toCacheKey()] = e;
                    }),
                    (t.prototype.touch = function (t) {
                        var e = t.toCacheKey(),
                            n = this.keys.indexOf(e);
                        n > -1 && this.keys.splice(n, 1), this.keys.unshift(e), this.trim();
                    }),
                    (t.prototype.trim = function () {
                        for (var t = 0, e = this.keys.splice(this.size); t < e.length; t++) {
                            var n = e[t];
                            delete this.snapshots[n];
                        }
                    }),
                    t
                );
            })(),
            at = (function () {
                var t = function (e, n) {
                    return (t =
                        Object.setPrototypeOf ||
                        ({ __proto__: [] } instanceof Array &&
                            function (t, e) {
                                t.__proto__ = e;
                            }) ||
                        function (t, e) {
                            for (var n in e) e.hasOwnProperty(n) && (t[n] = e[n]);
                        })(e, n);
                };
                return function (e, n) {
                    function r() {
                        this.constructor = e;
                    }
                    t(e, n), (e.prototype = null === n ? Object.create(n) : ((r.prototype = n.prototype), new r()));
                };
            })(),
            ct = function () {
                for (var t = 0, e = 0, n = arguments.length; e < n; e++) t += arguments[e].length;
                var r = Array(t),
                    i = 0;
                for (e = 0; e < n; e++) for (var o = arguments[e], s = 0, a = o.length; s < a; s++, i++) r[i] = o[s];
                return r;
            },
            lt = (function (t) {
                function e(e, n, r, i) {
                    var o = t.call(this) || this;
                    return (o.delegate = e), (o.currentSnapshot = n), (o.currentHeadDetails = n.headDetails), (o.newSnapshot = r), (o.newHeadDetails = r.headDetails), (o.newBody = r.bodyElement), (o.isPreview = i), o;
                }
                return (
                    at(e, t),
                    (e.render = function (t, e, n, r, i) {
                        return new this(t, n, r, i).render(e);
                    }),
                    (e.prototype.render = function (t) {
                        var e = this;
                        this.shouldRender()
                            ? (this.mergeHead(),
                                this.renderView(function () {
                                    e.replaceBody(), e.isPreview || e.focusFirstAutofocusableElement(), t();
                                }))
                            : this.invalidateView();
                    }),
                    (e.prototype.mergeHead = function () {
                        this.copyNewHeadStylesheetElements(), this.copyNewHeadScriptElements(), this.removeCurrentHeadProvisionalElements(), this.copyNewHeadProvisionalElements();
                    }),
                    (e.prototype.replaceBody = function () {
                        var t = this.relocateCurrentBodyPermanentElements();
                        this.activateNewBody(), this.assignNewBody(), this.replacePlaceholderElementsWithClonedPermanentElements(t);
                    }),
                    (e.prototype.shouldRender = function () {
                        return this.newSnapshot.isVisitable() && this.trackedElementsAreIdentical();
                    }),
                    (e.prototype.trackedElementsAreIdentical = function () {
                        return this.currentHeadDetails.getTrackedElementSignature() == this.newHeadDetails.getTrackedElementSignature();
                    }),
                    (e.prototype.copyNewHeadStylesheetElements = function () {
                        for (var t = 0, e = this.getNewHeadStylesheetElements(); t < e.length; t++) {
                            var n = e[t];
                            document.head.appendChild(n);
                        }
                    }),
                    (e.prototype.copyNewHeadScriptElements = function () {
                        for (var t = 0, e = this.getNewHeadScriptElements(); t < e.length; t++) {
                            var n = e[t];
                            document.head.appendChild(this.createScriptElement(n));
                        }
                    }),
                    (e.prototype.removeCurrentHeadProvisionalElements = function () {
                        for (var t = 0, e = this.getCurrentHeadProvisionalElements(); t < e.length; t++) {
                            var n = e[t];
                            document.head.removeChild(n);
                        }
                    }),
                    (e.prototype.copyNewHeadProvisionalElements = function () {
                        for (var t = 0, e = this.getNewHeadProvisionalElements(); t < e.length; t++) {
                            var n = e[t];
                            document.head.appendChild(n);
                        }
                    }),
                    (e.prototype.relocateCurrentBodyPermanentElements = function () {
                        var t = this;
                        return this.getCurrentBodyPermanentElements().reduce(function (e, n) {
                            var r = t.newSnapshot.getPermanentElementById(n.id);
                            if (r) {
                                var i = (function (t) {
                                    var e = document.createElement("meta");
                                    return e.setAttribute("name", "turbolinks-permanent-placeholder"), e.setAttribute("content", t.id), { element: e, permanentElement: t };
                                })(n);
                                return ut(n, i.element), ut(r, n), ct(e, [i]);
                            }
                            return e;
                        }, []);
                    }),
                    (e.prototype.replacePlaceholderElementsWithClonedPermanentElements = function (t) {
                        for (var e = 0, n = t; e < n.length; e++) {
                            var r = n[e];
                            ut(r.element, r.permanentElement.cloneNode(!0));
                        }
                    }),
                    (e.prototype.activateNewBody = function () {
                        document.adoptNode(this.newBody), this.activateNewBodyScriptElements();
                    }),
                    (e.prototype.activateNewBodyScriptElements = function () {
                        for (var t = 0, e = this.getNewBodyScriptElements(); t < e.length; t++) {
                            var n = e[t];
                            ut(n, this.createScriptElement(n));
                        }
                    }),
                    (e.prototype.assignNewBody = function () {
                        document.body ? ut(document.body, this.newBody) : document.documentElement.appendChild(this.newBody);
                    }),
                    (e.prototype.focusFirstAutofocusableElement = function () {
                        var t = this.newSnapshot.findFirstAutofocusableElement();
                        (function (t) {
                            return t && "function" == typeof t.focus;
                        })(t) && t.focus();
                    }),
                    (e.prototype.getNewHeadStylesheetElements = function () {
                        return this.newHeadDetails.getStylesheetElementsNotInDetails(this.currentHeadDetails);
                    }),
                    (e.prototype.getNewHeadScriptElements = function () {
                        return this.newHeadDetails.getScriptElementsNotInDetails(this.currentHeadDetails);
                    }),
                    (e.prototype.getCurrentHeadProvisionalElements = function () {
                        return this.currentHeadDetails.getProvisionalElements();
                    }),
                    (e.prototype.getNewHeadProvisionalElements = function () {
                        return this.newHeadDetails.getProvisionalElements();
                    }),
                    (e.prototype.getCurrentBodyPermanentElements = function () {
                        return this.currentSnapshot.getPermanentElementsPresentInSnapshot(this.newSnapshot);
                    }),
                    (e.prototype.getNewBodyScriptElements = function () {
                        return r(this.newBody.querySelectorAll("script"));
                    }),
                    e
                );
            })(nt);
        function ut(t, e) {
            var n = t.parentElement;
            if (n) return n.replaceChild(e, t);
        }
        var ht = (function () {
            function t(t) {
                (this.htmlElement = document.documentElement), (this.snapshotCache = new st(10)), (this.delegate = t);
            }
            return (
                (t.prototype.getRootLocation = function () {
                    return this.getSnapshot().getRootLocation();
                }),
                (t.prototype.getElementForAnchor = function (t) {
                    return this.getSnapshot().getElementForAnchor(t);
                }),
                (t.prototype.getSnapshot = function () {
                    return P.fromHTMLElement(this.htmlElement);
                }),
                (t.prototype.clearSnapshotCache = function () {
                    this.snapshotCache.clear();
                }),
                (t.prototype.shouldCacheSnapshot = function () {
                    return this.getSnapshot().isCacheable();
                }),
                (t.prototype.cacheSnapshot = function () {
                    var t = this;
                    if (this.shouldCacheSnapshot()) {
                        this.delegate.viewWillCacheSnapshot();
                        var e = this.getSnapshot(),
                            n = this.lastRenderedLocation || m.currentLocation;
                        o(function () {
                            return t.snapshotCache.put(n, e.clone());
                        });
                    }
                }),
                (t.prototype.getCachedSnapshotForLocation = function (t) {
                    return this.snapshotCache.get(t);
                }),
                (t.prototype.render = function (t, e) {
                    var n = t.snapshot,
                        r = t.error,
                        i = t.isPreview;
                    this.markAsPreview(i), n ? this.renderSnapshot(n, i, e) : this.renderError(r, e);
                }),
                (t.prototype.scrollToAnchor = function (t) {
                    var e = this.getElementForAnchor(t);
                    e ? this.scrollToElement(e) : this.scrollToPosition({ x: 0, y: 0 });
                }),
                (t.prototype.scrollToElement = function (t) {
                    t.scrollIntoView();
                }),
                (t.prototype.scrollToPosition = function (t) {
                    var e = t.x,
                        n = t.y;
                    window.scrollTo(e, n);
                }),
                (t.prototype.markAsPreview = function (t) {
                    t ? this.htmlElement.setAttribute("data-turbolinks-preview", "") : this.htmlElement.removeAttribute("data-turbolinks-preview");
                }),
                (t.prototype.renderSnapshot = function (t, e, n) {
                    lt.render(this.delegate, n, this.getSnapshot(), t, e || !1);
                }),
                (t.prototype.renderError = function (t, e) {
                    ot.render(this.delegate, e, t || "");
                }),
                t
            );
        })(),
            dt = (function () {
                function t() {
                    (this.navigator = new G(this)),
                        (this.adapter = new V(this)),
                        (this.history = new H(this)),
                        (this.view = new ht(this)),
                        (this.pageObserver = new tt(this)),
                        (this.linkClickObserver = new z(this)),
                        (this.formSubmitObserver = new _(this)),
                        (this.scrollObserver = new et(this)),
                        (this.enabled = !0),
                        (this.progressBarDelay = 500),
                        (this.started = !1);
                }
                return (
                    (t.prototype.start = function () {
                        t.supported &&
                            !this.started &&
                            (this.pageObserver.start(), this.linkClickObserver.start(), this.formSubmitObserver.start(), this.scrollObserver.start(), this.history.start(), (this.started = !0), (this.enabled = !0));
                    }),
                    (t.prototype.disable = function () {
                        this.enabled = !1;
                    }),
                    (t.prototype.stop = function () {
                        this.started && (this.pageObserver.stop(), this.linkClickObserver.stop(), this.formSubmitObserver.stop(), this.scrollObserver.stop(), this.history.stop(), (this.started = !1));
                    }),
                    (t.prototype.clearCache = function () {
                        this.view.clearSnapshotCache();
                    }),
                    (t.prototype.visit = function (t, e) {
                        void 0 === e && (e = {}), this.navigator.proposeVisit(m.wrap(t), e);
                    }),
                    (t.prototype.startVisitToLocation = function (t, e, n) {
                        this.navigator.startVisit(m.wrap(t), e, n);
                    }),
                    (t.prototype.setProgressBarDelay = function (t) {
                        this.progressBarDelay = t;
                    }),
                    Object.defineProperty(t.prototype, "location", {
                        get: function () {
                            return this.history.location;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "restorationIdentifier", {
                        get: function () {
                            return this.history.restorationIdentifier;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    (t.prototype.historyPoppedToLocationWithRestorationIdentifier = function (t, e) {
                        this.enabled ? this.navigator.proposeVisit(t, { action: "restore", historyChanged: !0 }) : this.adapter.pageInvalidated();
                    }),
                    (t.prototype.scrollPositionChanged = function (t) {
                        this.history.updateRestorationData({ scrollPosition: t });
                    }),
                    (t.prototype.willFollowLinkToLocation = function (t, e) {
                        return this.linkIsVisitable(t) && this.locationIsVisitable(e) && this.applicationAllowsFollowingLinkToLocation(t, e);
                    }),
                    (t.prototype.followedLinkToLocation = function (t, e) {
                        var n = this.getActionForLink(t);
                        this.visit(e, { action: n });
                    }),
                    (t.prototype.allowsVisitingLocation = function (t) {
                        return this.applicationAllowsVisitingLocation(t);
                    }),
                    (t.prototype.visitProposedToLocation = function (t, e) {
                        this.adapter.visitProposedToLocation(t, e);
                    }),
                    (t.prototype.visitStarted = function (t) {
                        this.notifyApplicationAfterVisitingLocation(t.location);
                    }),
                    (t.prototype.visitCompleted = function (t) {
                        this.notifyApplicationAfterPageLoad(t.getTimingMetrics());
                    }),
                    (t.prototype.willSubmitForm = function (t) {
                        return !0;
                    }),
                    (t.prototype.formSubmitted = function (t) {
                        this.navigator.submitForm(t);
                    }),
                    (t.prototype.pageBecameInteractive = function () {
                        (this.view.lastRenderedLocation = this.location), this.notifyApplicationAfterPageLoad();
                    }),
                    (t.prototype.pageLoaded = function () { }),
                    (t.prototype.pageInvalidated = function () {
                        this.adapter.pageInvalidated();
                    }),
                    (t.prototype.viewWillRender = function (t) {
                        this.notifyApplicationBeforeRender(t);
                    }),
                    (t.prototype.viewRendered = function () {
                        (this.view.lastRenderedLocation = this.history.location), this.notifyApplicationAfterRender();
                    }),
                    (t.prototype.viewInvalidated = function () {
                        this.pageObserver.invalidate();
                    }),
                    (t.prototype.viewWillCacheSnapshot = function () {
                        this.notifyApplicationBeforeCachingSnapshot();
                    }),
                    (t.prototype.applicationAllowsFollowingLinkToLocation = function (t, e) {
                        return !this.notifyApplicationAfterClickingLinkToLocation(t, e).defaultPrevented;
                    }),
                    (t.prototype.applicationAllowsVisitingLocation = function (t) {
                        return !this.notifyApplicationBeforeVisitingLocation(t).defaultPrevented;
                    }),
                    (t.prototype.notifyApplicationAfterClickingLinkToLocation = function (t, e) {
                        return s("turbolinks:click", { target: t, data: { url: e.absoluteURL }, cancelable: !0 });
                    }),
                    (t.prototype.notifyApplicationBeforeVisitingLocation = function (t) {
                        return s("turbolinks:before-visit", { data: { url: t.absoluteURL }, cancelable: !0 });
                    }),
                    (t.prototype.notifyApplicationAfterVisitingLocation = function (t) {
                        return s("turbolinks:visit", { data: { url: t.absoluteURL } });
                    }),
                    (t.prototype.notifyApplicationBeforeCachingSnapshot = function () {
                        return s("turbolinks:before-cache");
                    }),
                    (t.prototype.notifyApplicationBeforeRender = function (t) {
                        return s("turbolinks:before-render", { data: { newBody: t } });
                    }),
                    (t.prototype.notifyApplicationAfterRender = function () {
                        return s("turbolinks:render");
                    }),
                    (t.prototype.notifyApplicationAfterPageLoad = function (t) {
                        return void 0 === t && (t = {}), s("turbolinks:load", { data: { url: this.location.absoluteURL, timing: t } });
                    }),
                    (t.prototype.getActionForLink = function (t) {
                        var e = t.getAttribute("data-turbolinks-action");
                        return (function (t) {
                            return "advance" == t || "replace" == t || "restore" == t;
                        })(e)
                            ? e
                            : "advance";
                    }),
                    (t.prototype.linkIsVisitable = function (t) {
                        var e = i(t, "[data-turbolinks]");
                        return !e || "false" != e.getAttribute("data-turbolinks");
                    }),
                    (t.prototype.locationIsVisitable = function (t) {
                        return t.isPrefixedBy(this.view.getRootLocation()) && t.isHTML();
                    }),
                    (t.supported = !!(window.history.pushState && window.requestAnimationFrame && window.addEventListener)),
                    t
                );
            })(),
            pt = new dt(),
            ft = dt.supported;
        function mt(t, e) {
            pt.visit(t, e);
        }
        function gt() {
            pt.clearCache();
        }
        function yt(t) {
            pt.setProgressBarDelay(t);
        }
        function bt() {
            pt.start();
        }
    },
    function (t, e, n) {
        "use strict";
        n.d(e, "a", function () {
            return J;
        }),
            n.d(e, "b", function () {
                return nt;
            });
        var r = (function () {
            function t(t, e, n) {
                (this.eventTarget = t), (this.eventName = e), (this.eventOptions = n), (this.unorderedBindings = new Set());
            }
            return (
                (t.prototype.connect = function () {
                    this.eventTarget.addEventListener(this.eventName, this, this.eventOptions);
                }),
                (t.prototype.disconnect = function () {
                    this.eventTarget.removeEventListener(this.eventName, this, this.eventOptions);
                }),
                (t.prototype.bindingConnected = function (t) {
                    this.unorderedBindings.add(t);
                }),
                (t.prototype.bindingDisconnected = function (t) {
                    this.unorderedBindings.delete(t);
                }),
                (t.prototype.handleEvent = function (t) {
                    for (
                        var e = (function (t) {
                            if (("immediatePropagationStopped" in t)) return t;
                            var e = t.stopImmediatePropagation;
                            return Object.assign(t, {
                                immediatePropagationStopped: !1,
                                stopImmediatePropagation: function () {
                                    (this.immediatePropagationStopped = !0), e.call(this);
                                },
                            });
                        })(t),
                        n = 0,
                        r = this.bindings;
                        n < r.length;
                        n++
                    ) {
                        var i = r[n];
                        if (e.immediatePropagationStopped) break;
                        i.handleEvent(e);
                    }
                }),
                Object.defineProperty(t.prototype, "bindings", {
                    get: function () {
                        return Array.from(this.unorderedBindings).sort(function (t, e) {
                            var n = t.index,
                                r = e.index;
                            return n < r ? -1 : n > r ? 1 : 0;
                        });
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                t
            );
        })();
        var i = (function () {
            function t(t) {
                (this.application = t), (this.eventListenerMaps = new Map()), (this.started = !1);
            }
            return (
                (t.prototype.start = function () {
                    this.started ||
                        ((this.started = !0),
                            this.eventListeners.forEach(function (t) {
                                return t.connect();
                            }));
                }),
                (t.prototype.stop = function () {
                    this.started &&
                        ((this.started = !1),
                            this.eventListeners.forEach(function (t) {
                                return t.disconnect();
                            }));
                }),
                Object.defineProperty(t.prototype, "eventListeners", {
                    get: function () {
                        return Array.from(this.eventListenerMaps.values()).reduce(function (t, e) {
                            return t.concat(Array.from(e.values()));
                        }, []);
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                (t.prototype.bindingConnected = function (t) {
                    this.fetchEventListenerForBinding(t).bindingConnected(t);
                }),
                (t.prototype.bindingDisconnected = function (t) {
                    this.fetchEventListenerForBinding(t).bindingDisconnected(t);
                }),
                (t.prototype.handleError = function (t, e, n) {
                    void 0 === n && (n = {}), this.application.handleError(t, "Error " + e, n);
                }),
                (t.prototype.fetchEventListenerForBinding = function (t) {
                    var e = t.eventTarget,
                        n = t.eventName,
                        r = t.eventOptions;
                    return this.fetchEventListener(e, n, r);
                }),
                (t.prototype.fetchEventListener = function (t, e, n) {
                    var r = this.fetchEventListenerMapForEventTarget(t),
                        i = this.cacheKey(e, n),
                        o = r.get(i);
                    return o || ((o = this.createEventListener(t, e, n)), r.set(i, o)), o;
                }),
                (t.prototype.createEventListener = function (t, e, n) {
                    var i = new r(t, e, n);
                    return this.started && i.connect(), i;
                }),
                (t.prototype.fetchEventListenerMapForEventTarget = function (t) {
                    var e = this.eventListenerMaps.get(t);
                    return e || ((e = new Map()), this.eventListenerMaps.set(t, e)), e;
                }),
                (t.prototype.cacheKey = function (t, e) {
                    var n = [t];
                    return (
                        Object.keys(e)
                            .sort()
                            .forEach(function (t) {
                                n.push((e[t] ? "" : "!") + t);
                            }),
                        n.join(":")
                    );
                }),
                t
            );
        })(),
            o = /^((.+?)(@(window|document))?->)?(.+?)(#([^:]+?))(:(.+))?$/;
        function s(t) {
            return "window" == t ? window : "document" == t ? document : void 0;
        }
        var a = (function () {
            function t(t, e, n) {
                (this.element = t),
                    (this.index = e),
                    (this.eventTarget = n.eventTarget || t),
                    (this.eventName =
                        n.eventName ||
                        (function (t) {
                            var e = t.tagName.toLowerCase();
                            if (e in c) return c[e](t);
                        })(t) ||
                        l("missing event name")),
                    (this.eventOptions = n.eventOptions || {}),
                    (this.identifier = n.identifier || l("missing identifier")),
                    (this.methodName = n.methodName || l("missing method name"));
            }
            return (
                (t.forToken = function (t) {
                    return new this(
                        t.element,
                        t.index,
                        ((e = t.content),
                        {
                            eventTarget: s((r = e.trim().match(o) || [])[4]),
                            eventName: r[2],
                            eventOptions: r[9]
                                ? ((n = r[9]),
                                    n.split(":").reduce(function (t, e) {
                                        var n;
                                        return Object.assign(t, (((n = {})[e.replace(/^!/, "")] = !/^!/.test(e)), n));
                                    }, {}))
                                : {},
                            identifier: r[5],
                            methodName: r[7],
                        })
                    );
                    var e, n, r;
                }),
                (t.prototype.toString = function () {
                    var t = this.eventTargetName ? "@" + this.eventTargetName : "";
                    return "" + this.eventName + t + "->" + this.identifier + "#" + this.methodName;
                }),
                Object.defineProperty(t.prototype, "eventTargetName", {
                    get: function () {
                        return (t = this.eventTarget) == window ? "window" : t == document ? "document" : void 0;
                        var t;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                t
            );
        })(),
            c = {
                a: function (t) {
                    return "click";
                },
                button: function (t) {
                    return "click";
                },
                form: function (t) {
                    return "submit";
                },
                input: function (t) {
                    return "submit" == t.getAttribute("type") ? "click" : "input";
                },
                select: function (t) {
                    return "change";
                },
                textarea: function (t) {
                    return "input";
                },
            };
        function l(t) {
            throw new Error(t);
        }
        var u = (function () {
            function t(t, e) {
                (this.context = t), (this.action = e);
            }
            return (
                Object.defineProperty(t.prototype, "index", {
                    get: function () {
                        return this.action.index;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "eventTarget", {
                    get: function () {
                        return this.action.eventTarget;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "eventOptions", {
                    get: function () {
                        return this.action.eventOptions;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "identifier", {
                    get: function () {
                        return this.context.identifier;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                (t.prototype.handleEvent = function (t) {
                    this.willBeInvokedByEvent(t) && this.invokeWithEvent(t);
                }),
                Object.defineProperty(t.prototype, "eventName", {
                    get: function () {
                        return this.action.eventName;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "method", {
                    get: function () {
                        var t = this.controller[this.methodName];
                        if ("function" == typeof t) return t;
                        throw new Error('Action "' + this.action + '" references undefined method "' + this.methodName + '"');
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                (t.prototype.invokeWithEvent = function (t) {
                    try {
                        this.method.call(this.controller, t);
                    } catch (l) {
                        var e = this,
                            n = { identifier: e.identifier, controller: e.controller, element: e.element, index: e.index, event: t };
                        this.context.handleError(l, 'invoking action "' + this.action + '"', n);
                    }
                }),
                (t.prototype.willBeInvokedByEvent = function (t) {
                    var e = t.target;
                    return this.element === e || !(e instanceof Element && this.element.contains(e)) || this.scope.containsElement(e);
                }),
                Object.defineProperty(t.prototype, "controller", {
                    get: function () {
                        return this.context.controller;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "methodName", {
                    get: function () {
                        return this.action.methodName;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "element", {
                    get: function () {
                        return this.scope.element;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "scope", {
                    get: function () {
                        return this.context.scope;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                t
            );
        })(),
            h = (function () {
                function t(t, e) {
                    var n = this;
                    (this.element = t),
                        (this.started = !1),
                        (this.delegate = e),
                        (this.elements = new Set()),
                        (this.mutationObserver = new MutationObserver(function (t) {
                            return n.processMutations(t);
                        }));
                }
                return (
                    (t.prototype.start = function () {
                        this.started || ((this.started = !0), this.mutationObserver.observe(this.element, { attributes: !0, childList: !0, subtree: !0 }), this.refresh());
                    }),
                    (t.prototype.stop = function () {
                        this.started && (this.mutationObserver.takeRecords(), this.mutationObserver.disconnect(), (this.started = !1));
                    }),
                    (t.prototype.refresh = function () {
                        if (this.started) {
                            for (var t = new Set(this.matchElementsInTree()), e = 0, n = Array.from(this.elements); e < n.length; e++) {
                                var r = n[e];
                                t.has(r) || this.removeElement(r);
                            }
                            for (var i = 0, o = Array.from(t); i < o.length; i++) {
                                r = o[i];
                                this.addElement(r);
                            }
                        }
                    }),
                    (t.prototype.processMutations = function (t) {
                        if (this.started)
                            for (var e = 0, n = t; e < n.length; e++) {
                                var r = n[e];
                                this.processMutation(r);
                            }
                    }),
                    (t.prototype.processMutation = function (t) {
                        "attributes" == t.type ? this.processAttributeChange(t.target, t.attributeName) : "childList" == t.type && (this.processRemovedNodes(t.removedNodes), this.processAddedNodes(t.addedNodes));
                    }),
                    (t.prototype.processAttributeChange = function (t, e) {
                        var n = t;
                        this.elements.has(n) ? (this.delegate.elementAttributeChanged && this.matchElement(n) ? this.delegate.elementAttributeChanged(n, e) : this.removeElement(n)) : this.matchElement(n) && this.addElement(n);
                    }),
                    (t.prototype.processRemovedNodes = function (t) {
                        for (var e = 0, n = Array.from(t); e < n.length; e++) {
                            var r = n[e],
                                i = this.elementFromNode(r);
                            i && this.processTree(i, this.removeElement);
                        }
                    }),
                    (t.prototype.processAddedNodes = function (t) {
                        for (var e = 0, n = Array.from(t); e < n.length; e++) {
                            var r = n[e],
                                i = this.elementFromNode(r);
                            i && this.elementIsActive(i) && this.processTree(i, this.addElement);
                        }
                    }),
                    (t.prototype.matchElement = function (t) {
                        return this.delegate.matchElement(t);
                    }),
                    (t.prototype.matchElementsInTree = function (t) {
                        return void 0 === t && (t = this.element), this.delegate.matchElementsInTree(t);
                    }),
                    (t.prototype.processTree = function (t, e) {
                        for (var n = 0, r = this.matchElementsInTree(t); n < r.length; n++) {
                            var i = r[n];
                            e.call(this, i);
                        }
                    }),
                    (t.prototype.elementFromNode = function (t) {
                        if (t.nodeType == Node.ELEMENT_NODE) return t;
                    }),
                    (t.prototype.elementIsActive = function (t) {
                        return t.isConnected == this.element.isConnected && this.element.contains(t);
                    }),
                    (t.prototype.addElement = function (t) {
                        this.elements.has(t) || (this.elementIsActive(t) && (this.elements.add(t), this.delegate.elementMatched && this.delegate.elementMatched(t)));
                    }),
                    (t.prototype.removeElement = function (t) {
                        this.elements.has(t) && (this.elements.delete(t), this.delegate.elementUnmatched && this.delegate.elementUnmatched(t));
                    }),
                    t
                );
            })(),
            d = (function () {
                function t(t, e, n) {
                    (this.attributeName = e), (this.delegate = n), (this.elementObserver = new h(t, this));
                }
                return (
                    Object.defineProperty(t.prototype, "element", {
                        get: function () {
                            return this.elementObserver.element;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "selector", {
                        get: function () {
                            return "[" + this.attributeName + "]";
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    (t.prototype.start = function () {
                        this.elementObserver.start();
                    }),
                    (t.prototype.stop = function () {
                        this.elementObserver.stop();
                    }),
                    (t.prototype.refresh = function () {
                        this.elementObserver.refresh();
                    }),
                    Object.defineProperty(t.prototype, "started", {
                        get: function () {
                            return this.elementObserver.started;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    (t.prototype.matchElement = function (t) {
                        return t.hasAttribute(this.attributeName);
                    }),
                    (t.prototype.matchElementsInTree = function (t) {
                        var e = this.matchElement(t) ? [t] : [],
                            n = Array.from(t.querySelectorAll(this.selector));
                        return e.concat(n);
                    }),
                    (t.prototype.elementMatched = function (t) {
                        this.delegate.elementMatchedAttribute && this.delegate.elementMatchedAttribute(t, this.attributeName);
                    }),
                    (t.prototype.elementUnmatched = function (t) {
                        this.delegate.elementUnmatchedAttribute && this.delegate.elementUnmatchedAttribute(t, this.attributeName);
                    }),
                    (t.prototype.elementAttributeChanged = function (t, e) {
                        this.delegate.elementAttributeValueChanged && this.attributeName == e && this.delegate.elementAttributeValueChanged(t, e);
                    }),
                    t
                );
            })(),
            p = (function () {
                function t(t, e) {
                    var n = this;
                    (this.element = t),
                        (this.delegate = e),
                        (this.started = !1),
                        (this.stringMap = new Map()),
                        (this.mutationObserver = new MutationObserver(function (t) {
                            return n.processMutations(t);
                        }));
                }
                return (
                    (t.prototype.start = function () {
                        this.started || ((this.started = !0), this.mutationObserver.observe(this.element, { attributes: !0 }), this.refresh());
                    }),
                    (t.prototype.stop = function () {
                        this.started && (this.mutationObserver.takeRecords(), this.mutationObserver.disconnect(), (this.started = !1));
                    }),
                    (t.prototype.refresh = function () {
                        if (this.started)
                            for (var t = 0, e = this.knownAttributeNames; t < e.length; t++) {
                                var n = e[t];
                                this.refreshAttribute(n);
                            }
                    }),
                    (t.prototype.processMutations = function (t) {
                        if (this.started)
                            for (var e = 0, n = t; e < n.length; e++) {
                                var r = n[e];
                                this.processMutation(r);
                            }
                    }),
                    (t.prototype.processMutation = function (t) {
                        var e = t.attributeName;
                        e && this.refreshAttribute(e);
                    }),
                    (t.prototype.refreshAttribute = function (t) {
                        var e = this.delegate.getStringMapKeyForAttribute(t);
                        if (null != e) {
                            this.stringMap.has(t) || this.stringMapKeyAdded(e, t);
                            var n = this.element.getAttribute(t);
                            this.stringMap.get(t) != n && this.stringMapValueChanged(n, e), null == n ? (this.stringMap.delete(t), this.stringMapKeyRemoved(e, t)) : this.stringMap.set(t, n);
                        }
                    }),
                    (t.prototype.stringMapKeyAdded = function (t, e) {
                        this.delegate.stringMapKeyAdded && this.delegate.stringMapKeyAdded(t, e);
                    }),
                    (t.prototype.stringMapValueChanged = function (t, e) {
                        this.delegate.stringMapValueChanged && this.delegate.stringMapValueChanged(t, e);
                    }),
                    (t.prototype.stringMapKeyRemoved = function (t, e) {
                        this.delegate.stringMapKeyRemoved && this.delegate.stringMapKeyRemoved(t, e);
                    }),
                    Object.defineProperty(t.prototype, "knownAttributeNames", {
                        get: function () {
                            return Array.from(new Set(this.currentAttributeNames.concat(this.recordedAttributeNames)));
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "currentAttributeNames", {
                        get: function () {
                            return Array.from(this.element.attributes).map(function (t) {
                                return t.name;
                            });
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "recordedAttributeNames", {
                        get: function () {
                            return Array.from(this.stringMap.keys());
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    t
                );
            })();
        function f(t, e, n) {
            g(t, e).add(n);
        }
        function m(t, e, n) {
            g(t, e).delete(n),
                (function (t, e) {
                    var n = t.get(e);
                    null != n && 0 == n.size && t.delete(e);
                })(t, e);
        }
        function g(t, e) {
            var n = t.get(e);
            return n || ((n = new Set()), t.set(e, n)), n;
        }
        var y,
            b = (function () {
                function t() {
                    this.valuesByKey = new Map();
                }
                return (
                    Object.defineProperty(t.prototype, "values", {
                        get: function () {
                            return Array.from(this.valuesByKey.values()).reduce(function (t, e) {
                                return t.concat(Array.from(e));
                            }, []);
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "size", {
                        get: function () {
                            return Array.from(this.valuesByKey.values()).reduce(function (t, e) {
                                return t + e.size;
                            }, 0);
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    (t.prototype.add = function (t, e) {
                        f(this.valuesByKey, t, e);
                    }),
                    (t.prototype.delete = function (t, e) {
                        m(this.valuesByKey, t, e);
                    }),
                    (t.prototype.has = function (t, e) {
                        var n = this.valuesByKey.get(t);
                        return null != n && n.has(e);
                    }),
                    (t.prototype.hasKey = function (t) {
                        return this.valuesByKey.has(t);
                    }),
                    (t.prototype.hasValue = function (t) {
                        return Array.from(this.valuesByKey.values()).some(function (e) {
                            return e.has(t);
                        });
                    }),
                    (t.prototype.getValuesForKey = function (t) {
                        var e = this.valuesByKey.get(t);
                        return e ? Array.from(e) : [];
                    }),
                    (t.prototype.getKeysForValue = function (t) {
                        return Array.from(this.valuesByKey)
                            .filter(function (e) {
                                e[0];
                                return e[1].has(t);
                            })
                            .map(function (t) {
                                var e = t[0];
                                t[1];
                                return e;
                            });
                    }),
                    t
                );
            })(),
            v =
                ((y = function (t, e) {
                    return (y =
                        Object.setPrototypeOf ||
                        ({ __proto__: [] } instanceof Array &&
                            function (t, e) {
                                t.__proto__ = e;
                            }) ||
                        function (t, e) {
                            for (var n in e) e.hasOwnProperty(n) && (t[n] = e[n]);
                        })(t, e);
                }),
                    function (t, e) {
                        function n() {
                            this.constructor = t;
                        }
                        y(t, e), (t.prototype = null === e ? Object.create(e) : ((n.prototype = e.prototype), new n()));
                    }),
            w =
                ((function (t) {
                    function e() {
                        var e = t.call(this) || this;
                        return (e.keysByValue = new Map()), e;
                    }
                    v(e, t),
                        Object.defineProperty(e.prototype, "values", {
                            get: function () {
                                return Array.from(this.keysByValue.keys());
                            },
                            enumerable: !0,
                            configurable: !0,
                        }),
                        (e.prototype.add = function (e, n) {
                            t.prototype.add.call(this, e, n), f(this.keysByValue, n, e);
                        }),
                        (e.prototype.delete = function (e, n) {
                            t.prototype.delete.call(this, e, n), m(this.keysByValue, n, e);
                        }),
                        (e.prototype.hasValue = function (t) {
                            return this.keysByValue.has(t);
                        }),
                        (e.prototype.getKeysForValue = function (t) {
                            var e = this.keysByValue.get(t);
                            return e ? Array.from(e) : [];
                        });
                })(b),
                    (function () {
                        function t(t, e, n) {
                            (this.attributeObserver = new d(t, e, this)), (this.delegate = n), (this.tokensByElement = new b());
                        }
                        return (
                            Object.defineProperty(t.prototype, "started", {
                                get: function () {
                                    return this.attributeObserver.started;
                                },
                                enumerable: !0,
                                configurable: !0,
                            }),
                            (t.prototype.start = function () {
                                this.attributeObserver.start();
                            }),
                            (t.prototype.stop = function () {
                                this.attributeObserver.stop();
                            }),
                            (t.prototype.refresh = function () {
                                this.attributeObserver.refresh();
                            }),
                            Object.defineProperty(t.prototype, "element", {
                                get: function () {
                                    return this.attributeObserver.element;
                                },
                                enumerable: !0,
                                configurable: !0,
                            }),
                            Object.defineProperty(t.prototype, "attributeName", {
                                get: function () {
                                    return this.attributeObserver.attributeName;
                                },
                                enumerable: !0,
                                configurable: !0,
                            }),
                            (t.prototype.elementMatchedAttribute = function (t) {
                                this.tokensMatched(this.readTokensForElement(t));
                            }),
                            (t.prototype.elementAttributeValueChanged = function (t) {
                                var e = this.refreshTokensForElement(t),
                                    n = e[0],
                                    r = e[1];
                                this.tokensUnmatched(n), this.tokensMatched(r);
                            }),
                            (t.prototype.elementUnmatchedAttribute = function (t) {
                                this.tokensUnmatched(this.tokensByElement.getValuesForKey(t));
                            }),
                            (t.prototype.tokensMatched = function (t) {
                                var e = this;
                                t.forEach(function (t) {
                                    return e.tokenMatched(t);
                                });
                            }),
                            (t.prototype.tokensUnmatched = function (t) {
                                var e = this;
                                t.forEach(function (t) {
                                    return e.tokenUnmatched(t);
                                });
                            }),
                            (t.prototype.tokenMatched = function (t) {
                                this.delegate.tokenMatched(t), this.tokensByElement.add(t.element, t);
                            }),
                            (t.prototype.tokenUnmatched = function (t) {
                                this.delegate.tokenUnmatched(t), this.tokensByElement.delete(t.element, t);
                            }),
                            (t.prototype.refreshTokensForElement = function (t) {
                                var e,
                                    n,
                                    r,
                                    i = this.tokensByElement.getValuesForKey(t),
                                    o = this.readTokensForElement(t),
                                    s = ((e = i),
                                        (n = o),
                                        (r = Math.max(e.length, n.length)),
                                        Array.from({ length: r }, function (t, r) {
                                            return [e[r], n[r]];
                                        })).findIndex(function (t) {
                                            return !(function (t, e) {
                                                return t && e && t.index == e.index && t.content == e.content;
                                            })(t[0], t[1]);
                                        });
                                return -1 == s ? [[], []] : [i.slice(s), o.slice(s)];
                            }),
                            (t.prototype.readTokensForElement = function (t) {
                                var e = this.attributeName;
                                return (function (t, e, n) {
                                    return t
                                        .trim()
                                        .split(/\s+/)
                                        .filter(function (t) {
                                            return t.length;
                                        })
                                        .map(function (t, r) {
                                            return { element: e, attributeName: n, content: t, index: r };
                                        });
                                })(t.getAttribute(e) || "", t, e);
                            }),
                            t
                        );
                    })());
        var E = (function () {
            function t(t, e, n) {
                (this.tokenListObserver = new w(t, e, this)), (this.delegate = n), (this.parseResultsByToken = new WeakMap()), (this.valuesByTokenByElement = new WeakMap());
            }
            return (
                Object.defineProperty(t.prototype, "started", {
                    get: function () {
                        return this.tokenListObserver.started;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                (t.prototype.start = function () {
                    this.tokenListObserver.start();
                }),
                (t.prototype.stop = function () {
                    this.tokenListObserver.stop();
                }),
                (t.prototype.refresh = function () {
                    this.tokenListObserver.refresh();
                }),
                Object.defineProperty(t.prototype, "element", {
                    get: function () {
                        return this.tokenListObserver.element;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "attributeName", {
                    get: function () {
                        return this.tokenListObserver.attributeName;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                (t.prototype.tokenMatched = function (t) {
                    var e = t.element,
                        n = this.fetchParseResultForToken(t).value;
                    n && (this.fetchValuesByTokenForElement(e).set(t, n), this.delegate.elementMatchedValue(e, n));
                }),
                (t.prototype.tokenUnmatched = function (t) {
                    var e = t.element,
                        n = this.fetchParseResultForToken(t).value;
                    n && (this.fetchValuesByTokenForElement(e).delete(t), this.delegate.elementUnmatchedValue(e, n));
                }),
                (t.prototype.fetchParseResultForToken = function (t) {
                    var e = this.parseResultsByToken.get(t);
                    return e || ((e = this.parseToken(t)), this.parseResultsByToken.set(t, e)), e;
                }),
                (t.prototype.fetchValuesByTokenForElement = function (t) {
                    var e = this.valuesByTokenByElement.get(t);
                    return e || ((e = new Map()), this.valuesByTokenByElement.set(t, e)), e;
                }),
                (t.prototype.parseToken = function (t) {
                    try {
                        return { value: this.delegate.parseValueForToken(t) };
                    } catch (l) {
                        return { error: l };
                    }
                }),
                t
            );
        })(),
            T = (function () {
                function t(t, e) {
                    (this.context = t), (this.delegate = e), (this.bindingsByAction = new Map());
                }
                return (
                    (t.prototype.start = function () {
                        this.valueListObserver || ((this.valueListObserver = new E(this.element, this.actionAttribute, this)), this.valueListObserver.start());
                    }),
                    (t.prototype.stop = function () {
                        this.valueListObserver && (this.valueListObserver.stop(), delete this.valueListObserver, this.disconnectAllActions());
                    }),
                    Object.defineProperty(t.prototype, "element", {
                        get: function () {
                            return this.context.element;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "identifier", {
                        get: function () {
                            return this.context.identifier;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "actionAttribute", {
                        get: function () {
                            return this.schema.actionAttribute;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "schema", {
                        get: function () {
                            return this.context.schema;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "bindings", {
                        get: function () {
                            return Array.from(this.bindingsByAction.values());
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    (t.prototype.connectAction = function (t) {
                        var e = new u(this.context, t);
                        this.bindingsByAction.set(t, e), this.delegate.bindingConnected(e);
                    }),
                    (t.prototype.disconnectAction = function (t) {
                        var e = this.bindingsByAction.get(t);
                        e && (this.bindingsByAction.delete(t), this.delegate.bindingDisconnected(e));
                    }),
                    (t.prototype.disconnectAllActions = function () {
                        var t = this;
                        this.bindings.forEach(function (e) {
                            return t.delegate.bindingDisconnected(e);
                        }),
                            this.bindingsByAction.clear();
                    }),
                    (t.prototype.parseValueForToken = function (t) {
                        var e = a.forToken(t);
                        if (e.identifier == this.identifier) return e;
                    }),
                    (t.prototype.elementMatchedValue = function (t, e) {
                        this.connectAction(e);
                    }),
                    (t.prototype.elementUnmatchedValue = function (t, e) {
                        this.disconnectAction(e);
                    }),
                    t
                );
            })(),
            S = (function () {
                function t(t, e) {
                    (this.context = t), (this.receiver = e), (this.stringMapObserver = new p(this.element, this)), (this.valueDescriptorMap = this.controller.valueDescriptorMap), this.invokeChangedCallbacksForDefaultValues();
                }
                return (
                    (t.prototype.start = function () {
                        this.stringMapObserver.start();
                    }),
                    (t.prototype.stop = function () {
                        this.stringMapObserver.stop();
                    }),
                    Object.defineProperty(t.prototype, "element", {
                        get: function () {
                            return this.context.element;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "controller", {
                        get: function () {
                            return this.context.controller;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    (t.prototype.getStringMapKeyForAttribute = function (t) {
                        if (t in this.valueDescriptorMap) return this.valueDescriptorMap[t].name;
                    }),
                    (t.prototype.stringMapValueChanged = function (t, e) {
                        this.invokeChangedCallbackForValue(e);
                    }),
                    (t.prototype.invokeChangedCallbacksForDefaultValues = function () {
                        for (var t = 0, e = this.valueDescriptors; t < e.length; t++) {
                            var n = e[t],
                                r = n.key,
                                i = n.name;
                            void 0 == n.defaultValue || this.controller.data.has(r) || this.invokeChangedCallbackForValue(i);
                        }
                    }),
                    (t.prototype.invokeChangedCallbackForValue = function (t) {
                        var e = t + "Changed",
                            n = this.receiver[e];
                        if ("function" == typeof n) {
                            var r = this.receiver[t];
                            n.call(this.receiver, r);
                        }
                    }),
                    Object.defineProperty(t.prototype, "valueDescriptors", {
                        get: function () {
                            var t = this.valueDescriptorMap;
                            return Object.keys(t).map(function (e) {
                                return t[e];
                            });
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    t
                );
            })(),
            C = (function () {
                function t(t, e) {
                    (this.module = t), (this.scope = e), (this.controller = new t.controllerConstructor(this)), (this.bindingObserver = new T(this, this.dispatcher)), (this.valueObserver = new S(this, this.controller));
                    try {
                        this.controller.initialize();
                    } catch (l) {
                        this.handleError(l, "initializing controller");
                    }
                }
                return (
                    (t.prototype.connect = function () {
                        this.bindingObserver.start(), this.valueObserver.start();
                        try {
                            this.controller.connect();
                        } catch (l) {
                            this.handleError(l, "connecting controller");
                        }
                    }),
                    (t.prototype.disconnect = function () {
                        try {
                            this.controller.disconnect();
                        } catch (l) {
                            this.handleError(l, "disconnecting controller");
                        }
                        this.valueObserver.stop(), this.bindingObserver.stop();
                    }),
                    Object.defineProperty(t.prototype, "application", {
                        get: function () {
                            return this.module.application;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "identifier", {
                        get: function () {
                            return this.module.identifier;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "schema", {
                        get: function () {
                            return this.application.schema;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "dispatcher", {
                        get: function () {
                            return this.application.dispatcher;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "element", {
                        get: function () {
                            return this.scope.element;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "parentElement", {
                        get: function () {
                            return this.element.parentElement;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    (t.prototype.handleError = function (t, e, n) {
                        void 0 === n && (n = {});
                        var r = this,
                            i = r.identifier,
                            o = r.controller,
                            s = r.element;
                        (n = Object.assign({ identifier: i, controller: o, element: s }, n)), this.application.handleError(t, "Error " + e, n);
                    }),
                    t
                );
            })();
        function k(t, e) {
            var n = O(t);
            return Array.from(
                n.reduce(function (t, n) {
                    return (
                        (function (t, e) {
                            var n = t[e];
                            return Array.isArray(n) ? n : [];
                        })(n, e).forEach(function (e) {
                            return t.add(e);
                        }),
                        t
                    );
                }, new Set())
            );
        }
        function x(t, e) {
            return O(t).reduce(function (t, n) {
                return (
                    t.push.apply(
                        t,
                        (function (t, e) {
                            var n = t[e];
                            return n
                                ? Object.keys(n).map(function (t) {
                                    return [t, n[t]];
                                })
                                : [];
                        })(n, e)
                    ),
                    t
                );
            }, []);
        }
        function O(t) {
            for (var e = []; t;) e.push(t), (t = Object.getPrototypeOf(t));
            return e.reverse();
        }
        var L = (function () {
            var t = function (e, n) {
                return (t =
                    Object.setPrototypeOf ||
                    ({ __proto__: [] } instanceof Array &&
                        function (t, e) {
                            t.__proto__ = e;
                        }) ||
                    function (t, e) {
                        for (var n in e) e.hasOwnProperty(n) && (t[n] = e[n]);
                    })(e, n);
            };
            return function (e, n) {
                function r() {
                    this.constructor = e;
                }
                t(e, n), (e.prototype = null === n ? Object.create(n) : ((r.prototype = n.prototype), new r()));
            };
        })();
        function A(t) {
            return (function (t, e) {
                var n = F(t),
                    r = (function (t, e) {
                        return P(e).reduce(function (n, r) {
                            var i,
                                o = (function (t, e, n) {
                                    var r = Object.getOwnPropertyDescriptor(t, n);
                                    if (!r || !("value" in r)) {
                                        var i = Object.getOwnPropertyDescriptor(e, n).value;
                                        return r && ((i.get = r.get || i.get), (i.set = r.set || i.set)), i;
                                    }
                                })(t, e, r);
                            return o && Object.assign(n, (((i = {})[r] = o), i)), n;
                        }, {});
                    })(t.prototype, e);
                return Object.defineProperties(n.prototype, r), n;
            })(
                t,
                (function (t) {
                    return k(t, "blessings").reduce(function (e, n) {
                        var r = n(t);
                        for (var i in r) {
                            var o = e[i] || {};
                            e[i] = Object.assign(o, r[i]);
                        }
                        return e;
                    }, {});
                })(t)
            );
        }
        var P =
            "function" == typeof Object.getOwnPropertySymbols
                ? function (t) {
                    return Object.getOwnPropertyNames(t).concat(Object.getOwnPropertySymbols(t));
                }
                : Object.getOwnPropertyNames,
            F = (function () {
                function t(t) {
                    function e() {
                        var n = this && this instanceof e ? this.constructor : void 0;
                        return Reflect.construct(t, arguments, n);
                    }
                    return (e.prototype = Object.create(t.prototype, { constructor: { value: e } })), Reflect.setPrototypeOf(e, t), e;
                }
                try {
                    return (
                        ((e = t(function () {
                            this.a.call(this);
                        })).prototype.a = function () { }),
                        new e(),
                        t
                    );
                } catch (l) {
                    return function (t) {
                        return (function (t) {
                            function e() {
                                return (null !== t && t.apply(this, arguments)) || this;
                            }
                            return L(e, t), e;
                        })(t);
                    };
                }
                var e;
            })();
        var j = (function () {
            function t(t, e) {
                (this.application = t),
                    (this.definition = (function (t) {
                        return { identifier: t.identifier, controllerConstructor: A(t.controllerConstructor) };
                    })(e)),
                    (this.contextsByScope = new WeakMap()),
                    (this.connectedContexts = new Set());
            }
            return (
                Object.defineProperty(t.prototype, "identifier", {
                    get: function () {
                        return this.definition.identifier;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "controllerConstructor", {
                    get: function () {
                        return this.definition.controllerConstructor;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "contexts", {
                    get: function () {
                        return Array.from(this.connectedContexts);
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                (t.prototype.connectContextForScope = function (t) {
                    var e = this.fetchContextForScope(t);
                    this.connectedContexts.add(e), e.connect();
                }),
                (t.prototype.disconnectContextForScope = function (t) {
                    var e = this.contextsByScope.get(t);
                    e && (this.connectedContexts.delete(e), e.disconnect());
                }),
                (t.prototype.fetchContextForScope = function (t) {
                    var e = this.contextsByScope.get(t);
                    return e || ((e = new C(this, t)), this.contextsByScope.set(t, e)), e;
                }),
                t
            );
        })(),
            M = (function () {
                function t(t) {
                    this.scope = t;
                }
                return (
                    (t.prototype.has = function (t) {
                        return this.data.has(this.getDataKey(t));
                    }),
                    (t.prototype.get = function (t) {
                        return this.data.get(this.getDataKey(t));
                    }),
                    (t.prototype.getAttributeName = function (t) {
                        return this.data.getAttributeNameForKey(this.getDataKey(t));
                    }),
                    (t.prototype.getDataKey = function (t) {
                        return t + "-class";
                    }),
                    Object.defineProperty(t.prototype, "data", {
                        get: function () {
                            return this.scope.data;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    t
                );
            })();
        function I(t) {
            return t.replace(/(?:[_-])([a-z0-9])/g, function (t, e) {
                return e.toUpperCase();
            });
        }
        function R(t) {
            return t.charAt(0).toUpperCase() + t.slice(1);
        }
        function B(t) {
            return t.replace(/([A-Z])/g, function (t, e) {
                return "-" + e.toLowerCase();
            });
        }
        var D = (function () {
            function t(t) {
                this.scope = t;
            }
            return (
                Object.defineProperty(t.prototype, "element", {
                    get: function () {
                        return this.scope.element;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "identifier", {
                    get: function () {
                        return this.scope.identifier;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                (t.prototype.get = function (t) {
                    var e = this.getAttributeNameForKey(t);
                    return this.element.getAttribute(e);
                }),
                (t.prototype.set = function (t, e) {
                    var n = this.getAttributeNameForKey(t);
                    return this.element.setAttribute(n, e), this.get(t);
                }),
                (t.prototype.has = function (t) {
                    var e = this.getAttributeNameForKey(t);
                    return this.element.hasAttribute(e);
                }),
                (t.prototype.delete = function (t) {
                    if (this.has(t)) {
                        var e = this.getAttributeNameForKey(t);
                        return this.element.removeAttribute(e), !0;
                    }
                    return !1;
                }),
                (t.prototype.getAttributeNameForKey = function (t) {
                    return "data-" + this.identifier + "-" + B(t);
                }),
                t
            );
        })(),
            N = (function () {
                function t(t) {
                    (this.warnedKeysByObject = new WeakMap()), (this.logger = t);
                }
                return (
                    (t.prototype.warn = function (t, e, n) {
                        var r = this.warnedKeysByObject.get(t);
                        r || ((r = new Set()), this.warnedKeysByObject.set(t, r)), r.has(e) || (r.add(e), this.logger.warn(n, t));
                    }),
                    t
                );
            })();
        function V(t, e) {
            return "[" + t + '~="' + e + '"]';
        }
        var _ = (function () {
            function t(t) {
                this.scope = t;
            }
            return (
                Object.defineProperty(t.prototype, "element", {
                    get: function () {
                        return this.scope.element;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "identifier", {
                    get: function () {
                        return this.scope.identifier;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "schema", {
                    get: function () {
                        return this.scope.schema;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                (t.prototype.has = function (t) {
                    return null != this.find(t);
                }),
                (t.prototype.find = function () {
                    for (var t = this, e = [], n = 0; n < arguments.length; n++) e[n] = arguments[n];
                    return e.reduce(function (e, n) {
                        return e || t.findTarget(n) || t.findLegacyTarget(n);
                    }, void 0);
                }),
                (t.prototype.findAll = function () {
                    for (var t = this, e = [], n = 0; n < arguments.length; n++) e[n] = arguments[n];
                    return e.reduce(function (e, n) {
                        return e.concat(t.findAllTargets(n), t.findAllLegacyTargets(n));
                    }, []);
                }),
                (t.prototype.findTarget = function (t) {
                    var e = this.getSelectorForTargetName(t);
                    return this.scope.findElement(e);
                }),
                (t.prototype.findAllTargets = function (t) {
                    var e = this.getSelectorForTargetName(t);
                    return this.scope.findAllElements(e);
                }),
                (t.prototype.getSelectorForTargetName = function (t) {
                    return V("data-" + this.identifier + "-target", t);
                }),
                (t.prototype.findLegacyTarget = function (t) {
                    var e = this.getLegacySelectorForTargetName(t);
                    return this.deprecate(this.scope.findElement(e), t);
                }),
                (t.prototype.findAllLegacyTargets = function (t) {
                    var e = this,
                        n = this.getLegacySelectorForTargetName(t);
                    return this.scope.findAllElements(n).map(function (n) {
                        return e.deprecate(n, t);
                    });
                }),
                (t.prototype.getLegacySelectorForTargetName = function (t) {
                    var e = this.identifier + "." + t;
                    return V(this.schema.targetAttribute, e);
                }),
                (t.prototype.deprecate = function (t, e) {
                    if (t) {
                        var n = this.identifier,
                            r = this.schema.targetAttribute;
                        this.guide.warn(
                            t,
                            "target:" + e,
                            "Please replace " + r + '="' + n + "." + e + '" with data-' + n + '-target="' + e + '". The ' + r + " attribute is deprecated and will be removed in a future version of Stimulus."
                        );
                    }
                    return t;
                }),
                Object.defineProperty(t.prototype, "guide", {
                    get: function () {
                        return this.scope.guide;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                t
            );
        })(),
            q = (function () {
                function t(t, e, n, r) {
                    (this.targets = new _(this)), (this.classes = new M(this)), (this.data = new D(this)), (this.schema = t), (this.element = e), (this.identifier = n), (this.guide = new N(r));
                }
                return (
                    (t.prototype.findElement = function (t) {
                        return this.findAllElements(t)[0];
                    }),
                    (t.prototype.findAllElements = function (t) {
                        var e = this.element.matches(t) ? [this.element] : [],
                            n = this.filterElements(Array.from(this.element.querySelectorAll(t)));
                        return e.concat(n);
                    }),
                    (t.prototype.filterElements = function (t) {
                        var e = this;
                        return t.filter(function (t) {
                            return e.containsElement(t);
                        });
                    }),
                    (t.prototype.containsElement = function (t) {
                        return t.closest(this.controllerSelector) === this.element;
                    }),
                    Object.defineProperty(t.prototype, "controllerSelector", {
                        get: function () {
                            return V(this.schema.controllerAttribute, this.identifier);
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    t
                );
            })(),
            H = (function () {
                function t(t, e, n) {
                    (this.element = t),
                        (this.schema = e),
                        (this.delegate = n),
                        (this.valueListObserver = new E(this.element, this.controllerAttribute, this)),
                        (this.scopesByIdentifierByElement = new WeakMap()),
                        (this.scopeReferenceCounts = new WeakMap());
                }
                return (
                    (t.prototype.start = function () {
                        this.valueListObserver.start();
                    }),
                    (t.prototype.stop = function () {
                        this.valueListObserver.stop();
                    }),
                    Object.defineProperty(t.prototype, "controllerAttribute", {
                        get: function () {
                            return this.schema.controllerAttribute;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    (t.prototype.parseValueForToken = function (t) {
                        var e = t.element,
                            n = t.content,
                            r = this.fetchScopesByIdentifierForElement(e),
                            i = r.get(n);
                        return i || ((i = this.delegate.createScopeForElementAndIdentifier(e, n)), r.set(n, i)), i;
                    }),
                    (t.prototype.elementMatchedValue = function (t, e) {
                        var n = (this.scopeReferenceCounts.get(e) || 0) + 1;
                        this.scopeReferenceCounts.set(e, n), 1 == n && this.delegate.scopeConnected(e);
                    }),
                    (t.prototype.elementUnmatchedValue = function (t, e) {
                        var n = this.scopeReferenceCounts.get(e);
                        n && (this.scopeReferenceCounts.set(e, n - 1), 1 == n && this.delegate.scopeDisconnected(e));
                    }),
                    (t.prototype.fetchScopesByIdentifierForElement = function (t) {
                        var e = this.scopesByIdentifierByElement.get(t);
                        return e || ((e = new Map()), this.scopesByIdentifierByElement.set(t, e)), e;
                    }),
                    t
                );
            })(),
            z = (function () {
                function t(t) {
                    (this.application = t), (this.scopeObserver = new H(this.element, this.schema, this)), (this.scopesByIdentifier = new b()), (this.modulesByIdentifier = new Map());
                }
                return (
                    Object.defineProperty(t.prototype, "element", {
                        get: function () {
                            return this.application.element;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "schema", {
                        get: function () {
                            return this.application.schema;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "logger", {
                        get: function () {
                            return this.application.logger;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "controllerAttribute", {
                        get: function () {
                            return this.schema.controllerAttribute;
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "modules", {
                        get: function () {
                            return Array.from(this.modulesByIdentifier.values());
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    Object.defineProperty(t.prototype, "contexts", {
                        get: function () {
                            return this.modules.reduce(function (t, e) {
                                return t.concat(e.contexts);
                            }, []);
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    (t.prototype.start = function () {
                        this.scopeObserver.start();
                    }),
                    (t.prototype.stop = function () {
                        this.scopeObserver.stop();
                    }),
                    (t.prototype.loadDefinition = function (t) {
                        this.unloadIdentifier(t.identifier);
                        var e = new j(this.application, t);
                        this.connectModule(e);
                    }),
                    (t.prototype.unloadIdentifier = function (t) {
                        var e = this.modulesByIdentifier.get(t);
                        e && this.disconnectModule(e);
                    }),
                    (t.prototype.getContextForElementAndIdentifier = function (t, e) {
                        var n = this.modulesByIdentifier.get(e);
                        if (n)
                            return n.contexts.find(function (e) {
                                return e.element == t;
                            });
                    }),
                    (t.prototype.handleError = function (t, e, n) {
                        this.application.handleError(t, e, n);
                    }),
                    (t.prototype.createScopeForElementAndIdentifier = function (t, e) {
                        return new q(this.schema, t, e, this.logger);
                    }),
                    (t.prototype.scopeConnected = function (t) {
                        this.scopesByIdentifier.add(t.identifier, t);
                        var e = this.modulesByIdentifier.get(t.identifier);
                        e && e.connectContextForScope(t);
                    }),
                    (t.prototype.scopeDisconnected = function (t) {
                        this.scopesByIdentifier.delete(t.identifier, t);
                        var e = this.modulesByIdentifier.get(t.identifier);
                        e && e.disconnectContextForScope(t);
                    }),
                    (t.prototype.connectModule = function (t) {
                        this.modulesByIdentifier.set(t.identifier, t),
                            this.scopesByIdentifier.getValuesForKey(t.identifier).forEach(function (e) {
                                return t.connectContextForScope(e);
                            });
                    }),
                    (t.prototype.disconnectModule = function (t) {
                        this.modulesByIdentifier.delete(t.identifier),
                            this.scopesByIdentifier.getValuesForKey(t.identifier).forEach(function (e) {
                                return t.disconnectContextForScope(e);
                            });
                    }),
                    t
                );
            })(),
            U = { controllerAttribute: "data-controller", actionAttribute: "data-action", targetAttribute: "data-target", classAttribute: "data-class" },
            W = function (t, e, n, r) {
                return new (n || (n = Promise))(function (i, o) {
                    function s(t) {
                        try {
                            c(r.next(t));
                        } catch (e) {
                            o(e);
                        }
                    }
                    function a(t) {
                        try {
                            c(r.throw(t));
                        } catch (e) {
                            o(e);
                        }
                    }
                    function c(t) {
                        t.done
                            ? i(t.value)
                            : new n(function (e) {
                                e(t.value);
                            }).then(s, a);
                    }
                    c((r = r.apply(t, e || [])).next());
                });
            },
            K = function (t, e) {
                var n,
                    r,
                    i,
                    o,
                    s = {
                        label: 0,
                        sent: function () {
                            if (1 & i[0]) throw i[1];
                            return i[1];
                        },
                        trys: [],
                        ops: [],
                    };
                return (
                    (o = { next: a(0), throw: a(1), return: a(2) }),
                    "function" === typeof Symbol &&
                    (o[Symbol.iterator] = function () {
                        return this;
                    }),
                    o
                );
                function a(o) {
                    return function (a) {
                        return (function (o) {
                            if (n) throw new TypeError("Generator is already executing.");
                            for (; s;)
                                try {
                                    if (((n = 1), r && (i = 2 & o[0] ? r.return : o[0] ? r.throw || ((i = r.return) && i.call(r), 0) : r.next) && !(i = i.call(r, o[1])).done)) return i;
                                    switch (((r = 0), i && (o = [2 & o[0], i.value]), o[0])) {
                                        case 0:
                                        case 1:
                                            i = o;
                                            break;
                                        case 4:
                                            return s.label++, { value: o[1], done: !1 };
                                        case 5:
                                            s.label++, (r = o[1]), (o = [0]);
                                            continue;
                                        case 7:
                                            (o = s.ops.pop()), s.trys.pop();
                                            continue;
                                        default:
                                            if (!(i = (i = s.trys).length > 0 && i[i.length - 1]) && (6 === o[0] || 2 === o[0])) {
                                                s = 0;
                                                continue;
                                            }
                                            if (3 === o[0] && (!i || (o[1] > i[0] && o[1] < i[3]))) {
                                                s.label = o[1];
                                                break;
                                            }
                                            if (6 === o[0] && s.label < i[1]) {
                                                (s.label = i[1]), (i = o);
                                                break;
                                            }
                                            if (i && s.label < i[2]) {
                                                (s.label = i[2]), s.ops.push(o);
                                                break;
                                            }
                                            i[2] && s.ops.pop(), s.trys.pop();
                                            continue;
                                    }
                                    o = e.call(t, s);
                                } catch (a) {
                                    (o = [6, a]), (r = 0);
                                } finally {
                                    n = i = 0;
                                }
                            if (5 & o[0]) throw o[1];
                            return { value: o[0] ? o[1] : void 0, done: !0 };
                        })([o, a]);
                    };
                }
            },
            J = (function () {
                function t(t, e) {
                    void 0 === t && (t = document.documentElement), void 0 === e && (e = U), (this.logger = console), (this.element = t), (this.schema = e), (this.dispatcher = new i(this)), (this.router = new z(this));
                }
                return (
                    (t.start = function (e, n) {
                        var r = new t(e, n);
                        return r.start(), r;
                    }),
                    (t.prototype.start = function () {
                        return W(this, void 0, void 0, function () {
                            return K(this, function (t) {
                                switch (t.label) {
                                    case 0:
                                        return [
                                            4,
                                            new Promise(function (t) {
                                                "loading" == document.readyState ? document.addEventListener("DOMContentLoaded", t) : t();
                                            }),
                                        ];
                                    case 1:
                                        return t.sent(), this.dispatcher.start(), this.router.start(), [2];
                                }
                            });
                        });
                    }),
                    (t.prototype.stop = function () {
                        this.dispatcher.stop(), this.router.stop();
                    }),
                    (t.prototype.register = function (t, e) {
                        this.load({ identifier: t, controllerConstructor: e });
                    }),
                    (t.prototype.load = function (t) {
                        for (var e = this, n = [], r = 1; r < arguments.length; r++) n[r - 1] = arguments[r];
                        var i = Array.isArray(t) ? t : [t].concat(n);
                        i.forEach(function (t) {
                            return e.router.loadDefinition(t);
                        });
                    }),
                    (t.prototype.unload = function (t) {
                        for (var e = this, n = [], r = 1; r < arguments.length; r++) n[r - 1] = arguments[r];
                        var i = Array.isArray(t) ? t : [t].concat(n);
                        i.forEach(function (t) {
                            return e.router.unloadIdentifier(t);
                        });
                    }),
                    Object.defineProperty(t.prototype, "controllers", {
                        get: function () {
                            return this.router.contexts.map(function (t) {
                                return t.controller;
                            });
                        },
                        enumerable: !0,
                        configurable: !0,
                    }),
                    (t.prototype.getControllerForElementAndIdentifier = function (t, e) {
                        var n = this.router.getContextForElementAndIdentifier(t, e);
                        return n ? n.controller : null;
                    }),
                    (t.prototype.handleError = function (t, e, n) {
                        this.logger.error("%s\n\n%o\n\n%o", e, t, n);
                    }),
                    t
                );
            })();
        function Y(t) {
            return k(t, "classes").reduce(function (t, e) {
                return Object.assign(
                    t,
                    (((r = {})[(i = (n = e) + "Class")] = {
                        get: function () {
                            var t = this.classes;
                            if (t.has(n)) return t.get(n);
                            var e = t.getAttributeName(n);
                            throw new Error('Missing attribute "' + e + '"');
                        },
                    }),
                        (r["has" + R(i)] = {
                            get: function () {
                                return this.classes.has(n);
                            },
                        }),
                        r)
                );
                var n, r, i;
            }, {});
        }
        function Q(t) {
            return k(t, "targets").reduce(function (t, e) {
                return Object.assign(
                    t,
                    (((r = {})[(n = e) + "Target"] = {
                        get: function () {
                            var t = this.targets.find(n);
                            if (t) return t;
                            throw new Error('Missing target element "' + this.identifier + "." + n + '"');
                        },
                    }),
                        (r[n + "Targets"] = {
                            get: function () {
                                return this.targets.findAll(n);
                            },
                        }),
                        (r["has" + R(n) + "Target"] = {
                            get: function () {
                                return this.targets.has(n);
                            },
                        }),
                        r)
                );
                var n, r;
            }, {});
        }
        function $(t) {
            var e = x(t, "values"),
                n = {
                    valueDescriptorMap: {
                        get: function () {
                            var t = this;
                            return e.reduce(function (e, n) {
                                var r,
                                    i = X(n),
                                    o = t.data.getAttributeNameForKey(i.key);
                                return Object.assign(e, (((r = {})[o] = i), r));
                            }, {});
                        },
                    },
                };
            return e.reduce(function (t, e) {
                return Object.assign(
                    t,
                    (function (t) {
                        var e,
                            n = X(t),
                            r = n.type,
                            i = n.key,
                            o = n.name,
                            s = G[r],
                            a = tt[r] || tt.default;
                        return (
                            ((e = {})[o] = {
                                get: function () {
                                    var t = this.data.get(i);
                                    return null !== t ? s(t) : n.defaultValue;
                                },
                                set: function (t) {
                                    void 0 === t ? this.data.delete(i) : this.data.set(i, a(t));
                                },
                            }),
                            (e["has" + R(o)] = {
                                get: function () {
                                    return this.data.has(i);
                                },
                            }),
                            e
                        );
                    })(e)
                );
            }, n);
        }
        function X(t) {
            return (function (t, e) {
                var n = "array" == e ? "values" : "value",
                    r = B(t) + "-" + n;
                return {
                    type: e,
                    key: r,
                    name: I(r),
                    get defaultValue() {
                        return Z[e];
                    },
                };
            })(
                t[0],
                (function (t) {
                    switch (t) {
                        case Array:
                            return "array";
                        case Boolean:
                            return "boolean";
                        case Number:
                            return "number";
                        case Object:
                            return "object";
                        case String:
                            return "string";
                    }
                    throw new Error('Unknown value type constant "' + t + '"');
                })(t[1])
            );
        }
        var Z = {
            get array() {
                return [];
            },
            boolean: !1,
            number: 0,
            get object() {
                return {};
            },
            string: "",
        },
            G = {
                array: function (t) {
                    var e = JSON.parse(t);
                    if (!Array.isArray(e)) throw new TypeError("Expected array");
                    return e;
                },
                boolean: function (t) {
                    return !("0" == t || "false" == t);
                },
                number: function (t) {
                    return parseFloat(t);
                },
                object: function (t) {
                    var e = JSON.parse(t);
                    if (null === e || "object" != typeof e || Array.isArray(e)) throw new TypeError("Expected object");
                    return e;
                },
                string: function (t) {
                    return t;
                },
            },
            tt = {
                default: function (t) {
                    return "" + t;
                },
                array: et,
                object: et,
            };
        function et(t) {
            return JSON.stringify(t);
        }
        var nt = (function () {
            function t(t) {
                this.context = t;
            }
            return (
                Object.defineProperty(t.prototype, "application", {
                    get: function () {
                        return this.context.application;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "scope", {
                    get: function () {
                        return this.context.scope;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "element", {
                    get: function () {
                        return this.scope.element;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "identifier", {
                    get: function () {
                        return this.scope.identifier;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "targets", {
                    get: function () {
                        return this.scope.targets;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "classes", {
                    get: function () {
                        return this.scope.classes;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                Object.defineProperty(t.prototype, "data", {
                    get: function () {
                        return this.scope.data;
                    },
                    enumerable: !0,
                    configurable: !0,
                }),
                (t.prototype.initialize = function () { }),
                (t.prototype.connect = function () { }),
                (t.prototype.disconnect = function () { }),
                (t.blessings = [Y, Q, $]),
                (t.targets = []),
                (t.values = {}),
                t
            );
        })();
    },
    function (t, e, n) {
        "use strict";
        n.d(e, "a", function () {
            return g;
        });
        var r = {};
        function i(t, e) {
            t.append(e);
        }
        function o(t, e) {
            t.prepend(e);
        }
        function s(t, e) {
            t.replaceWith(e);
        }
        function a(t, e) {
            (t.innerHTML = ""), t.append(e);
        }
        function c(t) {
            t.remove();
        }
        n.r(r),
            n.d(r, "append", function () {
                return i;
            }),
            n.d(r, "prepend", function () {
                return o;
            }),
            n.d(r, "replace", function () {
                return s;
            }),
            n.d(r, "update", function () {
                return a;
            }),
            n.d(r, "remove", function () {
                return c;
            });
        class l {
            constructor(t, e, n) {
                (this.type = t), (this.id = e), (this.content = n);
            }
            perform() {
                const t = r[this.type];
                if (!t) return;
                const e = ((n = this.id), document.getElementById(n));
                var n;
                e && t(e, this.content);
            }
        }
        const u = "data-page-update",
            h = "template[" + u + "]";
        function d(t) {
            return Array.from(
                (function (t) {
                    return (function (t) {
                        return document.createRange().createContextualFragment(t);
                    })(t).querySelectorAll(h);
                })(t),
                p
            );
        }
        function p(t) {
            const [e, n] = t.getAttribute(u).split("#");
            return new l(e, n, t.content);
        }
        const f = [];
        let m;
        function g(t) {
            d(t).forEach(y);
        }
        function y(t) {
            f.push(t),
                (function () {
                    if (m) return;
                    m = requestAnimationFrame(() => {
                        for (m = null; f.length;) f.shift().perform();
                    });
                })();
        }
    },
    function (t, e, n) {
        "use strict";
        function r(t) {
            const e = "==".slice(0, (4 - (t.length % 4)) % 4),
                n = t.replace(/-/g, "+").replace(/_/g, "/") + e,
                r = atob(n),
                i = new ArrayBuffer(r.length),
                o = new Uint8Array(i);
            for (let s = 0; s < r.length; s++) o[s] = r.charCodeAt(s);
            return i;
        }
        function i(t) {
            const e = new Uint8Array(t);
            let n = "";
            for (const r of e) n += String.fromCharCode(r);
            return btoa(n).replace(/\+/g, "-").replace(/\//g, "_").replace(/=/g, "");
        }
        n.d(e, "a", function () {
            return y;
        }),
            n.d(e, "b", function () {
                return b;
            }),
            n.d(e, "c", function () {
                return v;
            });
        const o = "copy",
            s = "convert";
        function a(t, e, n) {
            if (e === o) return n;
            if (e === s) return t(n);
            if (e instanceof Array) return n.map((n) => a(t, e[0], n));
            if (e instanceof Object) {
                const r = {};
                for (const [i, o] of Object.entries(e))
                    if (i in n) null != n[i] ? (r[i] = a(t, o.schema, n[i])) : (r[i] = null);
                    else if (o.required) throw new Error("Missing key: " + i);
                return r;
            }
        }
        function c(t) {
            return { required: !0, schema: t };
        }
        function l(t) {
            return { required: !1, schema: t };
        }
        const u = { type: c(o), id: c(s), transports: l(o) },
            h = { appid: l(o), appidExclude: l(o) },
            d = { appid: l(o), appidExclude: l(o) },
            p = {
                publicKey: c({
                    rp: c(o),
                    user: c({ id: c(s), name: c(o), displayName: c(o) }),
                    challenge: c(s),
                    pubKeyCredParams: c(o),
                    timeout: l(o),
                    excludeCredentials: l([u]),
                    authenticatorSelection: l(o),
                    attestation: l(o),
                    extensions: l(h),
                }),
                signal: l(o),
            },
            f = { type: c(o), id: c(o), rawId: c(s), response: c({ clientDataJSON: c(s), attestationObject: c(s) }), clientExtensionResults: c(d) },
            m = { mediation: l(o), publicKey: c({ challenge: c(s), timeout: l(o), rpId: l(o), allowCredentials: l([u]), userVerification: l(o), extensions: l(h) }), signal: l(o) },
            g = { type: c(o), id: c(o), rawId: c(s), response: c({ clientDataJSON: c(s), authenticatorData: c(s), signature: c(s), userHandle: c(s) }), clientExtensionResults: c(d) };
        async function y(t) {
            const e = a(r, p, t),
                n = await navigator.credentials.create(e);
            return (n.clientExtensionResults = n.getClientExtensionResults()), a(i, f, n);
        }
        async function b(t) {
            const e = a(r, m, t),
                n = await navigator.credentials.get(e);
            return (n.clientExtensionResults = n.getClientExtensionResults()), a(i, g, n);
        }
        function v() {
            return !!(navigator.credentials && navigator.credentials.create && navigator.credentials.get && window.PublicKeyCredential);
        }
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return r;
                });
                class r extends t {
                    initialize() {
                        this.observeMutations(this.refreshSizeValue);
                    }
                    refreshSizeValue() {
                        this.sizeValue = this.itemTargets.length;
                    }
                }
                (r.targets = ["item"]), (r.values = { size: Number });
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        function r(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
        }
        function i(t, e) {
            for (var n = 0; n < e.length; n++) {
                var r = e[n];
                (r.enumerable = r.enumerable || !1), (r.configurable = !0), "value" in r && (r.writable = !0), Object.defineProperty(t, r.key, r);
            }
        }
        function o(t, e, n) {
            return e && i(t.prototype, e), n && i(t, n), t;
        }
        function s(t, e, n) {
            return e in t ? Object.defineProperty(t, e, { value: n, enumerable: !0, configurable: !0, writable: !0 }) : (t[e] = n), t;
        }
        n.d(e, "a", function () {
            return m;
        }),
            n.d(e, "b", function () {
                return g;
            });
        var a = (function () {
            function t(e) {
                r(this, t), s(this, "parent", void 0), s(this, "children", []), (this.parent = e);
            }
            return (
                o(t, [
                    {
                        key: "delete",
                        value: function (t) {
                            var e = this.children.indexOf(t);
                            return -1 !== e && ((this.children = this.children.slice(0, e).concat(this.children.slice(e + 1))), 0 === this.children.length && this.parent.delete(this), !0);
                        },
                    },
                    {
                        key: "add",
                        value: function (t) {
                            return this.children.push(t), this;
                        },
                    },
                ]),
                t
            );
        })();
        function c(t) {
            if (!(t instanceof HTMLElement)) return !1;
            var e = t.nodeName.toLowerCase(),
                n = (t.getAttribute("type") || "").toLowerCase();
            return "select" === e || "textarea" === e || ("input" === e && "submit" !== n && "reset" !== n && "checkbox" !== n && "radio" !== n) || t.isContentEditable;
        }
        var l = new ((function () {
            function t(e) {
                r(this, t), s(this, "parent", null), s(this, "children", {}), (this.parent = e || null);
            }
            return (
                o(t, [
                    {
                        key: "get",
                        value: function (t) {
                            return this.children[t];
                        },
                    },
                    {
                        key: "insert",
                        value: function (e) {
                            for (var n = this, r = 0; r < e.length; r += 1) {
                                var i = e[r],
                                    o = n.get(i);
                                if (r === e.length - 1) return o instanceof t && (n.delete(o), (o = null)), o || ((o = new a(n)), (n.children[i] = o)), o;
                                o instanceof a && (o = null), o || ((o = new t(n)), (n.children[i] = o)), (n = o);
                            }
                            return n;
                        },
                    },
                    {
                        key: "delete",
                        value: function (t) {
                            for (var e in this.children) {
                                if (this.children[e] === t) {
                                    var n = delete this.children[e];
                                    return 0 === Object.keys(this.children).length && this.parent && this.parent.delete(this), n;
                                }
                            }
                            return !1;
                        },
                    },
                ]),
                t
            );
        })())(),
            u = new WeakMap(),
            h = l,
            d = null;
        function p() {
            (d = null), (h = l);
        }
        function f(t) {
            if (!(t.target instanceof Node && c(t.target))) {
                null != d && clearTimeout(d), (d = setTimeout(p, 1500));
                var e,
                    n = h.get(
                        (function (t) {
                            return ""
                                .concat(t.ctrlKey ? "Control+" : "")
                                .concat(t.altKey ? "Alt+" : "")
                                .concat(t.metaKey ? "Meta+" : "")
                                .concat(t.key);
                        })(t)
                    );
                if (n) return (h = n), n instanceof a ? (c((e = n.children[n.children.length - 1])) ? e.focus() : e.click(), t.preventDefault(), void p()) : void 0;
                p();
            }
        }
        function m(t, e) {
            0 === Object.keys(l.children).length && document.addEventListener("keydown", f);
            var n = (function (t) {
                return t.split(",").map(function (t) {
                    return t.split(" ");
                });
            })(e || t.getAttribute("data-hotkey") || "").map(function (e) {
                return l.insert(e).add(t);
            });
            u.set(t, n);
        }
        function g(t) {
            var e = u.get(t);
            if (e && e.length) {
                var n = !0,
                    r = !1,
                    i = void 0;
                try {
                    for (var o, s = e[Symbol.iterator](); !(n = (o = s.next()).done); n = !0) {
                        var a = o.value;
                        a && a.delete(t);
                    }
                } catch (c) {
                    (r = !0), (i = c);
                } finally {
                    try {
                        n || null == s.return || s.return();
                    } finally {
                        if (r) throw i;
                    }
                }
            }
            0 === Object.keys(l.children).length && document.removeEventListener("keydown", f);
        }
    },
    function (t, e, n) {
        const r = n(16);
        r.keys().forEach(r);
    },
    function (t, e, n) {
        "use strict";
        function r(t) {
            return t
                .keys()
                .map(function (e) {
                    return (function (t, e) {
                        var n = (function (t) {
                            var e = (t.match(/^(?:\.\/)?(.+)(?:[_-]controller\..+?)$/) || [])[1];
                            if (e) return e.replace(/_/g, "-").replace(/\//g, "--");
                        })(e);
                        if (n)
                            return (function (t, e) {
                                var n = t.default;
                                if ("function" == typeof n) return { identifier: e, controllerConstructor: n };
                            })(t(e), n);
                    })(t, e);
                })
                .filter(function (t) {
                    return t;
                });
        }
        n.d(e, "a", function () {
            return r;
        });
    },
    function (t, e, n) {
        "use strict";
        n.d(e, "a", function () {
            return r;
        });
        class r {
            constructor(t) {
                this.element = t;
            }
            save() {
                const t = Date.now(),
                    e = new URLSearchParams(this.formData).toString();
                localStorage.setItem(this.key, JSON.stringify({ time: t, params: e }));
            }
            restore() {
                const { params: t } = JSON.parse(localStorage.getItem(this.key));
                for (const [e, n] of new URLSearchParams(t)) {
                    const t = this.element.elements[e];
                    switch (t.type) {
                        case "checkbox":
                            t.checked = !!n;
                            break;
                        case "select-multiple":
                            t[t.options.length] = new Option(n, n, !0, !0);
                            break;
                        default: {
                            const e = this.element.querySelector('trix-editor[input="' + t.id + '"]');
                            e ? (e.value = n) : (t.value = n);
                            break;
                        }
                    }
                }
            }
            discard() {
                localStorage.removeItem(this.key);
            }
            get isSaved() {
                return this.key in localStorage;
            }
            get key() {
                return "localform::" + this.pathname;
            }
            get pathname() {
                return new URL(this.element.action, location).pathname;
            }
            get formData() {
                return new FormData(this.element);
            }
        }
    },
    function (t, e, n) {
        "use strict";
        n.d(e, "a", function () {
            return g;
        });
        var r = n(0);
        function i(t, e, n) {
            return Object.defineProperty(t, e, { value: n }), n;
        }
        function o(t = "") {
            return t
                .normalize("NFD")
                .replace(/[\u0300-\u036f]/g, "")
                .toLowerCase();
        }
        const s = { block: "nearest", behavior: "scrollBehavior" in document.body.style ? "smooth" : "auto" };
        class a {
            static fromJSON(t) {
                return new a(t);
            }
            static compare(t, e) {
                return t.compare(e);
            }
            constructor([t, e, n]) {
                (this.value = t), (this.label = e || t), (this.detail = n || t);
            }
            compare(t) {
                return this.string.localeCompare(t.string);
            }
            score(t) {
                return (t = o(t)), this.string == t ? 1 : this.string.startsWith(t) ? 2 : this.parts.some((e) => e == t) ? 3 : this.parts.some((e) => e.startsWith(t)) ? 4 : this.string.includes(t) ? 5 : void 0;
            }
            get values() {
                return i(this, "values", this.value.split(","));
            }
            get string() {
                return i(this, "string", o(this.label + " " + this.detail));
            }
            get parts() {
                return i(this, "parts", this.string.split(/\s/));
            }
        }
        class c {
            static async fetch(t, { expireIn: e = 6e4 } = {}) {
                let n = this.index.get(t);
                return n || ((n = c.load(t)), this.index.set(t, n), Object(r.e)(e).then(() => this.index.delete(t))), await n;
            }
            static async load(t) {
                return c.fromJSON(await r.k.getJSON(t));
            }
            static fromJSON(t) {
                return new c(t.map(a.fromJSON));
            }
            constructor(t = [], { sort: e = !0 } = {}) {
                e && t.sort(a.compare), (this.records = t);
            }
            at(t) {
                return this.records[t];
            }
            map(t) {
                return this.records.map(t);
            }
            find(t) {
                return this.records.find((e) => e.value == t);
            }
            indexOf(t) {
                return this.records.findIndex((e) => e.value == t);
            }
            add(t) {
                const e = new a([t]),
                    { records: n } = new c(this.records.concat(e));
                return (this.records = n), e;
            }
            without(...t) {
                const e = this.records.filter((e) => !t.includes(e.value));
                return new c(e, { sort: !1 });
            }
            first(t) {
                const e = this.records.slice(0, t);
                return new c(e, { sort: !1 });
            }
            search(t) {
                if (!t) return new c();
                const e = [];
                for (const n of this.records) {
                    const r = n.score(t);
                    r && (e[r] || (e[r] = []), e[r].push(n));
                }
                return new c(e.flat(), { sort: !1 });
            }
            get length() {
                return this.records.length;
            }
        }
        c.index = new Map();
        class l {
            constructor(t, e) {
                (this.optionsChanged = () => {
                    this.render(), this.notify(), this.dispatch();
                }),
                    (this.renderElementForOption = ({ value: t, label: e }) => {
                        const n = this.template.content.firstElementChild.cloneNode(!0);
                        return (
                            (n.querySelector("[data-value]").dataset.value = t),
                            (n.querySelector("[data-content=label]").textContent = e),
                            (n.querySelector("[data-content=label]").title = t),
                            this.template.insertAdjacentElement("beforebegin", n)
                        );
                    }),
                    (this.combobox = t),
                    (this.element = e),
                    (this.elements = []),
                    (this.observer = new MutationObserver(this.optionsChanged));
            }
            connect() {
                this.observer.observe(this.element, { childList: !0 }), this.render();
            }
            disconnect() {
                this.observer.disconnect(), this.render();
            }
            add(t, e = t) {
                return this.element.append(this.findOption(t) || this.createOption(t, e)), !0;
            }
            remove(t) {
                var e;
                null == (e = this.findOption(t)) || e.remove();
            }
            removeLast() {
                var t;
                null == (t = this.lastOption) || t.remove();
            }
            findOption(t) {
                return this.options.find((e) => e.value == t);
            }
            createOption(t, e) {
                return new Option(e, t, !0, !0);
            }
            get lastOption() {
                return this.options.slice(-1)[0];
            }
            get options() {
                return Array.from(this.element.options);
            }
            get values() {
                return this.options.map((t) => t.value);
            }
            get length() {
                return this.options.length;
            }
            render() {
                for (const t of this.elements) t.remove();
                this.elements = this.element.isConnected ? this.options.map(this.renderElementForOption) : [];
            }
            notify() {
                this.combobox.notifyComponents("selectionChanged");
            }
            dispatch(t = "input", { bubbles: e = !0 } = {}) {
                this.element.dispatchEvent(new CustomEvent(t, { bubbles: e }));
            }
            get template() {
                const t = this.element.getAttribute("data-template-id");
                return i(this, "template", document.getElementById(t));
            }
        }
        let u = 0;
        class h {
            constructor(t, e) {
                (this.combobox = t), (this.element = e), (this.element.id = "autocomplete-list-" + u++), this.element.setAttribute("role", "listbox"), (this.collection = new c());
            }
            disconnect() {
                this.resetCollection();
            }
            resetCollection() {
                this.setCollection(new c());
            }
            setCollection(t) {
                (this.collection = t), this.render();
            }
            render() {
                (this.element.innerHTML = ""), this.element.append(...this.collection.map(d)), (this.element.hidden = !this.collection.length), (this.index = 0);
            }
            get id() {
                return this.element.id;
            }
            get index() {
                return this.elements.findIndex(f);
            }
            set index(t) {
                const { elements: e } = this,
                    n = e.length - 1;
                t > n && (t = 0), t < 0 && (t = n), (this.activeElement = e[t]);
            }
            set activeElement(t) {
                this.elements.forEach(p),
                    t &&
                    (function (t) {
                        t.setAttribute("aria-selected", "true"), t.scrollIntoView(s);
                    })(t),
                    (this.input.activeDescendant = t);
            }
            get input() {
                return this.combobox.input;
            }
            get value() {
                var t;
                return null == (t = this.record) ? void 0 : t.value;
            }
            set value(t) {
                return (this.index = this.collection.indexOf(t));
            }
            get record() {
                return this.collection.at(this.index);
            }
            get elements() {
                return Array.from(this.element.children);
            }
            get length() {
                return this.elements.length;
            }
        }
        function d(t, e) {
            const n = document.createElement("li");
            return (n.id = "autocomplete-option-" + u++ + "-" + e), (n.textContent = t.label), n.setAttribute("data-detail", t.detail), n.setAttribute("data-value", t.value), n.setAttribute("role", "option"), n;
        }
        function p(t) {
            t.setAttribute("aria-selected", "false");
        }
        function f(t) {
            return "true" == t.getAttribute("aria-selected");
        }
        class m {
            constructor(t, e) {
                (this.combobox = t),
                    (this.element = e),
                    this.element.setAttribute("aria-autocomplete", "list"),
                    this.element.setAttribute("aria-controls", t.list.id),
                    this.element.setAttribute("aria-haspopup", "listbox"),
                    this.element.setAttribute("autocomplete", "off"),
                    this.element.setAttribute("role", "combobox"),
                    this.element.setAttribute("spellcheck", !1);
            }
            connect() {
                this.take("required", "multiple"), this.render();
            }
            disconnect() {
                this.give("required", "multiple");
            }
            selectionChanged() {
                this.render();
            }
            render() {
                const { length: t } = this.combobox.selection;
                (this.element.required = this.required && !t), (this.element.hidden = !this.multiple && t);
            }
            take(...t) {
                for (const e of t) (this[e] = this.element[e]), this.element.removeAttribute(e);
            }
            give(...t) {
                for (const e of t) this.element.toggleAttribute(e, this[e]);
            }
            get value() {
                return this.element.value.trim();
            }
            set activeDescendant(t) {
                const e = null == t ? void 0 : t.id;
                e ? this.element.setAttribute("aria-activedescendant", e) : this.element.removeAttribute("aria-activedescendant");
            }
        }
        class g {
            static fromController({ element: t, selectTarget: e, inputTarget: n, listTarget: r, urlValue: i }) {
                const o = new g(t, e, n, r);
                return c.fetch(i).then(o.setCollection), o;
            }
            constructor(t, e, n, r) {
                (this.setCollection = (t) => {
                    (this.collection = t), this.update();
                }),
                    (this.element = t),
                    (this.collection = new c()),
                    (this.selection = new l(this, e)),
                    (this.list = new h(this, r)),
                    (this.input = new m(this, n));
            }
            connect() {
                this.notifyComponents("connect");
            }
            disconnect() {
                this.notifyComponents("disconnect");
            }
            notifyComponents(t, ...e) {
                const n = [this.selection, this.list, this.input];
                for (const i of n) {
                    var r;
                    null == (r = i[t]) || r.call(i, ...e);
                }
            }
            update() {
                this.list.setCollection(
                    this.collection
                        .without(...this.selection.values)
                        .search(this.input.value)
                        .first(20)
                ),
                    this.toggle(this.list.length);
            }
            commit() {
                var t;
                return null == (t = this.list.record) ? void 0 : t.values.every((t) => this.add(t));
            }
            add(t, e) {
                const n = this.collection.find(t);
                return n && (e = n.label), this.selection.add(t, e);
            }
            remove(t) {
                return this.selection.remove(t);
            }
            removeLast() {
                return this.selection.removeLast();
            }
            toggle(t) {
                this.element.setAttribute("aria-expanded", !!t);
            }
            collapse() {
                this.toggle(!1), this.list.resetCollection();
            }
            get index() {
                return this.list.index;
            }
            set index(t) {
                return (this.list.index = t);
            }
            get value() {
                return this.list.value;
            }
            set value(t) {
                return (this.list.value = t);
            }
        }
    },
    function (t, e, n) {
        (window.Turbolinks = n(4)), Turbolinks.start(), n(15).start(), n(10), n(86), n(22);
    },
    function (t, e, n) {
        var r, i;
        (function () {
            (function () {
                (function () {
                    var t = [].slice;
                    this.LocalTime = {
                        config: {},
                        run: function () {
                            return this.getController().processElements();
                        },
                        process: function () {
                            var e, n, r, i;
                            for (r = 0, i = (n = 1 <= arguments.length ? t.call(arguments, 0) : []).length; r < i; r++) (e = n[r]), this.getController().processElement(e);
                            return n.length;
                        },
                        getController: function () {
                            return null != this.controller ? this.controller : (this.controller = new o.Controller());
                        },
                    };
                }.call(this));
            }.call(this));
            var o = this.LocalTime;
            (function () {
                (function () {
                    o.config.i18n = {
                        en: {
                            date: {
                                dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                                abbrDayNames: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                                monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                                abbrMonthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                                yesterday: "yesterday",
                                today: "today",
                                tomorrow: "tomorrow",
                                on: "on {date}",
                                formats: { default: "%b %e, %Y", thisYear: "%b %e" },
                            },
                            time: {
                                am: "am",
                                pm: "pm",
                                singular: "a {time}",
                                singularAn: "an {time}",
                                elapsed: "{time} ago",
                                second: "second",
                                seconds: "seconds",
                                minute: "minute",
                                minutes: "minutes",
                                hour: "hour",
                                hours: "hours",
                                formats: { default: "%l:%M%P" },
                            },
                            datetime: { at: "{date} at {time}", formats: { default: "%B %e, %Y at %l:%M%P %Z" } },
                        },
                    };
                }.call(this),
                    function () {
                        (o.config.locale = "en"), (o.config.defaultLocale = "en");
                    }.call(this),
                    function () {
                        o.config.timerInterval = 6e4;
                    }.call(this),
                    function () {
                        var t, e, n;
                        (n = !isNaN(Date.parse("2011-01-01T12:00:00-05:00"))),
                            (o.parseDate = function (t) {
                                return (t = t.toString()), n || (t = e(t)), new Date(Date.parse(t));
                            }),
                            (t = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})(Z|[-+]?[\d:]+)$/),
                            (e = function (e) {
                                var n, r, i, o, s, a, c, l, u;
                                if ((i = e.match(t)))
                                    return i[0], (l = i[1]), (s = i[2]), (n = i[3]), (r = i[4]), (o = i[5]), (c = i[6]), "Z" !== (u = i[7]) && (a = u.replace(":", "")), l + "/" + s + "/" + n + " " + r + ":" + o + ":" + c + " GMT" + [a];
                            });
                    }.call(this),
                    function () {
                        o.elementMatchesSelector = (function () {
                            var t, e, n, r, i, o;
                            return (
                                (t = document.documentElement),
                                (e = null != (n = null != (r = null != (i = null != (o = t.matches) ? o : t.matchesSelector) ? i : t.webkitMatchesSelector) ? r : t.mozMatchesSelector) ? n : t.msMatchesSelector),
                                function (t, n) {
                                    if ((null != t ? t.nodeType : void 0) === Node.ELEMENT_NODE) return e.call(t, n);
                                }
                            );
                        })();
                    }.call(this),
                    function () {
                        var t, e, n;
                        (t = o.config),
                            (n = t.i18n),
                            (o.getI18nValue = function (r, i) {
                                var s, a;
                                return null == r && (r = ""), (s = (null != i ? i : { locale: t.locale }).locale), null != (a = e(n[s], r)) ? a : s !== t.defaultLocale ? o.getI18nValue(r, { locale: t.defaultLocale }) : void 0;
                            }),
                            (o.translate = function (t, e, n) {
                                var r, i, s;
                                for (r in (null == e && (e = {}), (s = o.getI18nValue(t, n)), e)) (i = e[r]), (s = s.replace("{" + r + "}", i));
                                return s;
                            }),
                            (e = function (t, e) {
                                var n, r, i, o, s;
                                for (s = t, n = 0, i = (o = e.split(".")).length; n < i; n++) {
                                    if (null == s[(r = o[n])]) return null;
                                    s = s[r];
                                }
                                return s;
                            });
                    }.call(this),
                    function () {
                        var t, e, n, r, i;
                        (t = o.getI18nValue),
                            (i = o.translate),
                            (o.strftime = r = function (o, s) {
                                var a, c, l, u, h, d, p;
                                return (
                                    (c = o.getDay()),
                                    (a = o.getDate()),
                                    (h = o.getMonth()),
                                    (p = o.getFullYear()),
                                    (l = o.getHours()),
                                    (u = o.getMinutes()),
                                    (d = o.getSeconds()),
                                    s.replace(/%(-?)([%aAbBcdeHIlmMpPSwyYZ])/g, function (s, f, m) {
                                        switch (m) {
                                            case "%":
                                                return "%";
                                            case "a":
                                                return t("date.abbrDayNames")[c];
                                            case "A":
                                                return t("date.dayNames")[c];
                                            case "b":
                                                return t("date.abbrMonthNames")[h];
                                            case "B":
                                                return t("date.monthNames")[h];
                                            case "c":
                                                return o.toString();
                                            case "d":
                                                return e(a, f);
                                            case "e":
                                                return a;
                                            case "H":
                                                return e(l, f);
                                            case "I":
                                                return e(r(o, "%l"), f);
                                            case "l":
                                                return 0 === l || 12 === l ? 12 : (l + 12) % 12;
                                            case "m":
                                                return e(h + 1, f);
                                            case "M":
                                                return e(u, f);
                                            case "p":
                                                return i("time." + (l > 11 ? "pm" : "am")).toUpperCase();
                                            case "P":
                                                return i("time." + (l > 11 ? "pm" : "am"));
                                            case "S":
                                                return e(d, f);
                                            case "w":
                                                return c;
                                            case "y":
                                                return e(p % 100, f);
                                            case "Y":
                                                return p;
                                            case "Z":
                                                return n(o);
                                        }
                                    })
                                );
                            }),
                            (e = function (t, e) {
                                switch (e) {
                                    case "-":
                                        return t;
                                    default:
                                        return ("0" + t).slice(-2);
                                }
                            }),
                            (n = function (t) {
                                var e, n, r, i, o;
                                return (e = null != (n = (o = t.toString()).match(/\(([\w\s]+)\)$/)) ? n[1] : void 0)
                                    ? /\s/.test(e)
                                        ? e.match(/\b(\w)/g).join("")
                                        : e
                                    : (e = null != (r = o.match(/(\w{3,4})\s\d{4}$/)) ? r[1] : void 0) || (e = null != (i = o.match(/(UTC[\+\-]\d+)/)) ? i[1] : void 0)
                                        ? e
                                        : "";
                            });
                    }.call(this),
                    function () {
                        o.CalendarDate = (function () {
                            function t(t, e, n) {
                                (this.date = new Date(Date.UTC(t, e - 1))),
                                    this.date.setUTCDate(n),
                                    (this.year = this.date.getUTCFullYear()),
                                    (this.month = this.date.getUTCMonth() + 1),
                                    (this.day = this.date.getUTCDate()),
                                    (this.value = this.date.getTime());
                            }
                            return (
                                (t.fromDate = function (t) {
                                    return new this(t.getFullYear(), t.getMonth() + 1, t.getDate());
                                }),
                                (t.today = function () {
                                    return this.fromDate(new Date());
                                }),
                                (t.prototype.equals = function (t) {
                                    return (null != t ? t.value : void 0) === this.value;
                                }),
                                (t.prototype.is = function (t) {
                                    return this.equals(t);
                                }),
                                (t.prototype.isToday = function () {
                                    return this.is(this.constructor.today());
                                }),
                                (t.prototype.occursOnSameYearAs = function (t) {
                                    return this.year === (null != t ? t.year : void 0);
                                }),
                                (t.prototype.occursThisYear = function () {
                                    return this.occursOnSameYearAs(this.constructor.today());
                                }),
                                (t.prototype.daysSince = function (t) {
                                    if (t) return (this.date - t.date) / 864e5;
                                }),
                                (t.prototype.daysPassed = function () {
                                    return this.constructor.today().daysSince(this);
                                }),
                                t
                            );
                        })();
                    }.call(this),
                    function () {
                        var t, e, n;
                        (e = o.strftime),
                            (n = o.translate),
                            (t = o.getI18nValue),
                            (o.RelativeTime = (function () {
                                function r(t) {
                                    (this.date = t), (this.calendarDate = o.CalendarDate.fromDate(this.date));
                                }
                                return (
                                    (r.prototype.toString = function () {
                                        var t, e;
                                        return (e = this.toTimeElapsedString())
                                            ? n("time.elapsed", { time: e })
                                            : (t = this.toWeekdayString())
                                                ? ((e = this.toTimeString()), n("datetime.at", { date: t, time: e }))
                                                : n("date.on", { date: this.toDateString() });
                                    }),
                                    (r.prototype.toTimeOrDateString = function () {
                                        return this.calendarDate.isToday() ? this.toTimeString() : this.toDateString();
                                    }),
                                    (r.prototype.toTimeElapsedString = function () {
                                        var t, e, r, i, o;
                                        return (
                                            (r = new Date().getTime() - this.date.getTime()),
                                            (i = Math.round(r / 1e3)),
                                            (e = Math.round(i / 60)),
                                            (t = Math.round(e / 60)),
                                            r < 0
                                                ? null
                                                : i < 10
                                                    ? ((o = n("time.second")), n("time.singular", { time: o }))
                                                    : i < 45
                                                        ? i + " " + n("time.seconds")
                                                        : i < 90
                                                            ? ((o = n("time.minute")), n("time.singular", { time: o }))
                                                            : e < 45
                                                                ? e + " " + n("time.minutes")
                                                                : e < 90
                                                                    ? ((o = n("time.hour")), n("time.singularAn", { time: o }))
                                                                    : t < 24
                                                                        ? t + " " + n("time.hours")
                                                                        : ""
                                        );
                                    }),
                                    (r.prototype.toWeekdayString = function () {
                                        switch (this.calendarDate.daysPassed()) {
                                            case 0:
                                                return n("date.today");
                                            case 1:
                                                return n("date.yesterday");
                                            case -1:
                                                return n("date.tomorrow");
                                            case 2:
                                            case 3:
                                            case 4:
                                            case 5:
                                            case 6:
                                                return e(this.date, "%A");
                                            default:
                                                return "";
                                        }
                                    }),
                                    (r.prototype.toDateString = function () {
                                        var n;
                                        return (n = t(this.calendarDate.occursThisYear() ? "date.formats.thisYear" : "date.formats.default")), e(this.date, n);
                                    }),
                                    (r.prototype.toTimeString = function () {
                                        return e(this.date, t("time.formats.default"));
                                    }),
                                    r
                                );
                            })());
                    }.call(this),
                    function () {
                        var t,
                            e = function (t, e) {
                                return function () {
                                    return t.apply(e, arguments);
                                };
                            };
                        (t = o.elementMatchesSelector),
                            (o.PageObserver = (function () {
                                function n(t, n) {
                                    (this.selector = t), (this.callback = n), (this.processInsertion = e(this.processInsertion, this)), (this.processMutations = e(this.processMutations, this));
                                }
                                return (
                                    (n.prototype.start = function () {
                                        if (!this.started) return this.observeWithMutationObserver() || this.observeWithMutationEvent(), (this.started = !0);
                                    }),
                                    (n.prototype.observeWithMutationObserver = function () {
                                        if ("undefined" != typeof MutationObserver && null !== MutationObserver) return new MutationObserver(this.processMutations).observe(document.documentElement, { childList: !0, subtree: !0 }), !0;
                                    }),
                                    (n.prototype.observeWithMutationEvent = function () {
                                        return addEventListener("DOMNodeInserted", this.processInsertion, !1), !0;
                                    }),
                                    (n.prototype.findSignificantElements = function (e) {
                                        var n;
                                        return (n = []), (null != e ? e.nodeType : void 0) === Node.ELEMENT_NODE && (t(e, this.selector) && n.push(e), n.push.apply(n, e.querySelectorAll(this.selector))), n;
                                    }),
                                    (n.prototype.processMutations = function (t) {
                                        var e, n, r, i, o, s, a, c;
                                        for (e = [], n = 0, i = t.length; n < i; n++)
                                            switch (((s = t[n]), s.type)) {
                                                case "childList":
                                                    for (r = 0, o = (c = s.addedNodes).length; r < o; r++) (a = c[r]), e.push.apply(e, this.findSignificantElements(a));
                                            }
                                        return this.notify(e);
                                    }),
                                    (n.prototype.processInsertion = function (t) {
                                        var e;
                                        return (e = this.findSignificantElements(t.target)), this.notify(e);
                                    }),
                                    (n.prototype.notify = function (t) {
                                        if (null != t ? t.length : void 0) return "function" == typeof this.callback ? this.callback(t) : void 0;
                                    }),
                                    n
                                );
                            })());
                    }.call(this),
                    function () {
                        var t, e, n, r;
                        (n = o.parseDate),
                            (r = o.strftime),
                            (e = o.getI18nValue),
                            (t = o.config),
                            (o.Controller = (function () {
                                function i() {
                                    (this.processElements = (function (t, e) {
                                        return function () {
                                            return t.apply(e, arguments);
                                        };
                                    })(this.processElements, this)),
                                        (this.pageObserver = new o.PageObserver(s, this.processElements));
                                }
                                var s, a, c;
                                return (
                                    (s = "time[data-local]:not([data-localized])"),
                                    (i.prototype.start = function () {
                                        if (!this.started) return this.processElements(), this.startTimer(), this.pageObserver.start(), (this.started = !0);
                                    }),
                                    (i.prototype.startTimer = function () {
                                        var e;
                                        if ((e = t.timerInterval)) return null != this.timer ? this.timer : (this.timer = setInterval(this.processElements, e));
                                    }),
                                    (i.prototype.processElements = function (t) {
                                        var e, n, r;
                                        for (null == t && (t = document.querySelectorAll(s)), n = 0, r = t.length; n < r; n++) (e = t[n]), this.processElement(e);
                                        return t.length;
                                    }),
                                    (i.prototype.processElement = function (t) {
                                        var i, o, s, l, u, h;
                                        if (((o = t.getAttribute("datetime")), (s = t.getAttribute("data-format")), (l = t.getAttribute("data-local")), (u = n(o)), !isNaN(u)))
                                            return (
                                                t.hasAttribute("title") || ((h = r(u, e("datetime.formats.default"))), t.setAttribute("title", h)),
                                                (t.textContent = i = (function () {
                                                    switch (l) {
                                                        case "time":
                                                            return a(t), r(u, s);
                                                        case "date":
                                                            return a(t), c(u).toDateString();
                                                        case "time-ago":
                                                            return c(u).toString();
                                                        case "time-or-date":
                                                            return c(u).toTimeOrDateString();
                                                        case "weekday":
                                                            return c(u).toWeekdayString();
                                                        case "weekday-or-date":
                                                            return c(u).toWeekdayString() || c(u).toDateString();
                                                    }
                                                })()),
                                                t.hasAttribute("aria-label") ? void 0 : t.setAttribute("aria-label", i)
                                            );
                                    }),
                                    (a = function (t) {
                                        return t.setAttribute("data-localized", "");
                                    }),
                                    (c = function (t) {
                                        return new o.RelativeTime(t);
                                    }),
                                    i
                                );
                            })());
                    }.call(this),
                    function () {
                        var t, e, n, r;
                        (r = !1),
                            (t = function () {
                                return document.attachEvent ? "complete" === document.readyState : "loading" !== document.readyState;
                            }),
                            (e = function (t) {
                                var e;
                                return null != (e = "function" == typeof requestAnimationFrame ? requestAnimationFrame(t) : void 0) ? e : setTimeout(t, 17);
                            }),
                            (n = function () {
                                return o.getController().start();
                            }),
                            (o.start = function () {
                                if (!r) return (r = !0), ("undefined" != typeof MutationObserver && null !== MutationObserver) || t() ? n() : e(n);
                            }),
                            window.LocalTime === o && o.start();
                    }.call(this));
            }.call(this),
                t.exports ? (t.exports = o) : void 0 === (i = "function" === typeof (r = o) ? r.call(e, n, e, t) : r) || (t.exports = i));
        }.call(this));
    },
    function (t, e, n) {
        var r = { "./hcaptcha.js": 17, "./index.js": 10, "./mailto_protocol_handler.js": 18, "./page_updates.js": 19, "./time_zone_cookie.js": 20 };
        function i(t) {
            var e = o(t);
            return n(e);
        }
        function o(t) {
            if (!n.o(r, t)) {
                var e = new Error("Cannot find module '" + t + "'");
                throw ((e.code = "MODULE_NOT_FOUND"), e);
            }
            return r[t];
        }
        (i.keys = function () {
            return Object.keys(r);
        }),
            (i.resolve = o),
            (t.exports = i),
            (i.id = 16);
    },
    function (t, e) {
        function n(t) {
            const e = new CustomEvent("hcaptchaSubmitted", { detail: t });
            window.dispatchEvent(e);
        }
        const r = new Promise((t) => {
            window.hcaptchaLoaded = t;
        });
        Promise.resolve().then(() => {
            (window.hcaptchaLoaded.promise = r), (window.hcaptchaSubmitted = n);
        });
    },
    function (t, e, n) {
        "use strict";
        n.r(e);
        var r,
            i = n(0);
        "registerProtocolHandler" in navigator &&
            ((r = function () {
                localStorage.mailtoHandlerRegisteredAt || ((localStorage.mailtoHandlerRegisteredAt = new Date().toJSON()), navigator.registerProtocolHandler("mailto", location.origin + "/mailto/%s", "HEY"));
            }),
                i.c.identity.id
                    ? r()
                    : addEventListener("turbolinks:load", function t() {
                        i.c.identity.id && (removeEventListener("turbolinks:load", t), r());
                    }));
    },
    function (t, e, n) {
        "use strict";
        n.r(e);
        var r = n(6);
        const i = "text/html; page-update";
        addEventListener("turbolinks:before-fetch-request", function (t) {
            const { headers: e } = t.data.fetchOptions;
            e.Accept = [i, e.Accept].join(", ");
        }),
            addEventListener("turbolinks:before-fetch-response", function (t) {
                var e;
                const { fetchResponse: n } = t.data;
                (null == (e = n.contentType) ? void 0 : e.includes(i)) && (t.preventDefault(), n.responseHTML.then(r.a));
            });
    },
    function (t, e, n) {
        "use strict";
        n.r(e);
        var r = n(2);
        const { timeZone: i } = new Intl.DateTimeFormat().resolvedOptions();
        Object(r.b)("time_zone", i);
    },
    function (t, e) {
        class n extends HTMLElement {
            constructor() {
                super(), (this.mutationObserver = void 0), (this.mutationObserver = new MutationObserver(() => this.render())), this.mutationObserver.observe(this, { childList: !0 });
            }
            connectedCallback() {
                this.render();
            }
            render() {
                const t = this.attachShadow({ mode: "open" });
                (t.innerHTML =
                    "\n      <style>\n        a {\n          color: var(--color-txt--action);\n        }\n\n        blockquote {\n          margin: 0;\n          padding: 0 0 0 1.5rem;\n          border-left: 1px solid var(--color-border);\n        }\n\n        hr {\n          border: 0;\n          border-bottom: 1px solid var(--color-border);\n        }\n\n        img {\n          border: 1px solid var(--color-border);\n          max-width: 100%;\n          height: auto;\n        }\n\n        pre {\n          display: inline-block;\n          width: 96%;\n          vertical-align: top;\n          font-family: monospace;\n          font-size: 0.9em;\n          margin: 0;\n          padding: 0.5em 2%;\n          white-space: pre-wrap;\n          background-color: var(--color-bg--surface);\n          overflow-x: auto;\n          border-radius: 4px;\n        }\n      </style>\n    "),
                    this.templateContentFragment && t.appendChild(this.templateContentFragment);
            }
            get templateContentFragment() {
                if (this.templateElement) return document.importNode(this.templateElement.content, !0);
            }
            get templateElement() {
                return this.querySelector("template");
            }
        }
        customElements.define("shadow-content", n);
    },
    function (t, e, n) {
        "use strict";
        n.r(e);
        var r = n(5),
            i = n(11),
            o = n(0);
        const s = r.a.start(),
            a = n(23);
        s.load(Object(i.a)(a)),
            o.i &&
            (async function () {
                const { definitions: t } = await n.e(1).then(n.bind(null, 89));
                s.load(t);
            })();
    },
    function (t, e, n) {
        var r = {
            "./action_bar_controller.js": 24,
            "./address_fields_controller.js": 25,
            "./anchor_controller.js": 26,
            "./animation_controller.js": 27,
            "./auto_remove_controller.js": 28,
            "./autoclick_controller.js": 29,
            "./autocomplete_controller.js": 30,
            "./autodraft_controller.js": 31,
            "./autofocus_first_element_controller.js": 32,
            "./autoimport_controller.js": 33,
            "./autoresize_controller.js": 34,
            "./autosubmit_controller.js": 35,
            "./beacon_controller.js": 36,
            "./box_freshness_controller.js": 37,
            "./box_keyboard_navigation_controller.js": 38,
            "./bulk_actions_controller.js": 39,
            "./captcha_controller.js": 40,
            "./clearances_controller.js": 41,
            "./clips_controller.js": 42,
            "./color_scheme_controller.js": 43,
            "./composition_controller.js": 44,
            "./copy_to_clipboard_controller.js": 45,
            "./credit_card_form_controller.js": 46,
            "./disable_on_mobile_controller.js": 47,
            "./element_removal_controller.js": 48,
            "./entry_controller.js": 49,
            "./expander_controller.js": 50,
            "./extenzion_fields_controller.js": 51,
            "./file_input_controller.js": 52,
            "./first_run_controller.js": 53,
            "./focus_controller.js": 54,
            "./focus_next_controller.js": 55,
            "./form_controller.js": 56,
            "./geared_refresh_frame_controller.js": 58,
            "./hotkey_controller.js": 59,
            "./journal_params_controller.js": 60,
            "./list_controller.js": 8,
            "./list_unseen_controller.js": 61,
            "./live_search_controller.js": 62,
            "./loading_indicator_controller.js": 63,
            "./outlet_controller.js": 64,
            "./page_controller.js": 65,
            "./page_update_subscription_controller.js": 66,
            "./pagination_controller.js": 67,
            "./popup_menu_controller.js": 68,
            "./popup_picker_controller.js": 69,
            "./posting_state_controller.js": 70,
            "./rich_text_controller.js": 71,
            "./scroll_to_self_controller.js": 72,
            "./search_controller.js": 73,
            "./signup_email_address_controller.js": 74,
            "./sorted_controller.js": 75,
            "./steps_controller.js": 76,
            "./template_inputs_controller.js": 77,
            "./toggle_delete_controller.js": 78,
            "./toggle_edit_controller.js": 79,
            "./topic_compressed_controller.js": 80,
            "./topic_title_controller.js": 81,
            "./totp_setup_controller.js": 82,
            "./web_authn_controller.js": 83,
            "./web_notifications_controller.js": 84,
            "./window_controller.js": 85,
        };
        function i(t) {
            var e = o(t);
            return n(e);
        }
        function o(t) {
            if (!n.o(r, t)) {
                var e = new Error("Cannot find module '" + t + "'");
                throw ((e.code = "MODULE_NOT_FOUND"), e);
            }
            return r[t];
        }
        (i.keys = function () {
            return Object.keys(r);
        }),
            (i.resolve = o),
            (t.exports = i),
            (i.id = 23);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return r;
                });
                class r extends t {
                    updateSelection(t) {
                        t.target.classList.contains(this.highlightClass) ? this.removeExistingSelections() : (this.removeExistingSelections(), t.target.classList.add(this.highlightClass)), this.dispatch("updateSelection");
                    }
                    removeExistingSelections() {
                        this.itemTargets.forEach((t) => t.classList.remove(this.highlightClass));
                    }
                }
                (r.targets = ["item"]), (r.classes = ["highlight"]);
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return r;
                });
                class r extends t {
                    localize() {
                        this.hideLocalizedFields(), this.showLocalizedFieldsForCountry(this.selectedCountry);
                    }
                    hideLocalizedFields() {
                        this.localizedFieldTargets.forEach((t) => i(t, !1)), i(this.internationalFieldTarget, !1);
                    }
                    showLocalizedFieldsForCountry(t) {
                        i(this.findLocalizedFieldsForCountry(t) || this.internationalFieldTarget, !0);
                    }
                    findLocalizedFieldsForCountry(t) {
                        return this.localizedFieldTargets.find((e) => e.dataset.country === t);
                    }
                    get selectedCountry() {
                        return this.countryTarget.value.toLowerCase();
                    }
                }
                function i(t, e) {
                    t.disabled = !e;
                }
                r.targets = ["country", "localizedField", "internationalField"];
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                e.default = class extends t {
                    scroll(t) {
                        t.preventDefault();
                        const e = t.currentTarget.hash.replace(/^#/, ""),
                            n = document.getElementById(e);
                        n.scrollIntoView(), n.focus();
                    }
                };
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return i;
                });
                var r = n(0);
                class i extends t {
                    async play() {
                        await Object(r.j)(), this.classList.remove(this.playClass), this.forceReflow(), this.classList.add(this.playClass);
                    }
                    forceReflow() {
                        this.element.offsetWidth;
                    }
                }
                i.classes = ["play"];
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return r;
                });
                class r extends t {
                    constructor(...t) {
                        super(...t),
                            (this.remove = () => {
                                this.element.remove();
                            });
                    }
                    connect() {
                        this.timeout = setTimeout(this.remove, this.delayValue);
                    }
                    disconnect() {
                        clearTimeout(this.timeout), this.remove();
                    }
                }
                r.values = { delay: Number };
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return r;
                });
                class r extends t {
                    constructor(...t) {
                        super(...t),
                            (this.clickOnce = () => {
                                this.clickedValue || ((this.clickedValue = !0), this.element.click());
                            });
                    }
                    connect() {
                        this.timeout = setTimeout(this.clickOnce, this.delayValue);
                    }
                    disconnect() {
                        clearTimeout(this.timeout);
                    }
                }
                r.values = { delay: Number, clicked: Boolean };
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return i;
                });
                var r = n(13);
                class i extends t {
                    constructor(...t) {
                        super(...t),
                            (this.keyHandlers = {
                                ArrowDown(t) {
                                    this.combobox.index++, t.preventDefault();
                                },
                                ArrowUp(t) {
                                    this.combobox.index--, t.preventDefault();
                                },
                                Backspace(t) {
                                    this.inputTarget.value || this.combobox.removeLast();
                                },
                                Escape(t) {
                                    this.combobox.collapse();
                                },
                                Enter(t) {
                                    this.commitOrAdd() && t.preventDefault();
                                },
                                Tab(t) {
                                    this.commit() && t.preventDefault();
                                },
                                ","(t) {
                                    this.commitOrAdd() && t.preventDefault();
                                },
                                ";"(t) {
                                    this.commitOrAdd() && t.preventDefault();
                                },
                            }),
                            (this.parsers = {
                                email(t = "") {
                                    const e = [];
                                    for (const r of t.split(/,|\n/)) {
                                        var n;
                                        const t = null == (n = r.trim().match(/(.*<?\S+@\S+\.\S+>?)/)) ? void 0 : n[1];
                                        t && e.push(t);
                                    }
                                    return e;
                                },
                            });
                    }
                    initialize() {
                        this.combobox = r.a.fromController(this);
                    }
                    connect() {
                        this.combobox.connect();
                    }
                    disconnect() {
                        this.combobox.disconnect(), this.reset();
                    }
                    search(t) {
                        null == t || t.stopPropagation(), this.combobox.update();
                    }
                    select(t) {
                        const { value: e } = t.target.dataset;
                        this.combobox.value = e;
                    }
                    commit(t) {
                        if ((null == t || t.preventDefault(), this.combobox.commit())) return this.reset(), this.inputTarget.focus(), !0;
                    }
                    remove(t) {
                        t.preventDefault();
                        const { value: e } = t.target.dataset;
                        this.combobox.remove(e);
                    }
                    navigate(t) {
                        var e;
                        null == (e = this.keyHandlers[t.key]) || e.call(this, t);
                    }
                    extract(t) {
                        var e;
                        if (this.forbidAddingValue) return;
                        const n = null == (e = t.clipboardData) ? void 0 : e.getData("text/plain");
                        if (!n) return;
                        const r = this.parsers[this.inputTarget.type];
                        if (!r) return;
                        const i = r(n);
                        if (i.length) {
                            t.preventDefault();
                            for (const t of i) this.combobox.add(t);
                            this.reset();
                        }
                    }
                    teardown(t) {
                        ("submit" == t.type && t.target != this.form) || this.add() || this.reset();
                    }
                    commitOrAdd() {
                        return this.commit() || this.add();
                    }
                    add() {
                        if (this.forbidAddingValue) return;
                        if (!this.inputTarget.checkValidity()) return;
                        const { value: t } = this.inputTarget;
                        return t ? (this.combobox.add(t), this.reset(), !0) : void 0;
                    }
                    reset() {
                        (this.inputTarget.value = ""), this.search();
                    }
                    get form() {
                        return this.selectTarget.form;
                    }
                }
                (i.targets = ["input", "list", "select"]), (i.values = { url: String, forbidAdding: Boolean });
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return s;
                });
                var r = n(12),
                    i = n(3),
                    o = n(0);
                class s extends t {
                    initialize() {
                        this.isNewRecord || this.addBridgeAttributeToDiscardButton(), this.hasLocalDraft && (this.restorationPromptTarget.hidden = !1);
                    }
                    connect() {
                        this.stoppedValue = this.pageIsTurbolinksPreview;
                    }
                    disconnect() {
                        this.stoppedValue = !0;
                    }
                    async saveAutomatically() {
                        this.saveScheduled || ((this.saveScheduled = !0), await Object(o.e)(1e3), (this.saveScheduled = !1), this.saveDraft());
                    }
                    async saveManually() {
                        await this.createOrUpdateDraft(), this.manualSaveRedirectLinkTarget.click();
                    }
                    async saveDraft() {
                        this.savingDraft ? (this.pendingRequest = !0) : ((this.savingDraft = !0), await this.createOrUpdateDraft(), (this.savingDraft = !1), this.pendingRequest && ((this.pendingRequest = !1), this.saveDraft()));
                    }
                    async createOrUpdateDraft() {
                        if (!this.stoppedValue)
                            try {
                                const t = await this.submitFormAsDraft();
                                if (t.ok) {
                                    if ((this.discardLocalDraft(), this.isNewRecord)) {
                                        const e = t.headers.get("Location");
                                        this.nextSubmitShouldUpdate(e), this.nextDiscardShouldDelete(e);
                                    }
                                    this.dispatch("save");
                                } else this.saveLocalDraft();
                            } catch (t) {
                                this.saveLocalDraft();
                            }
                    }
                    submitFormAsDraft() {
                        return new i.a("post", this.formAction, { body: this.autodraftableFormData }).perform();
                    }
                    stopSavingDrafts() {
                        this.stoppedValue = !0;
                    }
                    resumeSavingDrafts(t) {
                        t.data.success ? this.discardLocalDraft() : (this.hasLocalDraft && this.displayLocalDraftAlert(), (this.stoppedValue = !1));
                    }
                    nextSubmitShouldUpdate(t) {
                        (this.formAction = t), (this.formMethod = "PUT");
                    }
                    nextDiscardShouldDelete(t) {
                        this.hasDiscardButtonTarget && ((this.discardButtonTarget.form.action = t), (this.discardButtonTarget.hidden = !1), (this.discardLinkTarget.hidden = !0), this.addBridgeAttributeToDiscardButton());
                    }
                    addBridgeAttributeToDiscardButton() {
                        if (this.hasDiscardButtonTarget) {
                            const t = this.discardButtonTarget.getAttribute("data-controller") || "";
                            this.hasDiscardLinkTarget && this.discardLinkTarget.remove(), this.discardButtonTarget.setAttribute("data-controller", t + " bridge--nav-button");
                        }
                    }
                    synthesizeFormMethod() {
                        (this.syntheticMethodInput = this.element.querySelector("input[name=_method]")),
                            this.syntheticMethodInput ||
                            ((this.syntheticMethodInput = document.createElement("input")),
                                (this.syntheticMethodInput.type = "hidden"),
                                (this.syntheticMethodInput.name = "_method"),
                                (this.syntheticMethodInput.value = this.element.method),
                                this.element.append(this.syntheticMethodInput));
                    }
                    saveLocalDraft() {
                        this.localForm.save(), this.dispatch("save-local");
                    }
                    discardLocalDraft() {
                        this.hasLocalDraft && (this.localForm.discard(), (this.restorationPromptTarget.hidden = !0));
                    }
                    restoreLocalDraft() {
                        this.hasLocalDraft && (this.localForm.restore(), this.localForm.discard(), (this.restorationPromptTarget.hidden = !0));
                    }
                    displayLocalDraftAlert() {
                        alert("Your email has been saved, but not sent. It will stay saved " + (this.isNewRecord ? "on this page" : "in the drafts section") + ", but only on this device.");
                    }
                    get hasLocalDraft() {
                        return this.localForm.isSaved;
                    }
                    get localForm() {
                        return new r.a(this.formTarget);
                    }
                    get isNewRecord() {
                        return "POST" == this.formMethod.toUpperCase();
                    }
                    get formAction() {
                        return this.formTarget.action;
                    }
                    set formAction(t) {
                        this.formTarget.action = t;
                    }
                    get formMethod() {
                        return this.syntheticMethodInput || this.synthesizeFormMethod(), this.syntheticMethodInput.value;
                    }
                    set formMethod(t) {
                        this.syntheticMethodInput || this.synthesizeFormMethod(), (this.syntheticMethodInput.value = t);
                    }
                    get autodraftableFormData() {
                        const t = new FormData(this.formTarget);
                        return t.set("autodraft", "true"), t.set("entry[status]", "drafted"), t;
                    }
                }
                (s.targets = ["form", "manualSaveRedirectLink", "discardLink", "discardButton", "restorationPrompt"]), (s.values = { stopped: Boolean });
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return r;
                });
                class r extends t {
                    connect() {
                        this.elementTargets[0].focus();
                    }
                }
                r.targets = ["element"];
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                e.default = class extends t {
                    initialize() {
                        this.element.replaceWith(this.element.content);
                    }
                };
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                var r = n(0);
                e.default = class extends t {
                    async connect() {
                        this.autoresize();
                    }
                    async autoresize() {
                        await Object(r.j)(), (this.element.style.height = 0);
                        const t = this.element.offsetHeight - this.element.clientHeight;
                        this.element.style.height = this.element.scrollHeight + t + "px";
                    }
                };
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                e.default = class extends t {
                    initialize() {
                        this.element.requestSubmit();
                    }
                };
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                var r = n(0),
                    i = n(2);
                e.default = class extends t {
                    initialize() {
                        o(this.element.action, this.formData), this.element.remove();
                    }
                    get formData() {
                        const t = new FormData(this.element);
                        return t.set("authenticity_token", this.csrfToken), t;
                    }
                    get csrfToken() {
                        var t;
                        const e = null == (t = document.head.querySelector("meta[name=csrf-param]")) ? void 0 : t.content;
                        return e ? Object(i.a)(e) : void 0;
                    }
                };
                const o = navigator.sendBeacon && !r.g ? (t, e) => navigator.sendBeacon(t, e) : (t, e) => r.k.post(t, { body: e });
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return i;
                });
                var r = n(0);
                class i extends t {
                    connect() {
                        (this.pagehiddenAt = null), (this.heartbeatChannelDisconnectedAt = null), this.subscribeToHeartbeatChannel();
                    }
                    disconnect() {
                        this.unsubscribeFromHeartbeatChannel();
                    }
                    visibilityChanged() {
                        switch (document.visibilityState) {
                            case "hidden":
                                this.pagehiddenAt = this.getCurrentTime();
                                break;
                            case "visible":
                                this.pageWasHiddenForTooLong && this.reloadPageIfStale(), (this.pagehiddenAt = null);
                        }
                    }
                    get pageWasHiddenForTooLong() {
                        return this.pagehiddenAt && this.secondsSince(this.pagehiddenAt) > 300;
                    }
                    async subscribeToHeartbeatChannel() {
                        this.subscription = await Object(r.m)("HeartbeatChannel", { connected: this.heartbeatChannelConnected.bind(this), disconnected: this.heartbeatChannelDisconnected.bind(this) });
                    }
                    unsubscribeFromHeartbeatChannel() {
                        var t;
                        null == (t = this.subscription) || t.unsubscribe();
                    }
                    heartbeatChannelConnected() {
                        this.heartbeatChannelDisconnectedForTooLong && this.reloadPageIfStale(), (this.heartbeatChannelDisconnectedAt = null);
                    }
                    heartbeatChannelDisconnected() {
                        this.heartbeatChannelDisconnectedAt = this.getCurrentTime();
                    }
                    get heartbeatChannelDisconnectedForTooLong() {
                        return this.heartbeatChannelDisconnectedAt && this.secondsSince(this.heartbeatChannelDisconnectedAt) > 60;
                    }
                    async reloadPageIfStale() {
                        (await this.latestDocumentMatchesCurrentRowsIdentifiers()) ? (this.active = !0) : ((this.active = !1), this.reloadPage());
                    }
                    async latestDocumentMatchesCurrentRowsIdentifiers() {
                        const t = await r.k.get(this.urlValue),
                            e = new DOMParser().parseFromString(t, "text/html").querySelector('[data-controller~="' + this.identifier + '"]'),
                            n = (null == e ? void 0 : e.querySelectorAll("[data-" + this.identifier + '-target~="row"]')) || [],
                            i = Array.from(n)
                                .slice(0, this.firstPageSizeValue)
                                .map((t) => t.dataset.identifier);
                        return JSON.stringify(i) === JSON.stringify(this.currentRowsIdentifiers);
                    }
                    get currentRowsIdentifiers() {
                        return this.rowTargets.slice(0, this.firstPageSizeValue).map((t) => t.dataset.identifier);
                    }
                    getCurrentTime() {
                        return new Date().getTime();
                    }
                    secondsSince(t) {
                        return (this.getCurrentTime() - t) / 1e3;
                    }
                    set active(t) {
                        document.body.classList.toggle(this.reloadingClass, !t);
                    }
                    reloadPage() {
                        Turbolinks.visit(window.location.href);
                    }
                }
                (i.targets = ["row"]), (i.classes = ["reloading"]), (i.values = { url: String, firstPageSize: Number });
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return o;
                });
                var r = n(0);
                const i = new r.a();
                class o extends t {
                    constructor(...t) {
                        super(...t),
                            (this.stashFocus = () => {
                                const t = this.linkTargets[this.focusIndexValue - 1],
                                    e = this.linkTargets[this.focusIndexValue],
                                    n = this.linkTargets[this.focusIndexValue + 1];
                                i.stash([e, n, t]);
                            }),
                            (this.restoreFocus = () => {
                                const t = i.restore();
                                t && Object(r.l)(t, { behavior: "auto", block: "nearest" });
                            });
                    }
                    initialize() {
                        this.observeMutations(this.updateFocusAfterMutation);
                    }
                    connect() {
                        this.restoreFocus();
                    }
                    disconnect() {
                        this.stashFocus();
                    }
                    async updateFocusAfterMutation([t]) {
                        const { focusIndexValue: e, linkTargets: n } = this,
                            [...i] = t.removedNodes,
                            o = i.filter((t) => t.querySelector).some((t) => t.querySelector("[tabindex='0']")),
                            s = n[e] || n[e - 1];
                        o && s && (await Object(r.j)(), s.focus(), Object(r.l)(s, { behavior: "auto", block: "nearest" }));
                    }
                    focusExclusively(t) {
                        const e = t.target;
                        for (const n of this.linkTargetsInScopeWithFocus) n.setAttribute("tabindex", -1);
                        (this.focusIndexValue = this.linkTargets.indexOf(e)), e.setAttribute("tabindex", 0);
                    }
                    focusClosest({ target: t }) {
                        const e = this.containerTargets.find(s(t)),
                            n = this.linkTargets.find(a(e));
                        null == n || n.focus();
                    }
                    trackFocusWithinContainer(t) {
                        if (this.hasFocusClass) {
                            const e = t.target,
                                n = this.containerTargetsInScopeWithFocus.find(s(e));
                            for (const t of this.containerTargetsInScopeWithFocus) t.classList.remove(this.focusClass);
                            null == n || n.classList.add(this.focusClass);
                        }
                    }
                    focusPrevious() {
                        this.focusLinkTargetInDirection(-1);
                    }
                    focusNext() {
                        this.focusLinkTargetInDirection(1);
                    }
                    resumeFocus() {
                        const t = this.indexOfLastFocus(this.linkTargetsInScopeWithFocus),
                            e = this.linkTargetsInScopeWithFocus[t];
                        e ? e.focus() : this.focusNext();
                    }
                    toggleHint(t) {
                        const { count: e } = t.detail;
                        for (const n of this.linkTargets) e && this.ariaDescribedbyValue ? n.setAttribute("aria-describedby", this.ariaDescribedbyValue) : n.removeAttribute("aria-describedby");
                    }
                    focusLinkTargetInDirection(t) {
                        const e = this.getLinkTargetInDirection(t);
                        null == e || e.focus(), this.scrollTo(e);
                    }
                    getLinkTargetInDirection(t) {
                        const { identifier: e, scopeWithFocus: n, linkTargetsInScopeWithFocus: r } = this;
                        return r[this.indexOfLastFocus(r) + (n.hasAttribute("data-" + e + "-reverse") ? -1 * t : t)];
                    }
                    get scopeWithFocus() {
                        const { activeElement: t } = document,
                            { element: e, scopeTargets: n } = this,
                            r = n[0] || e;
                        return n.find(s(t)) || r;
                    }
                    get linkTargetsInScopeWithFocus() {
                        return this.linkTargets.filter((t) => this.scopeWithFocus.contains(t));
                    }
                    get containerTargetsInScopeWithFocus() {
                        const { containerTargets: t, scopeWithFocus: e } = this;
                        return t.filter(a(e));
                    }
                    indexOfLastFocus(t = this.linkTargets) {
                        const e = t.indexOf(document.activeElement);
                        return -1 !== e ? e : t.findIndex((t) => "0" === t.getAttribute("tabindex"));
                    }
                    scrollTo(t) {
                        t && Object.keys(this.scrollOptionsValue).length && Object(r.l)(t, this.scrollOptionsValue);
                    }
                }
                function s(t) {
                    return (e) => (null == e ? void 0 : e.contains(t));
                }
                function a(t) {
                    return (e) => (null == t ? void 0 : t.contains(e));
                }
                (o.classes = ["focus"]), (o.targets = ["container", "link", "scope"]), (o.values = { ariaDescribedby: String, focusIndex: Number, scrollOptions: Object });
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return a;
                });
                var r = n(0);
                const i = "asidebox imbox laterbox feedbox trailbox".split(" "),
                    o = "bundle muted noted seen topic".split(" "),
                    s = "merge mute read note project seen trash unmute unseen done folder".split(" ");
                class a extends t {
                    select() {
                        const { parentElement: t } = document.activeElement,
                            e = this.checkboxTargets.find((e) => t.contains(e) || this.checkboxParentTargets.find((n) => n.contains(t) && n.contains(e)));
                        null == e || e.click();
                    }
                    update(t) {
                        (null == t ? void 0 : t.target.checked) && (this.isShiftSelected && this.lastSelectedCheckbox && this.shiftSelect(this.lastSelectedCheckbox, t.target), (this.lastSelectedCheckbox = t.target));
                        const { ids: e, accountIds: n, accountPurposes: r, boxKinds: o, count: s, seenCount: a, bundleCount: c, mutedCount: l, topicCount: u } = this.selected,
                            [h] = [...(1 == o.size ? o : [])];
                        this.selectedIdsFieldTargets.forEach((t) => (t.value = e)),
                            this.selectedCountTargets.forEach((t) => (t.dataset.count = s)),
                            this.classList.toggle(this.selectedClass, s > 0),
                            (this.menuTarget.open = s > 0),
                            i.forEach((t) => {
                                this["has" + this.capitalizeString(t) + "ButtonTarget"] && this.toggleButton(this[t + "ButtonTarget"], !h || h == t || c > 0);
                            }),
                            this.hasSeenButtonTarget && this.toggleButton(this.seenButtonTarget, a == s || "imbox" != h || l > 0),
                            this.hasUnseenButtonTarget && this.toggleButton(this.unseenButtonTarget, 0 == a || "imbox" != h || l > 0),
                            this.hasMuteButtonTarget && this.toggleButton(this.muteButtonTarget, l == s),
                            this.hasUnmuteButtonTarget && this.toggleButton(this.unmuteButtonTarget, 0 == l),
                            this.hasMergeButtonTarget && this.toggleButton(this.mergeButtonTarget, s < 2 || c > 0 || n.size > 1),
                            this.hasReadButtonTarget && this.toggleButton(this.readButtonTarget, s < 2 || c > 0),
                            this.hasNoteButtonTarget && this.toggleButton(this.noteButtonTarget, s > 1 || "imbox" != h),
                            this.hasProjectButtonTarget && this.toggleButton(this.projectButtonTarget, r.has("home") || u != s || n.size > 1),
                            this.hasTrashButtonTarget && this.toggleButton(this.trashButtonTarget, u != s),
                            this.hasImboxButtonTarget && this.toggleButton(this.imboxButtonTarget, !["feedbox", "trailbox"].includes(h)),
                            this.hasDoneButtonTarget && this.toggleButton(this.doneButtonTarget, !["asidebox", "laterbox"].includes(h)),
                            this.hasFolderButtonTarget && this.toggleButton(this.folderButtonTarget, c > 0);
                        const d = this.enabledMenuButtons;
                        this.dispatch("update", { detail: { count: s, enabledMenuButtons: d } });
                    }
                    async reset(t) {
                        "submit" == (null == t ? void 0 : t.type) && (await Object(r.j)()), this.checkboxTargets.forEach((t) => (t.checked = !1)), this.update();
                    }
                    loadPostingNoteForms() {
                        this.selected.ids.forEach((t) => document.getElementById("edit_note_link_posting_" + t).click()), this.reset();
                    }
                    toggleFilingsMenu() {
                        this.menuTarget.classList.toggle(this.filingsClass);
                    }
                    enableShiftSelect(t) {
                        "Shift" == t.key && (this.shiftSelectEnabled = !0);
                    }
                    disableShiftSelect(t) {
                        "Shift" == t.key && (this.shiftSelectEnabled = !1);
                    }
                    shiftSelect(t, e) {
                        const n = this.checkboxTargets.indexOf(t),
                            r = this.checkboxTargets.indexOf(e);
                        this.checkboxTargets.slice(Math.min(n, r), Math.max(n, r) + 1).forEach((t) => (t.checked = !0));
                    }
                    get selected() {
                        const t = { ids: [], accountIds: new Set(), accountPurposes: new Set(), boxKinds: new Set(), count: 0 };
                        return (
                            o.forEach((e) => (t[e + "Count"] = 0)),
                            this.selectedRowTargets.forEach(({ dataset: e }) => {
                                t.ids.push(e.identifier), t.accountIds.add(e.accountId), t.accountPurposes.add(e.accountPurpose), t.boxKinds.add(e.boxKind), t.count++, o.forEach((n) => (t[n + "Count"] += n in e ? 1 : 0));
                            }),
                            t
                        );
                    }
                    get selectedRowTargets() {
                        const { selectedCheckboxTargets: t } = this;
                        return this.rowTargets.filter((e) => t.some((t) => e.contains(t)));
                    }
                    get selectedCheckboxTargets() {
                        return this.checkboxTargets.filter((t) => t.checked);
                    }
                    get isShiftSelected() {
                        return this.shiftSelectEnabled;
                    }
                    get enabledMenuItems() {
                        return this.menuItemTargets.filter((t) => !t.hidden);
                    }
                    get enabledMenuButtons() {
                        const { enabledMenuItems: t } = this;
                        return this.menuButtonTargets.filter((e) => t.some((t) => t.contains(e)));
                    }
                    toggleButton(t, e) {
                        const n = this.menuButtonTargets.filter((e) => t.contains(e));
                        "disabled" in t && (t.disabled = e);
                        for (const r of n) r.disabled = e;
                        t.hidden = e;
                    }
                    capitalizeString(t) {
                        return t.charAt(0).toUpperCase() + t.slice(1);
                    }
                }
                (a.targets = ["checkbox", "checkboxParent", "row", "menu", "menuItem", "menuButton", "selectedCount", "selectedIdsField", ...s.map((t) => t + "Button"), ...i.map((t) => t + "Button")]), (a.classes = ["selected", "filings"]);
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return r;
                });
                class r extends t {
                    async connect() {
                        await this.renderCaptcha(), this.element.classList.remove(this.submittingClass);
                    }
                    submit(t) {
                        (this.element.response.value = t.detail), this.element.submit(), this.element.classList.add(this.submittingClass);
                    }
                    async renderCaptcha() {
                        await window.hcaptchaLoaded.promise, window.hcaptcha.render(this.containerTarget, this.options);
                    }
                    get options() {
                        return { sitekey: this.siteKeyValue, callback: this.callbackValue };
                    }
                }
                (r.values = { siteKey: String, callback: String }), (r.targets = ["container"]), (r.classes = ["submitting"]);
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return r;
                });
                class r extends t {
                    open() {
                        this.element.open = !0;
                    }
                    screenIn() {
                        this.clearanceElement && this.screenInButtonTargets.find((t) => this.clearanceElement.contains(t)).click();
                    }
                    screenOut() {
                        this.clearanceElement && this.screenOutButtonTargets.find((t) => this.clearanceElement.contains(t)).click();
                    }
                    feedbox() {
                        this.clearanceElement && this.feedboxButtonTargets.find((t) => this.clearanceElement.contains(t)).click();
                    }
                    trailbox() {
                        this.clearanceElement && this.trailboxButtonTargets.find((t) => this.clearanceElement.contains(t)).click();
                    }
                    toggle() {
                        this.clearanceElement && this.summaryButtonTargets.find((t) => this.clearanceElement.contains(t)).click();
                    }
                    get clearanceElement() {
                        const { parentElement: t } = document.activeElement;
                        return this.element.contains(t) ? this.clearanceTargets.find((e) => e.contains(t)) : null;
                    }
                }
                r.targets = ["clearance", "summaryButton", "screenInButton", "screenOutButton", "feedboxButton", "trailboxButton"];
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return i;
                });
                var r = n(0);
                class i extends t {
                    initialize() {
                        this.openMenu = Object(r.d)(this.openMenu.bind(this), 200);
                    }
                    async openMenu(t) {
                        await Object(r.j)(), this.closeMenu(), this.setSelectionFromEvent(t);
                    }
                    closeMenu() {
                        this.menuTarget.open = !1;
                    }
                    setSelectionFromEvent(t) {
                        const e = o.fromEvent(t, this.element, this.entryTargets);
                        e && ((this.selection = e), e.present && (this.updateFieldValues(), this.positionMenu()));
                    }
                    clearSelection() {
                        var t;
                        null == (t = this.domSelection) || t.removeAllRanges(), delete this.selection;
                    }
                    updateFieldValues() {
                        const { entryId: t, content: e } = this.selection;
                        (this.idFieldTarget.value = t), (this.contentFieldTarget.value = e);
                    }
                    positionMenu() {
                        const {
                            menuContentTarget: t,
                            menuTarget: e,
                            selection: { selectionBounds: n },
                        } = this,
                            r = n.height + t.clientHeight + 10;
                        (e.open = !0), (e.style.top = n.top + r + "px"), (e.style.left = n.left + n.width / 2 + "px");
                    }
                    get domSelection() {
                        var t;
                        return null == (t = this.selection) ? void 0 : t.domSelection;
                    }
                }
                i.targets = ["entry", "menu", "menuContent", "contentField", "idField"];
                class o {
                    static fromEvent(t, e, n) {
                        if ("selectionchange" == t.type) return new s(document, e, n);
                        {
                            const r = t.target;
                            return new a(r, e, n);
                        }
                    }
                    constructor(t, e, n) {
                        (this.domSelection = t),
                            (this.outerElement = e),
                            (this.entryTargets = n),
                            (this.selectedRange = (function (t) {
                                if ((null == t ? void 0 : t.rangeCount) > 0) return t.getRangeAt(0);
                            })(this.domSelection));
                    }
                    get present() {
                        return this.entryElement && this.content;
                    }
                    get entryId() {
                        var t;
                        return null == (t = this.entryElement) ? void 0 : t.dataset.entryId;
                    }
                    get content() {
                        var t;
                        return null == (t = this.domSelection) ? void 0 : t.toString();
                    }
                    get selectionBounds() {
                        const { selectedRangeBounds: t, outerBounds: e } = this,
                            n = t.x - e.x,
                            r = t.y - e.y;
                        return new DOMRect(n, r, t.width, t.height);
                    }
                    get selectedRangeBounds() {
                        var t, e;
                        return null != (t = null == (e = this.selectedRange) ? void 0 : e.getBoundingClientRect()) ? t : new DOMRect(0, 0);
                    }
                    get outerBounds() {
                        return this.outerElement.getBoundingClientRect();
                    }
                    findEntryTargetContainingNode(t) {
                        return this.entryTargets.find((e) => e.contains(t));
                    }
                }
                class s extends o {
                    constructor(t, e, n) {
                        super(t.getSelection(), e, n);
                    }
                    get entryElement() {
                        return this.findEntryTargetContainingNode(this.commonAncestorContainer);
                    }
                    get commonAncestorContainer() {
                        if (this.selectedRange) {
                            const t = this.selectedRange.commonAncestorContainer;
                            return t.nodeType == Node.ELEMENT_NODE ? t : t.parentElement;
                        }
                        return null;
                    }
                }
                class a extends o {
                    constructor(t, e, n) {
                        super(t.document.getSelection(), e, n), (this.messageContentElement = t);
                    }
                    get entryElement() {
                        return this.findEntryTargetContainingNode(this.messageContentElement);
                    }
                    get selectionBounds() {
                        const { x: t, y: e, width: n, height: r } = super.selectionBounds,
                            { x: i, y: o } = this.messageContentElementBounds;
                        return new DOMRect(t + i, e + o, n, r);
                    }
                    get messageContentElementBounds() {
                        return this.messageContentElement.getBoundingClientRect();
                    }
                }
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                var r = n(2);
                e.default = class extends t {
                    setLightPref() {
                        this.colorScheme = "light";
                    }
                    setDarkPref() {
                        this.colorScheme = "dark";
                    }
                    discardPref() {
                        this.colorScheme = "none";
                    }
                    set colorScheme(t) {
                        document.documentElement.setAttribute("data-color-scheme", t), Object(r.b)("color_scheme", t);
                    }
                };
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                e.default = class extends t {
                    compose({ data: t }) {
                        const e = t.formSubmission.formData;
                        e.set("message[subject]", e.get("category") + " " + e.get("message[subject]"));
                    }
                };
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return i;
                });
                var r = n(0);
                class i extends t {
                    async copy() {
                        this.reset();
                        (await Object(r.b)(this.text)) && this.classList.add(this.successClass);
                    }
                    reset() {
                        this.classList.remove(this.successClass), this.forceReflow();
                    }
                    get text() {
                        const { value: t, textContent: e } = this.copyableTarget;
                        return t || e;
                    }
                    forceReflow() {
                        this.element.offsetWidth;
                    }
                }
                (i.targets = ["copyable"]), (i.classes = ["success"]);
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return a;
                });
                var r = n(0);
                const i = document.documentElement.getAttribute("data-color-scheme"),
                    o = "rgb(35, 28, 51)",
                    s = "rgb(236, 233, 230)";
                class a extends t {
                    connect() {
                        this.installHostedFields();
                    }
                    disconnect() {
                        var t;
                        null == (t = this.hostedFields) || t.teardown();
                    }
                    async submit(t) {
                        t.stopPropagation(), t.preventDefault(), (this.buttonTarget.disabled = !0), this.buttonTarget.classList.add(this.submittingClass);
                        try {
                            const t = await this.hostedFields.tokenize();
                            (this.nonceTarget.value = t.nonce), this.element.submit();
                        } catch (e) {
                            (this.buttonTarget.disabled = !1), this.buttonTarget.classList.remove(this.submittingClass), this.hostedFieldsTokenizationFailedWithError(e);
                        }
                    }
                    updatePricing() {
                        this.hasPricingFrameTarget && (this.pricingFrameTarget.src = this.pricingURL);
                    }
                    set cardType(t) {
                        t ? this.element.setAttribute("data-card-type", t) : this.element.removeAttribute("data-card-type");
                    }
                    get pricingURL() {
                        const t = new URL(this.pricingUrlValue);
                        return t.searchParams.set("country", this.countryTarget.value.toLowerCase()), t;
                    }
                    async installHostedFields() {
                        const t = await n.e(0).then(n.t.bind(null, 87, 7)),
                            e = await t.client.create({ authorization: this.tokenizationKeyValue }),
                            r = { number: { container: this.numberTarget }, expirationDate: { container: this.expirationDateTarget }, cvv: { container: this.cvvTarget } },
                            a = { input: { "font-size": "18px", padding: "0 11px", color: "dark" == i ? s : o }, "@media (prefers-color-scheme: dark)": { input: { color: "light" == i ? o : s } } };
                        (this.hostedFields = await t.hostedFields.create({ client: e, fields: r, styles: a })),
                            this.hostedFields.on("cardTypeChange", this.hostedFieldsCardTypeChanged.bind(this)),
                            this.hostedFields.on("inputSubmitRequest", this.hostedFieldsInputSubmitRequested.bind(this)),
                            this.hostedFields.focus("number");
                    }
                    hostedFieldsCardTypeChanged(t) {
                        1 === t.cards.length ? (this.cardType = t.cards[0].type) : (this.cardType = null);
                    }
                    hostedFieldsInputSubmitRequested(t) {
                        this.buttonTarget.click();
                    }
                    hostedFieldsTokenizationFailedWithError(t) {
                        switch ((Object(r.l)(this.element), t.code)) {
                            case "HOSTED_FIELDS_FIELDS_EMPTY":
                                this.hostedFields.focus("number");
                                break;
                            case "HOSTED_FIELDS_FIELDS_INVALID":
                                this.hostedFieldsInvalid(t);
                                break;
                            case "HOSTED_FIELDS_FAILED_TOKENIZATION":
                                this.numberTarget.addClass(this.invalidFieldClass);
                        }
                    }
                    hostedFieldsInvalid(t) {
                        const { invalidFieldKeys: e, invalidFields: n } = t.details;
                        e.forEach((t) => {
                            n[t].classList.add(this.invalidFieldClass);
                        });
                    }
                }
                (a.targets = ["number", "expirationDate", "cvv", "nonce", "button", "country", "pricingFrame"]), (a.classes = ["invalidField", "submitting"]), (a.values = { tokenizationKey: String, pricingUrl: String });
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                var r = n(0);
                e.default = class extends t {
                    initialize() {
                        this.element.disabled = r.h;
                    }
                };
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                e.default = class extends t {
                    remove() {
                        this.element.remove();
                    }
                };
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return r;
                });
                class r extends t {
                    connect() {
                        this.hasContentTarget || this.markAsLoaded();
                    }
                    markAsLoading() {
                        this.isLoading || this.classList.add(this.loadingClass);
                    }
                    markAsLoaded() {
                        this.isLoading && this.classList.remove(this.loadingClass);
                    }
                    dispatchEntryAnchorMoveEvent() {
                        const t = { entryElement: this.element },
                            e = new CustomEvent("entry-anchor-move", { bubbles: !0, detail: t });
                        this.anchorTarget.dispatchEvent(e);
                    }
                    get isLoading() {
                        return this.classList.contains(this.loadingClass);
                    }
                    get classList() {
                        return this.element.classList;
                    }
                }
                (r.targets = ["anchor", "content"]), (r.classes = ["loading"]);
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                e.default = class extends t {
                    connect() {
                        this.synchronizeControlledTarget();
                    }
                    toggle() {
                        this.expanded = !this.expanded;
                    }
                    synchronizeControlledTarget() {
                        this.controlledTarget.hidden = !this.expanded;
                    }
                    get controlledTarget() {
                        return document.getElementById(this.element.getAttribute("aria-controls"));
                    }
                    set expanded(t) {
                        this.element.setAttribute("aria-expanded", t ? "true" : "false"), this.synchronizeControlledTarget();
                    }
                    get expanded() {
                        return "true" == this.element.getAttribute("aria-expanded");
                    }
                };
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return r;
                });
                class r extends t {
                    toggleFields({ target: t }) {
                        this.membershipFieldTargets.forEach((e) => {
                            e.disabled = e.dataset.type !== t.value;
                        });
                    }
                }
                r.targets = ["membershipField"];
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return r;
                });
                class r extends t {
                    update() {
                        this.filenameTarget.textContent = this.inputTarget.files[0].name;
                    }
                }
                r.targets = ["input", "filename"];
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return r;
                });
                class r extends t {
                    revealButton() {
                        this.element.classList.add(this.revealClass);
                    }
                }
                r.classes = ["reveal"];
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return r;
                });
                class r extends t {
                    connect() {
                        var t;
                        document.activeElement == document.body && (null == (t = this.currentAnchorTarget) || t.focus());
                    }
                    get currentAnchorTarget() {
                        if (location.hash.length > 0) {
                            const t = location.hash.slice(1);
                            return this.anchorTargets.find((e) => e.id == t);
                        }
                        return this.hasAnchorTarget ? this.anchorTarget : null;
                    }
                }
                r.targets = ["anchor"];
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return r;
                });
                class r extends t {
                    next(t) {
                        const e = this.itemTargets.findIndex((e) => e.contains(t.target)),
                            n = this.focusableTargets[e + 1] || this.focusableTargets[e - 1];
                        null == n || n.focus();
                    }
                }
                r.targets = ["item", "focusable"];
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return r;
                });
                n(57);
                class r extends t {
                    disconnect() {
                        this.enableSubmits(), this.element.toggleAttribute("data-submitting", !1);
                    }
                    submit(t) {
                        const e = t.target.form || t.target.closest("form");
                        e && e.requestSubmit();
                    }
                    submitByKeyboard(t) {
                        if (!("Enter" == t.key && (t.metaKey || t.ctrlKey))) return;
                        const e = t.target.form || t.target.closest("form");
                        e && (t.preventDefault(), e.requestSubmit());
                    }
                    preventSubmitByKeyboard(t) {
                        "Enter" == t.key && t.preventDefault();
                    }
                    cancelByKeyboard(t) {
                        "Escape" == t.key && this.hasCancelTarget && this.cancelTarget.click();
                    }
                    disableSubmitIfTrixAttachmentsUploading(t) {
                        const { hasTrixAttachmentsUploading: e } = this;
                        this.submitTargets.forEach((t) => (t.disabled = e));
                    }
                    validateForm(t) {
                        const e = this.firstIncompleteRequiredField;
                        (e || this.isSubmitDisabled || this.hasTrixAttachmentsUploading) && (null == e || e.focus(), t.preventDefault(), t.stopImmediatePropagation());
                    }
                    disableSubmits() {
                        this.submitTargets.forEach((t) => (t.disabled = !0));
                    }
                    enableSubmits() {
                        this.submitTargets.forEach((t) => (t.disabled = !1));
                    }
                    submitStart(t) {
                        const e = this.element;
                        e && (e.toggleAttribute("data-submitting", !0), this.disableSubmits());
                    }
                    get firstIncompleteRequiredField() {
                        return this.requiredInputTargets.find(i);
                    }
                    get isSubmitDisabled() {
                        return this.submitTargets.some((t) => t.disabled);
                    }
                    get hasTrixAttachmentsUploading() {
                        return this.trixAttachments.some((t) => t.isPending());
                    }
                    get trixAttachments() {
                        return this.trixElements.flatMap((t) => t.editor.getDocument().getAttachments());
                    }
                    get trixElements() {
                        return Array.from(this.element.querySelectorAll("trix-editor"));
                    }
                }
                function i(t) {
                    return 0 == (t.editor ? t.editor.getDocument() : t.value).toString().trim().length;
                }
                r.targets = ["cancel", "requiredInput", "submit"];
            }.call(this, n(1).default);
    },
    function (t, e) {
        !(function (t) {
            function e(t, e, n) {
                throw new t("Failed to execute 'requestSubmit' on 'HTMLFormElement': " + e + ".", n);
            }
            "function" != typeof t.requestSubmit &&
                (t.requestSubmit = function (t) {
                    t
                        ? (!(function (t, n) {
                            t instanceof HTMLElement || e(TypeError, "parameter 1 is not of type 'HTMLElement'"),
                                "submit" == t.type || e(TypeError, "The specified element is not a submit button"),
                                t.form == n || e(DOMException, "The specified element is not owned by this form element", "NotFoundError");
                        })(t, this),
                            t.click())
                        : (((t = document.createElement("input")).type = "submit"), (t.hidden = !0), this.appendChild(t), t.click(), this.removeChild(t));
                });
        })(HTMLFormElement.prototype);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return i;
                });
                var r = n(0);
                class i extends t {
                    connect() {
                        (this.gearIndex = 0), this.refresh();
                    }
                    disconnect() {
                        delete this.refreshRequest;
                    }
                    refresh() {
                        this.refreshRequest = Object(r.e)(this.currentRefreshValue).then(() => {
                            this.refreshableTargets.length && (this.gearIndex++, (this.element.src = this.srcValue), this.refresh());
                        });
                    }
                    get currentRefreshValue() {
                        return 1e3 * (this.gearsValues[this.gearIndex] || this.gearsValues.slice(-1)[0] || 60);
                    }
                }
                (i.targets = ["refreshable"]), (i.values = { src: String, gears: Array });
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return o;
                });
                var r = n(9),
                    i = n(0);
                class o extends t {
                    initialize() {
                        this.hasShortcutTarget && (this.shortcutTarget.innerHTML = this.shortcutTarget.textContent.replace("Control+", "^")), this.observeMutations(this.toggleWhenDisabled, this.element, { attributeFilter: ["disabled"] });
                    }
                    connect() {
                        this.disabled || Object(r.a)(this.element);
                    }
                    disconnect() {
                        Object(r.b)(this.element);
                    }
                    toggleWhenDisabled([t]) {
                        const e = t.target,
                            n = new Set((e.dataset.controller || "").split(/\s/));
                        e.disabled ? n.delete(this.identifier) : n.add(this.identifier), (e.dataset.controller = [...n].join(" "));
                    }
                    get disabled() {
                        return i.i || document.body.hasAttribute("data-hotkeys-disabled");
                    }
                }
                o.targets = ["shortcut"];
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                var r = n(0);
                e.default = class extends t {
                    initialize() {
                        this.url = this.urlWithParams;
                    }
                    get params() {
                        return new URLSearchParams(r.c.journal.params);
                    }
                    get urlWithParams() {
                        const { url: t } = this;
                        for (const [e, n] of this.params) t.searchParams.set(e, n);
                        return t;
                    }
                    get url() {
                        return new URL(this.element.href || this.element.action);
                    }
                    set url(t) {
                        this.element.href ? (this.element.href = t) : (this.element.action = t);
                    }
                };
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e);
        var r = n(8);
        e.default = class extends r.default { };
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return i;
                });
                var r = n(0);
                class i extends t {
                    initialize() {
                        this.performSearch = Object(r.d)(this.performSearch.bind(this), 300);
                    }
                    async performSearch({ target: { value: t } }) {
                        if (t.length) {
                            this.abortController = new AbortController();
                            const { signal: n } = this.abortController;
                            try {
                                this.resultsTarget.innerHTML = await r.k.get(this.buildSearchURL(t), { signal: n });
                            } catch (e) {
                                if ("AbortError" != e.name) throw e;
                            }
                        } else this.resetSearch();
                    }
                    cancelOnClose() {
                        this.element.open || this.resetSearch();
                    }
                    resetSearch() {
                        this.abortPendingSearch(), (this.resultsTarget.innerHTML = "");
                    }
                    buildSearchURL(t) {
                        const e = new URL(this.urlValue);
                        return e.searchParams.set("q", t), e;
                    }
                    abortPendingSearch() {
                        var t;
                        null == (t = this.abortController) || t.abort();
                    }
                }
                (i.targets = ["results"]), (i.values = { url: String });
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                e.default = class extends t {
                    connect() {
                        this.isLoading && this.show();
                    }
                    disconnect() {
                        this.hide();
                    }
                    show() {
                        this.toggle(!0);
                    }
                    hide() {
                        this.toggle(!1);
                    }
                    toggle(t) {
                        this.element.toggleAttribute("data-loading", t);
                    }
                    get isLoading() {
                        return !this.isLoaded;
                    }
                    get isLoaded() {
                        return this.element.complete;
                    }
                };
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return i;
                });
                var r = n(0);
                class i extends t {
                    async click() {
                        await Object(r.j)(), this.outletElement.click(), this.scrollToOutletElement();
                    }
                    async open() {
                        await Object(r.j)(), (this.outletElement.open = !0);
                    }
                    async toggle() {
                        const { open: t } = this.outletElement;
                        await Object(r.j)(), (this.outletElement.open = !t);
                    }
                    scrollToOutletElement() {
                        r.h || Object(r.l)(this.outletElement);
                    }
                    get outletElement() {
                        return document.getElementById(this.elementIdValue);
                    }
                }
                i.values = { elementId: String };
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                var r = n(0);
                e.default = class extends t {
                    adjustBottomMargin(t) {
                        const e = t.target,
                            { entryElement: n } = t.detail;
                        if (!r.h && e == document.activeElement) {
                            const { innerHeight: t } = window,
                                r = e.getBoundingClientRect().top,
                                i = n.getBoundingClientRect().bottom,
                                o = Math.max(0, t - (r + i));
                            this.element.style.marginBottom = o + "px";
                        }
                    }
                };
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return o;
                });
                var r = n(6),
                    i = n(0);
                class o extends t {
                    async connect() {
                        this.subscription = await Object(i.m)(this.channelValue, { received: r.a });
                    }
                    disconnect() {
                        var t;
                        null == (t = this.subscription) || t.unsubscribe();
                    }
                }
                o.values = { channel: Object };
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return i;
                });
                var r = n(0);
                class i extends t {
                    initialize() {
                        this.observeNextPageLink();
                    }
                    async loadNextPage(t) {
                        null == t || t.preventDefault();
                        const { nextPageLink: e } = this;
                        e.setAttribute("data-loading", "");
                        const n = await this.loadNextPageHTML();
                        await Object(r.j)(), (e.outerHTML = n), await Object(r.e)(500), this.observeNextPageLink();
                    }
                    async observeNextPageLink() {
                        if (this.manualLoadValue) return;
                        const { nextPageLink: t } = this;
                        var e;
                        t &&
                            (await ((e = t),
                                new Promise((t) => {
                                    new IntersectionObserver(([e], n) => {
                                        e.isIntersecting && (n.disconnect(), t());
                                    }).observe(e);
                                })),
                                this.loadNextPage());
                    }
                    async loadNextPageHTML() {
                        const t = await r.k.get(this.nextPageLink.href),
                            e = new DOMParser().parseFromString(t, "text/html").querySelector('[data-controller~="' + this.identifier + '"]');
                        return e ? e.innerHTML.trim() : "";
                    }
                    get nextPageLink() {
                        const t = this.nextPageLinkTargets;
                        return t.length > 1 && console.warn("Multiple next page links", t), t[t.length - 1];
                    }
                }
                (i.targets = ["nextPageLink"]), (i.values = { manualLoad: Boolean });
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return i;
                });
                var r = n(0);
                class i extends t {
                    initialize() {
                        this.hasLinkTarget && (this.linkTarget.hidden = !0);
                    }
                    connect() {
                        this.update();
                    }
                    disconnect() {
                        this.close();
                    }
                    async update() {
                        if (this.element.open) {
                            if ((this.hasLinkTarget && this.linkTarget.click(), this.frameElement)) {
                                if (this.frameElement.disabled) return void this.close();
                                await this.frameElement.loaded;
                            }
                            this.focusInitial();
                        }
                    }
                    updateSelectionWithKeyboard(t) {
                        if (this.element.open)
                            switch (t.key) {
                                case "ArrowUp":
                                    o(t), this.focusPrevious();
                                    break;
                                case "ArrowLeft":
                                    if (s(t.target)) break;
                                    o(t), this.focusPrevious();
                                    break;
                                case "ArrowDown":
                                    o(t), this.focusNext();
                                    break;
                                case "ArrowRight":
                                    if (s(t.target)) break;
                                    o(t), this.focusNext();
                                    break;
                                case "Escape":
                                    o(t), this.close(), this.summaryElement.focus();
                                    break;
                                case "Enter":
                                    if (t.target.getAttribute("data-popup-menu-adding-input")) break;
                                    this.activeItem || this.activateInitial();
                                    break;
                                case "Control":
                                    o(t), this.hasInputTarget && this.inputTarget.blur();
                            }
                    }
                    closeOnClickOutside({ target: t }) {
                        this.element.contains(t) || this.forceMenuOpen || this.close();
                    }
                    closeOnFocusOutside({ relatedTarget: t }) {
                        this.element.contains(t) || this.forceMenuOpen || this.close();
                    }
                    hideInAccessibilityTree() {
                        this.menuTarget.setAttribute("aria-hidden", "true");
                    }
                    showInAccessibilityTree() {
                        this.menuTarget.removeAttribute("aria-hidden");
                    }
                    toggleSummaryTabstop() {
                        const t = this.element.querySelector("summary");
                        this.element.open ? null == t || t.setAttribute("tabindex", -1) : null == t || t.removeAttribute("tabindex");
                    }
                    displayMenu() {
                        this.element.open = !0;
                    }
                    close() {
                        this.element.open = !1;
                    }
                    async activateInitial() {
                        var t;
                        await this.focusInitial(), await Object(r.j)(), null == (t = this.initialItem) || t.click();
                    }
                    async focusInitial() {
                        await Object(r.j)(), this.hasInputTarget ? this.inputTarget.focus() : this.hasItems && this.initialItem.focus();
                    }
                    async focusPrevious() {
                        var t;
                        await Object(r.j)(), null == (t = this.getItemInDirection(-1)) || t.focus();
                    }
                    async focusNext() {
                        var t;
                        await Object(r.j)(), null == (t = this.getItemInDirection(1)) || t.focus();
                    }
                    getItemInDirection(t) {
                        const { items: e } = this;
                        return e[e.indexOf(document.activeElement) + t];
                    }
                    get forceMenuOpen() {
                        return this.lockOnSelectionValue && (this.element.querySelector(":checked") || this.element.contains(document.activeElement));
                    }
                    get items() {
                        return this.itemTargets.filter(c);
                    }
                    get hasItems() {
                        return this.itemTargets.some(c);
                    }
                    get activeItem() {
                        return this.itemTargets.find(a);
                    }
                    get firstItem() {
                        return this.items[0];
                    }
                    get lastItem() {
                        return this.items.slice(-1)[0];
                    }
                    get initialItem() {
                        return this.firstItem;
                    }
                    get itemPlacement() {
                        return l(this.firstItem) < l(this.summaryElement) ? "above" : "below";
                    }
                    get summaryElement() {
                        return this.element.firstElementChild;
                    }
                    get frameElement() {
                        const t = this.hasLinkTarget && this.linkTarget.getAttribute("data-turbolinks-frame");
                        return t && document.getElementById(t);
                    }
                }
                function o(t) {
                    t.stopPropagation(), t.preventDefault();
                }
                function s(t) {
                    return "INPUT" == t.tagName;
                }
                function a(t) {
                    return t == document.activeElement;
                }
                function c(t) {
                    return null != t.offsetParent;
                }
                function l(t) {
                    return t.getBoundingClientRect().top;
                }
                (i.targets = ["item", "link", "input", "menu"]), (i.values = { lockOnSelection: Boolean });
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return i;
                });
                var r = n(0);
                class i extends t {
                    initialize() {
                        this.observeMutations(this.selectFirst);
                    }
                    filterSelectables() {
                        this.selectableTargets.forEach((t) => {
                            var e;
                            return (t.hidden = this.currentQuery && !(null == (e = t.dataset.name) ? void 0 : e.includes(this.currentQuery)));
                        }),
                            this.classList.toggle(this.filteredClass, this.currentQuery),
                            this.selectFirst();
                    }
                    filtering() {
                        this.classList.add(this.filteringClass);
                    }
                    doneFiltering() {
                        this.classList.remove(this.filteringClass);
                    }
                    adding() {
                        this.classList.add(this.addingClass);
                    }
                    cancel() {
                        this.inputTargets.forEach((t) => (t.value = "")), this.classList.remove(this.filteringClass, this.addingClass), this.filterSelectables();
                    }
                    cancelOnClose() {
                        this.element.open || r.h || this.cancel();
                    }
                    async selectFirst() {
                        var t;
                        await Object(r.j)(),
                            this.selectableTargets.forEach((t) => t.classList.remove(this.selectedClass)),
                            this.currentQuery &&
                            (null ==
                                (t = this.selectableTargets.find((t) => {
                                    var e;
                                    return null == (e = t.dataset.name) ? void 0 : e.includes(this.currentQuery);
                                })) ||
                                t.classList.add(this.selectedClass));
                    }
                    get currentQuery() {
                        var t;
                        return null == (t = this.inputTarget.value) ? void 0 : t.toLowerCase();
                    }
                }
                (i.targets = ["input", "selectable"]), (i.classes = ["filtering", "adding", "selected", "filtered"]);
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return r;
                });
                class r extends t {
                    toggleSelected(t) {
                        t.target.checked ? (this.labelTarget.innerHTML = this.labelValue) : (this.labelTarget.innerHTML = "");
                    }
                }
                (r.targets = ["label"]), (r.values = { label: String });
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return r;
                });
                class r extends t {
                    async initialize() {
                        await n.e(4).then(n.bind(null, 88)), this.classList.add(this.readyClass);
                    }
                    attachFiles({ target: t }) {
                        this.editor.insertFiles(t.files), (t.value = "");
                    }
                    toggleToolbar() {
                        this.classList.toggle(this.toolbarClass);
                    }
                    get editor() {
                        return this.editorElement.editor;
                    }
                    get editorElement() {
                        return this.element.querySelector("trix-editor");
                    }
                }
                r.classes = ["ready", "toolbar"];
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                var r = n(0);
                e.default = class extends t {
                    async connect() {
                        await Object(r.j)(), Object(r.l)(this.element);
                    }
                };
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return i;
                });
                var r = n(0);
                class i extends t {
                    initialize() {
                        this.attemptSearch = Object(r.d)(this.attemptSearch.bind(this), 300);
                    }
                    disconnect() {
                        this.reset();
                    }
                    selectedIndexValueChanged() {
                        this.addSelectionHighlight();
                    }
                    reset(t) {
                        (this.inputTarget.value = this.resultsTarget.innerHTML = ""),
                            this.classList.remove(this.showClass, this.emptyClass),
                            this.abortPendingSearchRequest(),
                            this.element.contains(null == t ? void 0 : t.target) && this.inputTarget.focus();
                    }
                    resetOnClickOutside(t) {
                        this.element.contains(t.target) || this.reset();
                    }
                    resetOnEscape(t) {
                        "Escape" == t.key && (this.reset(), this.element.focus());
                    }
                    updateSelectionWithKeyboard(t) {
                        switch (t.key) {
                            case "ArrowUp":
                                o(t), this.selectPrevious();
                                break;
                            case "ArrowDown":
                                o(t), this.selectNext();
                                break;
                            case "Enter":
                                this.activateSelection();
                        }
                    }
                    selectPrevious() {
                        this.selectedIndexValue > 0 && (this.removeSelectionHighlight(), this.selectedIndexValue--);
                    }
                    selectNext() {
                        this.selectedIndexValue < this.resultTargets.length - 1 && (this.removeSelectionHighlight(), this.selectedIndexValue++);
                    }
                    addSelectionHighlight() {
                        this.selectedResultTarget && (this.selectedResultTarget.classList.add(this.selectedClass), Object(r.l)(this.selectedResultTarget));
                    }
                    removeSelectionHighlight() {
                        var t;
                        null == (t = this.selectedResultTarget) || t.classList.remove(this.selectedClass);
                    }
                    activateSelection() {
                        var t;
                        null == (t = this.selectedResultTargetLink) || t.click();
                    }
                    abortPendingSearchRequest() {
                        var t;
                        null == (t = this.abortController) || t.abort();
                    }
                    attemptSearch() {
                        this.isQueryPresent ? this.performSearch() : this.reset();
                    }
                    async performSearch() {
                        this.abortPendingSearchRequest(), (this.abortController = new AbortController());
                        const { signal: t } = this.abortController;
                        try {
                            this.classList.add(this.busyClass), (this.resultsTarget.innerHTML = await r.k.get(this.searchURL, { signal: t })), this.openResults();
                        } catch (e) {
                            if ("AbortError" != e.name) throw e;
                            this.classList.remove(this.busyClass);
                        }
                    }
                    openResults() {
                        this.classList.remove(this.busyClass), this.classList.add(this.showClass), this.classList.toggle(this.emptyClass, 0 == this.resultTargets.length), (this.selectedIndexValue = 0), this.addSelectionHighlight();
                    }
                    get selectedResultTarget() {
                        return this.resultTargets[this.selectedIndexValue];
                    }
                    get selectedResultTargetLink() {
                        const t = this.selectedResultTarget;
                        return (null == t ? void 0 : t.matches("a[href]")) ? t : null == t ? void 0 : t.querySelector("a[href]");
                    }
                    get isQueryPresent() {
                        return this.searchQuery.length > 0;
                    }
                    get searchQuery() {
                        return this.inputTarget.value.trim();
                    }
                    get searchURL() {
                        const t = new URL(this.urlValue);
                        return t.searchParams.set("q", this.searchQuery), t;
                    }
                }
                function o(t) {
                    t.stopPropagation(), t.preventDefault();
                }
                (i.targets = ["input", "results", "result"]), (i.values = { url: String, selectedIndex: Number }), (i.classes = ["show", "selected", "busy", "empty"]);
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return i;
                });
                var r = n(0);
                class i extends t {
                    async connect() {
                        for (
                            await Object(r.j)(), this.containerWidth = this.element.clientWidth, this.maxFontSize = parseInt(window.getComputedStyle(this.element).getPropertyValue("font-size"), 10);
                            this.domainPartTarget.clientWidth > 0.6 * this.containerWidth;

                        )
                            this.element.style.fontSize = this.maxFontSize-- + "px";
                        (this.fontSize = Math.max(this.maxFontSize, 16)), this.resize();
                    }
                    async resize() {
                        await Object(r.j)(), this.updateReference(), this.setFontSize(), this.setInputSize();
                    }
                    updateReference() {
                        this.sizeReferenceTarget.textContent = this.localPartTarget.value;
                    }
                    setInputSize() {
                        (this.localPartTarget.style.maxWidth = this.maxWidth + 10 + "px"), (this.localPartTarget.style.width = this.sizeReferenceTarget.clientWidth + 10 + "px");
                    }
                    setFontSize() {
                        var t, e, n;
                        (this.fontSize = ((t = Math.floor((this.maxWidth * this.fontSize) / this.sizeReferenceTarget.clientWidth)), (e = 16), (n = this.maxFontSize), Math.min(Math.max(e, t), n))),
                            (this.element.style.fontSize = this.fontSize + "px");
                    }
                    get maxWidth() {
                        return Math.floor(this.containerWidth - this.domainPartTarget.clientWidth);
                    }
                }
                i.targets = ["localPart", "domainPart", "sizeReference"];
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                function n(t, e) {
                    return r(e) - r(t);
                }
                function r(t) {
                    return t.getAttribute("data-sort-code") || 0;
                }
                e.default = class extends t {
                    constructor(...t) {
                        super(...t), (this.append = (t) => this.element.append(t));
                    }
                    initialize() {
                        this.observeMutations(this.sortChildren);
                    }
                    sortChildren() {
                        const { children: t } = this;
                        (function ([t, ...e]) {
                            for (const r of e) {
                                if (n(t, r) > 0) return !1;
                                t = r;
                            }
                            return !0;
                        })(t) || t.sort(n).forEach(this.append);
                    }
                    get children() {
                        return Array.from(this.element.children);
                    }
                };
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return i;
                });
                var r = n(0);
                class i extends t {
                    indexValueChanged() {
                        var t;
                        "single" == this.displayValue ? this.showOnlyCurrentStep() : (this.showUptoCurrentStep(), this.scrollToCurrentStep()), null == (t = this.currentInputTarget) || t.focus();
                    }
                    next(t) {
                        this.currentStepTarget.contains(t.target) && this.indexValue++;
                    }
                    reset() {
                        this.indexValue = 0;
                    }
                    showOnlyCurrentStep() {
                        this.stepTargets.forEach((t) => {
                            t.classList.remove(this.showingClass);
                        }),
                            this.currentStepTarget.classList.add(this.showingClass);
                    }
                    showUptoCurrentStep() {
                        this.stepTargets.forEach((t, e) => {
                            t.classList.toggle(this.showingClass, e <= this.indexValue);
                        });
                    }
                    scrollToCurrentStep() {
                        this.indexValue > 0 && Object(r.l)(this.currentAnchorTarget);
                    }
                    get currentStepTarget() {
                        return this.stepTargets[this.indexValue];
                    }
                    get currentInputTarget() {
                        const { currentStepTarget: t } = this;
                        return this.inputTargets.find((e) => e.autofocus && t.contains(e));
                    }
                    get currentAnchorTarget() {
                        return this.anchorTargets[this.indexValue];
                    }
                }
                (i.targets = ["step", "input", "anchor"]), (i.values = { index: Number, display: String }), (i.classes = ["showing"]);
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return r;
                });
                class r extends t {
                    connect() {
                        this.appendTemplateContent();
                    }
                    append({ target: t }) {
                        this.lastInputTarget == t && this.canAppendMoreInputTargets && this.appendTemplateContent();
                    }
                    appendTemplateContent() {
                        const t = this.templateTarget.content.cloneNode(!0);
                        this.element.append(t);
                    }
                    get lastInputTarget() {
                        return this.inputTargets.slice(-1)[0];
                    }
                    get canAppendMoreInputTargets() {
                        return this.inputTargets.filter((t) => !t.value.length).length <= 1;
                    }
                }
                r.targets = ["template", "input"];
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return r;
                });
                class r extends t {
                    open() {
                        this.deleteTarget.open = !0;
                    }
                    close() {
                        this.deleteTarget.open = !1;
                    }
                    closeOnEscape(t) {
                        "Escape" == t.key && this.close();
                    }
                }
                r.targets = ["delete"];
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return r;
                });
                class r extends t {
                    open() {
                        (this.editTarget.open = !0), this.selectInput();
                    }
                    selectInput() {
                        this.editTarget.open && this.inputTarget.select();
                    }
                    close() {
                        (this.editTarget.open = !1), this.formTarget.reset();
                    }
                    closeOnEscape(t) {
                        "Escape" == t.key && this.close();
                    }
                }
                r.targets = ["edit", "input", "form"];
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return r;
                });
                class r extends t {
                    showAll() {
                        this.containerTarget.classList.add(this.revealedClass);
                    }
                    expandAll() {
                        this.containerTarget.classList.remove(this.compressedClass), this.containerTarget.classList.add(this.expandedClass);
                    }
                }
                (r.targets = ["container"]), (r.classes = ["revealed", "compressed", "expanded"]);
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return i;
                });
                var r = n(0);
                class i extends t {
                    async initialize() {
                        await Object(r.j)(), new IntersectionObserver(([t]) => this.classList.toggle(this.stuckClass, !t.isIntersecting), { rootMargin: "-" + this.topOffset + "px 0px 0px 0px" }).observe(this.element);
                    }
                    get topOffset() {
                        return this.element.getBoundingClientRect().top + window.scrollY;
                    }
                }
                i.classes = ["stuck"];
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return i;
                });
                var r = n(0);
                class i extends t {
                    async verify(t) {
                        t.preventDefault(), t.stopImmediatePropagation(), this.hideError();
                        try {
                            await r.k.post(t.target.action, { body: new FormData(t.target) }), this.continueButtonTarget.click();
                        } catch (e) {
                            this.showError();
                        }
                    }
                    showError() {
                        this.hasErrorTarget && (this.errorTarget.hidden = !1);
                    }
                    hideError() {
                        this.hasErrorTarget && (this.errorTarget.hidden = !0);
                    }
                    removeElements() {
                        this.removalTargets.forEach((t) => t.remove());
                    }
                }
                i.targets = ["error", "continueButton", "removal"];
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                n.d(e, "default", function () {
                    return o;
                });
                var r = n(7),
                    i = n(3);
                class o extends t {
                    connect() {
                        Object(r.c)() || this.handleUnsupportedBrowser();
                    }
                    getCredential() {
                        this.hideError(), this.disableForm(), this.requestChallengeAndVerify(r.b);
                    }
                    createCredential() {
                        this.hideError(), this.disableForm(), this.requestChallengeAndVerify(r.a);
                    }
                    handleUnsupportedBrowser() {
                        this.buttonTarget.parentNode.removeChild(this.buttonTarget), this.fallbackUrlValue ? window.location.replace(this.fallbackUrlValue) : this.supportTextTargets.forEach((t) => (t.hidden = !t.hidden));
                    }
                    async requestChallengeAndVerify(t) {
                        try {
                            const e = await this.requestPublicKeyChallenge(),
                                n = await t({ publicKey: e });
                            this.onCompletion(await this.verify(n));
                        } catch (e) {
                            this.onError(e);
                        }
                    }
                    async requestPublicKeyChallenge() {
                        return await this.request("get", this.challengeUrlValue);
                    }
                    async verify(t) {
                        return await this.request("post", this.verificationUrlValue, { body: JSON.stringify({ credential: t }), contentType: "application/json", responseKind: "json" });
                    }
                    onCompletion(t) {
                        window.location.replace(t.location);
                    }
                    onError(t) {
                        0 === t.code && "NotAllowedError" === t.name
                            ? (this.errorTarget.textContent = "That didn\u2019t work. Either it was cancelled or took too long. Please try again.")
                            : 11 === t.code && "InvalidStateError" === t.name
                                ? (this.errorTarget.textContent = "We couldn\u2019t add that security key. Please confirm you haven\u2019t already registered it, then try again.")
                                : (this.errorTarget.textContent = t.message),
                            this.showError();
                    }
                    hideError() {
                        this.hasErrorTarget && (this.errorTarget.hidden = !0);
                    }
                    showError() {
                        this.hasErrorTarget && ((this.errorTarget.hidden = !1), (this.buttonTarget.textContent = "Retry"), this.enableForm());
                    }
                    enableForm() {
                        this.element.classList.remove(this.loadingClass), (this.buttonTarget.disabled = !1);
                    }
                    disableForm() {
                        this.element.classList.add(this.loadingClass), (this.buttonTarget.disabled = !0);
                    }
                    async request(t, e, n) {
                        const r = new i.a(t, e, { responseKind: "json", ...n });
                        return (await r.perform()).json;
                    }
                }
                (o.targets = ["error", "button", "supportText"]), (o.values = { challengeUrl: String, verificationUrl: String, fallbackUrl: String }), (o.classes = ["loading"]);
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                var r = n(0);
                e.default = class extends t {
                    async connect() {
                        if (window.Notification)
                            switch (Notification.permission) {
                                case "denied":
                                    break;
                                case "granted":
                                    this.subscribe();
                                    break;
                                case "default":
                                    "granted" == (await Notification.requestPermission()) && this.subscribe();
                            }
                    }
                    deliver({ title: t, body: e, tag: n, url: r }) {
                        const i = new Notification(t, { body: e, tag: n });
                        (i.onclick = () => {
                            Turbolinks.visit(r), window.focus();
                        }),
                            setTimeout(() => {
                                i.close();
                            }, 15e3);
                    }
                    disconnect() {
                        var t;
                        null == (t = this.subscription) || t.unsubscribe();
                    }
                    async subscribe() {
                        this.subscription = await Object(r.m)("WebNotificationsChannel", { received: this.deliver });
                    }
                };
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        n.r(e),
            function (t) {
                e.default = class extends t {
                    print() {
                        window.print();
                    }
                    scrollToTop() {
                        window.scrollTo({ top: 0, behavior: "smooth" });
                    }
                    confirm(t) {
                        const { message: e } = t.target.closest("[data-message]").dataset;
                        window.confirm(e) || t.preventDefault();
                    }
                };
            }.call(this, n(1).default);
    },
    function (t, e, n) {
        "use strict";
        function r(t, e) {
            return i(t) && t.tagName.toLowerCase() == e;
        }
        function i(t) {
            return (null == t ? void 0 : t.nodeType) == Node.ELEMENT_NODE;
        }
        function o(t) {
            return r(t, "a");
        }
        function s(t) {
            return "cssRules" in t;
        }
        function a(t) {
            return !!i(t) && (r(t, "link") ? "stylesheet" == t.getAttribute("rel") : r(t, "style"));
        }
        function c(t) {
            return "cssRules" in t;
        }
        n.r(e);
        class l {
            constructor(t) {
                (this.element = void 0), (this.element = t);
            }
            dispatchEvent(t) {
                return this.element.dispatchEvent(t);
            }
            dispatchCustomEvent(t, e = { bubbles: !0 }) {
                return this.dispatchEvent(new CustomEvent(t, e));
            }
            get templateElement() {
                const { firstElementChild: t } = this.element;
                if (i((e = t)) && r(e, "template")) return t;
                throw new Error("A <message-content> element's first child must be a <template>");
                var e;
            }
            get stylesheetElement() {
                const t = this.element.getAttribute("stylesheet");
                if (t) {
                    const e = document.getElementById(t);
                    if (e && r(e, "link") && a(e)) return e;
                    throw new Error('Missing stylesheet element for <message-content stylesheet="' + t + ">");
                }
            }
        }
        class u {
            constructor(t, e) {
                (this.delegate = void 0),
                    (this.context = void 0),
                    (this.observer = void 0),
                    (this.intersect = (t) => {
                        const e = t.slice(-1)[0];
                        (null == e ? void 0 : e.isIntersecting) && this.delegate.frameAppeared();
                    }),
                    (this.delegate = t),
                    (this.context = e),
                    (this.observer = new IntersectionObserver(this.intersect));
            }
            connect() {
                this.observer.observe(this.element);
            }
            disconnect() {
                this.observer.unobserve(this.element);
            }
            get element() {
                return this.context.containerElement;
            }
        }
        class h {
            constructor(t, e, n) {
                (this.messageContentContext = void 0),
                    (this.containerElement = void 0),
                    (this.element = void 0),
                    (this.window = void 0),
                    (this.document = void 0),
                    (this.bodyElement = void 0),
                    (this.innerElement = void 0),
                    (this.messageContentContext = t),
                    (this.containerElement = e),
                    (this.element = n),
                    (this.window = this.element.contentWindow),
                    (this.document = this.element.contentDocument),
                    (this.bodyElement = this.document.body),
                    (this.innerElement = this.createInnerElement());
            }
            dispatchEvent(t) {
                return this.messageContentContext.dispatchEvent(t);
            }
            dispatchCustomEvent(t, e) {
                return this.messageContentContext.dispatchCustomEvent(t, e);
            }
            get documentElement() {
                return this.document.documentElement;
            }
            get messageContentElement() {
                return this.messageContentContext.element;
            }
            get templateElement() {
                return this.messageContentContext.templateElement;
            }
            get stylesheetElement() {
                return this.messageContentContext.stylesheetElement;
            }
            createInnerElement() {
                const t = this.document.createElement("div");
                return t.classList.add("message-content-inner"), t;
            }
        }
        function d(t) {
            for (const e of (function (t) {
                return Array.from(t.querySelectorAll("a[href]:not([download])")).filter(o);
            })(t))
                p(e), f(e);
        }
        function p(t) {
            (t.target = "_blank"), (t.rel = "noreferrer");
        }
        function f(t) {
            var e, n;
            const r = /^https?:/i,
                i = null != (e = null == (n = t.getAttribute("href")) ? void 0 : n.trim()) ? e : "";
            !r.test(i) && r.test(t.protocol) && t.setAttribute("href", i.replace(/^\/*/, "https://"));
        }
        function m(t) {
            const e = (function (t) {
                return (e = t.ownerDocument), e.defaultView;
                var e;
            })(t);
            return null == e ? void 0 : e.getComputedStyle(t);
        }
        function g(t, e = t.children) {
            for (const n of Array.from(e)) {
                if (v(n)) {
                    y(t, n) == t && w(n);
                }
                g(t, n.children);
            }
        }
        function y(t, e) {
            for (; e != t && e.parentElement && !b((e = e.parentElement)););
            return e;
        }
        function b(t) {
            return "static" != E(t);
        }
        function v(t) {
            const e = E(t);
            return "absolute" == e || "fixed" == e;
        }
        function w(t) {
            i(t) && t.style.setProperty("position", "static", "important");
        }
        function E(t) {
            var e;
            const n = m(t);
            return null != (e = null == n ? void 0 : n.position) ? e : "static";
        }
        function T(t) {
            return S(t.cssRules);
        }
        function S(t) {
            for (const e of Array.from(t)) c(e) ? S(e.cssRules) : "style" in e && C(e.style);
        }
        function C(t) {
            for (const e of Array.from(t))
                if ("font-size" == e || "line-height" == e) {
                    const n = t.getPropertyValue(e);
                    t.setProperty(e, x(n));
                }
        }
        function k(t) {
            const e = m(t);
            if (e) {
                const { fontSize: n } = e;
                (t.style.fontSize = x(n)), (t.style.lineHeight = "1.2"), t.removeAttribute("size");
            }
        }
        function x(t) {
            return (function (t) {
                return /(px|cm|mm|q|in|pc|pt)$/i.test(t);
            })(t)
                ? "calc(" + t + " / var(--type-scale-factor))"
                : t;
        }
        function O(t) {
            for (const e of (function (t) {
                return (function (t) {
                    let e,
                        n = [];
                    for (; (e = t.nextNode());) n.push(e);
                    return n;
                })(t.createNodeIterator(t.body, NodeFilter.SHOW_TEXT));
            })(t))
                A(e) && P(e);
        }
        const L = /[\0-\x08\x0E-\x1F!-\x84\x86-\x9F\xA1-\u167F\u1681-\u1FFF\u200B-\u2027\u202A-\u202E\u2030-\u205E\u2060-\u2FFF\u3001-\u{10FFFF}]{80}/u;
        function A(t) {
            var e;
            return (null != (e = t.textContent) ? e : "").match(L);
        }
        function P(t) {
            const e = t.parentElement;
            e && (e.style.wordBreak = "break-word");
        }
        function F(t) {
            d(t),
                (function (t) {
                    for (const e of Array.from(t.querySelectorAll("font[size]"))) k(e);
                })(t),
                (function (t) {
                    for (const n of Array.from(t.styleSheets).filter(s)) {
                        const t = () => T(n);
                        try {
                            t();
                        } catch {
                            var e;
                            null == (e = n.ownerNode) || e.addEventListener("load", t, { once: !0 });
                        }
                    }
                })(t),
                (function (t) {
                    for (const e of Array.from(t.querySelectorAll("[style]")).filter(i)) C(e.style);
                })(t),
                g(t.body),
                O(t);
        }
        class j {
            constructor(t, e) {
                (this.delegate = void 0),
                    (this.context = void 0),
                    (this.resourceLoaded = ({ target: t }) => {
                        this.delegate.bodyResourceLoaded();
                    }),
                    (this.detailsToggled = () => {
                        this.delegate.bodyResourceLoaded();
                    }),
                    (this.delegate = t),
                    (this.context = e),
                    this.render();
            }
            async render() {
                (this.bodyElement.innerHTML = ""), (this.innerElement.innerHTML = ""), this.innerElement.appendChild(this.cloneTemplateFragment()), this.bodyElement.appendChild(this.innerElement), await this.nextMicrotask, F(this.document);
            }
            connect() {
                this.documentElement.addEventListener("load", this.resourceLoaded, !0), this.documentElement.addEventListener("error", this.resourceLoaded, !0), this.documentElement.addEventListener("toggle", this.detailsToggled, !0);
            }
            disconnect() {
                this.documentElement.removeEventListener("load", this.resourceLoaded, !0),
                    this.documentElement.removeEventListener("error", this.resourceLoaded, !0),
                    this.documentElement.removeEventListener("toggle", this.detailsToggled, !0);
            }
            cloneTemplateFragment() {
                return this.document.importNode(this.templateFragment, !0);
            }
            get innerElement() {
                return this.context.innerElement;
            }
            get bodyElement() {
                return this.context.bodyElement;
            }
            get documentElement() {
                return this.context.documentElement;
            }
            get messageContentElement() {
                return this.context.messageContentElement;
            }
            get templateFragment() {
                return this.context.templateElement.content;
            }
            get document() {
                return this.context.document;
            }
            get nextMicrotask() {
                return Promise.resolve();
            }
        }
        class M {
            constructor(t, e) {
                (this.clientWidth = void 0), (this.scrollWidth = void 0), (this.clientWidth = t), (this.scrollWidth = e);
            }
            get clippedWidth() {
                return this.clientWidth - this.scrollWidth;
            }
            get scaleFactor() {
                return this.clientWidth / this.scrollWidth;
            }
        }
        function I(t, e) {
            var n = t;
            if (!n) return Promise.resolve(e());
            var r = n.ownerDocument.documentElement;
            var i = (function (t) {
                for (var e = []; t;) {
                    var n = t.getBoundingClientRect(),
                        r = n.top,
                        i = n.left;
                    e.push({ element: t, top: r, left: i }), (t = t.parentElement);
                }
                return e;
            })(n);
            return Promise.resolve(e()).then(function (t) {
                var e = (function (t) {
                    for (var e = 0; e < t.length; e++) {
                        var n = t[e];
                        if (r.contains(n.element)) return n;
                    }
                })(i);
                if (e) {
                    n = e.element;
                    var o = e.top,
                        s = e.left,
                        a = n.getBoundingClientRect(),
                        c = a.top,
                        l = a.left;
                    !(function (t, e, n) {
                        var r = t.ownerDocument,
                            i = r.defaultView;
                        function o(t) {
                            return t.offsetParent ? { top: t.scrollTop, left: t.scrollLeft } : { top: i.pageYOffset, left: i.pageXOffset };
                        }
                        function s(t, e, n) {
                            if (0 === e && 0 === n) return [0, 0];
                            var s = o(t),
                                a = s.top + n,
                                c = s.left + e;
                            t === r || t === i || t === r.documentElement || t === r.body ? r.defaultView.scrollTo(c, a) : ((t.scrollTop = a), (t.scrollLeft = c));
                            var l = o(t);
                            return [l.left - s.left, l.top - s.top];
                        }
                        function a(t) {
                            var e = t;
                            if (e.offsetParent && e !== r.body) {
                                for (; e !== r.body;) {
                                    if (!e.parentElement) return;
                                    e = e.parentElement;
                                    var n = i.getComputedStyle(e),
                                        o = n.position,
                                        s = n.overflowY,
                                        a = n.overflowX;
                                    if ("fixed" === o || "auto" === s || "auto" === a || "scroll" === s || "scroll" === a) break;
                                }
                                return e;
                            }
                        }
                        var c = a(t),
                            l = 0,
                            u = 0;
                        for (; c;) {
                            var h = s(c, e - l, n - u);
                            if (((l += h[0]), (u += h[1]), l === e && u === n)) break;
                            c = a(c);
                        }
                    })(n, l - s, c - o);
                }
                return t;
            });
        }
        function R(t) {
            if (t.activeElement !== t.body) return t.activeElement;
            var e = t.querySelectorAll(":hover"),
                n = e.length;
            return n ? e[n - 1] : void 0;
        }
        class B {
            constructor(t) {
                (this.context = void 0), (this.invalidated = !1), (this.measured = !1), (this.context = t);
            }
            async invalidate() {
                this.invalidated || ((this.invalidated = !0), await this.measure(), (this.invalidated = !1));
            }
            async measure() {
                await this.nextPaint, await this.performMeasurement(), (this.measured = !0);
            }
            async performMeasurement() {
                const t = this.containerElement.getBoundingClientRect();
                if (t.width > 0 || t.height > 0) {
                    const { containerStyles: e, frameStyles: n, documentElementStyles: r } = this;
                    await I(this.anchorElement, () => {
                        e.setProperty("height", t.height + "px"), n.setProperty("height", "0px"), r.setProperty("--clipped-width", "0"), r.setProperty("--scale-factor", "1"), r.setProperty("--type-scale-factor", "1");
                        const { scaleFactor: i } = this.geometry;
                        r.setProperty("--type-scale-factor", "" + i);
                        const { scaleFactor: o, clippedWidth: s } = this.geometry;
                        r.setProperty("--scale-factor", "" + o), r.setProperty("--clipped-width", s + "px");
                        const { height: a } = this.innerElement.getBoundingClientRect();
                        n.setProperty("height", "100%"), e.setProperty("height", a + "px"), this.dispatchMeasureEvent();
                    });
                } else this.dispatchMeasureEvent();
            }
            dispatchMeasureEvent() {
                this.context.dispatchCustomEvent("message-content-measure");
            }
            get nextPaint() {
                return new Promise((t) => requestAnimationFrame(t));
            }
            get anchorElement() {
                if (!this.measured) {
                    const t = document.getElementById(location.hash.slice(1));
                    if (t) {
                        const { top: e } = t.getBoundingClientRect();
                        if (e >= 0 && e < innerHeight) return t;
                    }
                }
                return R(document);
            }
            get containerElement() {
                return this.context.containerElement;
            }
            get frameElement() {
                return this.context.element;
            }
            get documentElement() {
                return this.context.documentElement;
            }
            get innerElement() {
                return this.context.innerElement;
            }
            get containerStyles() {
                return this.containerElement.style;
            }
            get frameStyles() {
                return this.frameElement.style;
            }
            get documentElementStyles() {
                return this.documentElement.style;
            }
            get geometry() {
                const { clientWidth: t, scrollWidth: e } = this.documentElement;
                return new M(t, e);
            }
        }
        class D {
            constructor(t) {
                (this.context = void 0),
                    (this.keyPressed = (t) => {
                        const e = (function (t) {
                            const { bubbles: e, cancelable: n, composed: r, key: i, code: o, location: s, ctrlKey: a, shiftKey: c, altKey: l, metaKey: u, repeat: h, isComposing: d } = t;
                            return new KeyboardEvent(t.type, { bubbles: e, cancelable: n, composed: r, key: i, code: o, location: s, ctrlKey: a, shiftKey: c, altKey: l, metaKey: u, repeat: h, isComposing: d });
                        })(t);
                        this.context.dispatchEvent(e), t.stopImmediatePropagation(), e.defaultPrevented && t.preventDefault();
                    }),
                    (this.context = t);
            }
            connect() {
                this.window.addEventListener("keydown", this.keyPressed, !0);
            }
            disconnect() {
                this.window.removeEventListener("keydown", this.keyPressed, !0);
            }
            get window() {
                return this.context.window;
            }
        }
        class N {
            constructor(t) {
                (this.context = void 0),
                    (this.selectionChanged = () => {
                        this.context.dispatchCustomEvent("message-content-selectionchange");
                    }),
                    (this.context = t);
            }
            connect() {
                this.document.addEventListener("selectionchange", this.selectionChanged);
            }
            disconnect() {
                this.document.removeEventListener("selectionchange", this.selectionChanged);
            }
            get document() {
                return this.context.document;
            }
            get messageContentElement() {
                return this.context.messageContentElement;
            }
        }
        class V {
            constructor(t) {
                (this.context = void 0), (this.context = t);
            }
            invalidate() {
                this.copyCustomProperties(), this.copyBaseStyles();
            }
            copyCustomProperties() {
                const { documentElementStyles: t, customProperties: e } = this;
                for (const [n, r] of e) t.setProperty(n, r);
            }
            copyBaseStyles() {
                const { documentElementStyles: t, computedStyles: e } = this;
                for (const n of _) {
                    const r = e.getPropertyValue(n);
                    t.setProperty(n, r);
                }
                C(t);
            }
            get documentElementStyles() {
                return this.documentElement.style;
            }
            get customProperties() {
                const { computedPropertyNames: t, computedStyles: e } = this;
                return t.filter((t) => t.startsWith("--")).map((t) => [t, e.getPropertyValue(t)]);
            }
            get computedPropertyNames() {
                const { computedStyleMap: t } = this;
                return t ? Array.from(t).map(([t]) => t) : Array.from(this.computedStyles);
            }
            get computedStyles() {
                return window.getComputedStyle(this.element);
            }
            get computedStyleMap() {
                var t, e;
                return null == (t = (e = this.element).computedStyleMap) ? void 0 : t.call(e);
            }
            get documentElement() {
                return this.document.documentElement;
            }
            get document() {
                return this.context.document;
            }
            get element() {
                return this.context.element;
            }
        }
        const _ = ["color", "font-family", "font-size", "line-height"];
        class q {
            constructor(t, e, n) {
                (this.context = void 0),
                    (this.appearanceController = void 0),
                    (this.bodyController = void 0),
                    (this.geometryController = void 0),
                    (this.keyboardController = void 0),
                    (this.selectionController = void 0),
                    (this.stylesController = void 0),
                    (this.context = new h(t, e, n)),
                    (this.appearanceController = new u(this, this.context)),
                    (this.bodyController = new j(this, this.context)),
                    (this.geometryController = new B(this.context)),
                    (this.keyboardController = new D(this.context)),
                    (this.selectionController = new N(this.context)),
                    (this.stylesController = new V(this.context)),
                    this.render();
            }
            connect() {
                this.appearanceController.connect(), this.bodyController.connect(), this.keyboardController.connect(), this.selectionController.connect();
            }
            disconnect() {
                this.appearanceController.disconnect(), this.bodyController.disconnect(), this.keyboardController.disconnect(), this.selectionController.disconnect();
            }
            render() {
                this.renderHeadContents(), this.stylesController.invalidate();
            }
            async invalidate() {
                await this.geometryController.invalidate(), this.stylesController.invalidate();
            }
            getSelection() {
                return this.document.getSelection();
            }
            renderHeadContents() {
                const { stylesheetURL: t } = this;
                this.headElement.innerHTML =
                    '\n      <meta charset="UTF-8">\n      <style>\n        html {\n          overflow: hidden;\n          overflow-wrap: break-word;\n        }\n        body {\n          margin: 0 var(--clipped-width) 0 0;\n          padding: 0;\n          transform: scale(var(--scale-factor));\n          transform-origin: left top;\n          display: flex;\n          position: relative;\n        }\n        .message-content-inner {\n          min-width: 0;\n          flex-grow: 1;\n          flex-shrink: 1;\n          height: max-content;\n        }\n        table {\n          font-size: medium;\n          line-height: 1.2;\n        }\n        img:not([width]):not([height]),\n        video:not([width]):not([height]) {\n          max-width: 100%;\n          height: auto;\n        }\n      </style>\n      ' +
                    (t ? '<style>@import url("' + t + '");</style>' : "") +
                    "\n    ";
            }
            frameAppeared() {
                this.invalidate();
            }
            bodyResourceLoaded() {
                this.invalidate();
            }
            get headElement() {
                return this.document.head;
            }
            get document() {
                return this.context.document;
            }
            get stylesheetURL() {
                var t;
                return null == (t = this.context.stylesheetElement) ? void 0 : t.href;
            }
        }
        class H {
            constructor(t) {
                (this.context = void 0),
                    (this.shadowRoot = void 0),
                    (this.containerElement = void 0),
                    (this.styleElement = void 0),
                    (this.frameElement = void 0),
                    (this.frameController = void 0),
                    (this.connected = !1),
                    (this.frameElementLoaded = () => {
                        (this.frameController = new q(this.context, this.containerElement, this.frameElement)), this.connected && this.frameController.connect();
                    }),
                    (this.context = t),
                    (this.shadowRoot = this.findOrCreateShadowRoot()),
                    (this.containerElement = this.findOrCreateContainerElement()),
                    (this.styleElement = this.findOrCreateStyleElement()),
                    (this.frameElement = this.findOrCreateFrameElement());
            }
            connect() {
                var t;
                (this.connected = !0), null == (t = this.frameController) || t.connect(), this.render(), this.waitForFrameToLoad();
            }
            disconnect() {
                var t;
                (this.connected = !1), null == (t = this.frameController) || t.disconnect();
            }
            invalidate() {
                var t;
                null == (t = this.frameController) || t.invalidate();
            }
            getSelection() {
                var t;
                return (null == (t = this.frameController) ? void 0 : t.getSelection()) || null;
            }
            findOrCreateShadowRoot() {
                var t;
                return null != (t = this.element.shadowRoot) ? t : this.element.attachShadow({ mode: "open" });
            }
            findOrCreateContainerElement() {
                var t;
                return null != (t = this.shadowRoot.querySelector("main")) ? t : document.createElement("main");
            }
            findOrCreateStyleElement() {
                var t;
                const e = null != (t = this.shadowRoot.querySelector("style")) ? t : document.createElement("style");
                return (
                    (e.textContent =
                        "\n      :host {\n        display: block;\n      }\n      main {\n        display: block;\n        overflow: hidden;\n        min-height: 1rem;\n      }\n      iframe {\n        border: none;\n        width: 100%;\n        height: 0;\n        margin: 0;\n      }\n    "),
                    e
                );
            }
            findOrCreateFrameElement() {
                var t;
                const e = null != (t = this.shadowRoot.querySelector("iframe")) ? t : document.createElement("iframe");
                return (e.src = "about:blank"), (e.title = "Message Content"), e;
            }
            render() {
                this.containerElement.appendChild(this.frameElement), this.shadowRoot.appendChild(this.styleElement), this.shadowRoot.appendChild(this.containerElement);
            }
            waitForFrameToLoad() {
                var t;
                "complete" == (null == (t = this.frameElement.contentDocument) ? void 0 : t.readyState) ? this.frameElementLoaded() : this.frameElement.addEventListener("load", this.frameElementLoaded, { once: !0 });
            }
            get element() {
                return this.context.element;
            }
            get frameDocument() {
                var t;
                return null == (t = this.frameController) ? void 0 : t.document;
            }
        }
        class z {
            constructor(t) {
                (this.context = void 0),
                    (this.shadowController = void 0),
                    (this.documentResourceLoaded = ({ target: t }) => {
                        a(t) && this.invalidate();
                    }),
                    (this.windowResized = () => {
                        this.invalidate();
                    }),
                    (this.context = new l(t));
            }
            connect() {
                (this.shadowController = new H(this.context)), this.shadowController.connect(), document.addEventListener("load", this.documentResourceLoaded, !0), window.addEventListener("resize", this.windowResized);
            }
            disconnect() {
                var t;
                null == (t = this.shadowController) || t.disconnect(), delete this.shadowController, document.removeEventListener("load", this.documentResourceLoaded, !0), window.removeEventListener("resize", this.windowResized);
            }
            invalidate() {
                var t;
                null == (t = this.shadowController) || t.invalidate();
            }
            get frameDocument() {
                var t;
                return null == (t = this.shadowController) ? void 0 : t.frameDocument;
            }
        }
        class U extends HTMLElement {
            constructor(...t) {
                super(...t), (this.controller = new z(this));
            }
            async connectedCallback() {
                await Promise.resolve(), this.controller.connect();
            }
            async disconnectedCallback() {
                await Promise.resolve(), this.controller.disconnect();
            }
            get document() {
                return this.controller.frameDocument;
            }
        }
        customElements.define("message-content", U);
        n(21);
        var W = n(4),
            K = n(0);
        class J extends HTMLElement {
            static get observedAttributes() {
                return ["src"];
            }
            constructor() {
                super(), (this.controller = void 0), (this.controller = new Y(this));
            }
            connectedCallback() {
                this.controller.connect();
            }
            disconnectedCallback() {
                this.controller.disconnect();
            }
            attributeChangedCallback() {
                if (this.src && this.isActive) {
                    const t = this.controller.visit(this.src);
                    Object.defineProperty(this, "loaded", { value: t, configurable: !0 });
                }
            }
            formSubmissionIntercepted(t) {
                this.controller.formSubmissionIntercepted(t);
            }
            get src() {
                return this.getAttribute("src");
            }
            set src(t) {
                t ? this.setAttribute("src", t) : this.removeAttribute("src");
            }
            get loaded() {
                return Promise.resolve(void 0);
            }
            get disabled() {
                return this.hasAttribute("disabled");
            }
            set disabled(t) {
                t ? this.setAttribute("disabled", "") : this.removeAttribute("disabled");
            }
            get autoscroll() {
                return this.hasAttribute("autoscroll");
            }
            set autoscroll(t) {
                t ? this.setAttribute("autoscroll", "") : this.removeAttribute("autoscroll");
            }
            get isActive() {
                return this.ownerDocument === document && !this.isPreview;
            }
            get isPreview() {
                var t, e;
                return null == (t = this.ownerDocument) || null == (e = t.documentElement) ? void 0 : e.hasAttribute("data-turbolinks-preview");
            }
        }
        class Y {
            constructor(t) {
                (this.element = void 0),
                    (this.linkInterceptor = void 0),
                    (this.formInterceptor = void 0),
                    (this.formSubmission = void 0),
                    (this.resolveVisitPromise = () => { }),
                    (this.element = t),
                    (this.linkInterceptor = new X(this, this.element)),
                    (this.formInterceptor = new Z(this, this.element));
            }
            connect() {
                this.linkInterceptor.start(), this.formInterceptor.start();
            }
            disconnect() {
                this.linkInterceptor.stop(), this.formInterceptor.stop();
            }
            shouldInterceptLinkClick(t, e) {
                return this.shouldInterceptNavigation(t);
            }
            linkClickIntercepted(t, e) {
                this.findFrameElement(t).src = e;
            }
            shouldInterceptFormSubmission(t) {
                return this.shouldInterceptNavigation(t);
            }
            formSubmissionIntercepted(t) {
                this.formSubmission && this.formSubmission.stop(), (this.formSubmission = new W.FormSubmission(this, t)), this.formSubmission.start();
            }
            async visit(t) {
                const e = W.Location.wrap(t),
                    n = new W.FetchRequest(this, W.FetchMethod.get, e);
                return new Promise((t) => {
                    (this.resolveVisitPromise = () => {
                        (this.resolveVisitPromise = () => { }), t();
                    }),
                        n.perform();
                });
            }
            additionalHeadersForRequest(t) {
                return { "X-Turbolinks-Frame": this.id };
            }
            requestStarted(t) {
                this.element.setAttribute("busy", "");
            }
            requestPreventedHandlingResponse(t, e) {
                this.resolveVisitPromise();
            }
            async requestSucceededWithResponse(t, e) {
                await this.loadResponse(e), this.resolveVisitPromise();
            }
            requestFailedWithResponse(t, e) {
                console.error(e), this.resolveVisitPromise();
            }
            requestErrored(t, e) {
                console.error(e), this.resolveVisitPromise();
            }
            requestFinished(t) {
                this.element.removeAttribute("busy");
            }
            formSubmissionStarted(t) { }
            formSubmissionSucceededWithResponse(t, e) {
                this.findFrameElement(t.formElement).controller.loadResponse(e);
            }
            formSubmissionFailedWithResponse(t, e) { }
            formSubmissionErrored(t, e) { }
            formSubmissionFinished(t) { }
            findFrameElement(t) {
                var e;
                return null != (e = Q(t.getAttribute("data-turbolinks-frame"))) ? e : this.element;
            }
            async loadResponse(t) {
                const e = (function (t = "") {
                    return document.implementation.createHTMLDocument().createRange().createContextualFragment(t);
                })(await t.responseHTML),
                    n = await this.extractForeignFrameElement(e);
                n && (await Object(K.j)(), this.loadFrameElement(n), this.scrollFrameIntoView(n), await Object(K.j)(), this.focusFirstAutofocusableElement());
            }
            async extractForeignFrameElement(t) {
                let e;
                const n = CSS.escape(this.id);
                return (e = $(t.querySelector("turbolinks-frame#" + n))) ? e : (e = $(t.querySelector("turbolinks-frame[src][recurse~=" + n + "]"))) ? (await e.loaded, await this.extractForeignFrameElement(e)) : void 0;
            }
            loadFrameElement(t) {
                var e;
                const n = document.createRange();
                n.selectNodeContents(this.element), n.deleteContents();
                const r = null == (e = t.ownerDocument) ? void 0 : e.createRange();
                r && (r.selectNodeContents(t), this.element.appendChild(r.extractContents()));
            }
            focusFirstAutofocusableElement() {
                const t = this.firstAutofocusableElement;
                return !!t && (t.focus(), !0);
            }
            scrollFrameIntoView(t) {
                if (this.element.autoscroll || t.autoscroll) {
                    const t = this.element.firstElementChild,
                        r = ((e = this.element.getAttribute("data-autoscroll-block")), (n = "end"), "end" == e || "start" == e || "center" == e || "nearest" == e ? e : n);
                    if (t) return Object(K.l)(t, { block: r }), !0;
                }
                var e, n;
                return !1;
            }
            shouldInterceptNavigation(t) {
                const e = t.getAttribute("data-turbolinks-frame") || this.element.getAttribute("links-target");
                if (!this.enabled || "top" == e) return !1;
                if (e) {
                    const t = Q(e);
                    if (t) return !t.disabled;
                }
                return !0;
            }
            get firstAutofocusableElement() {
                const t = this.element.querySelector("[autofocus]");
                return t instanceof HTMLElement ? t : null;
            }
            get id() {
                return this.element.id;
            }
            get enabled() {
                return !this.element.disabled;
            }
        }
        function Q(t) {
            if (null != t) {
                const e = document.getElementById(t);
                if (e instanceof J) return e;
            }
        }
        function $(t) {
            if ((t && t.ownerDocument !== document && (t = document.importNode(t, !0)), t instanceof J)) return t;
        }
        class X {
            constructor(t, e) {
                (this.delegate = void 0),
                    (this.element = void 0),
                    (this.clickEvent = void 0),
                    (this.clickBubbled = (t) => {
                        this.respondsToEventTarget(t.target) ? (this.clickEvent = t) : delete this.clickEvent;
                    }),
                    (this.linkClicked = (t) => {
                        this.clickEvent &&
                            this.respondsToEventTarget(t.target) &&
                            this.delegate.shouldInterceptLinkClick(t.target, t.data.url) &&
                            (this.clickEvent.preventDefault(), t.preventDefault(), this.delegate.linkClickIntercepted(t.target, t.data.url)),
                            delete this.clickEvent;
                    }),
                    (this.willVisit = () => {
                        delete this.clickEvent;
                    }),
                    (this.delegate = t),
                    (this.element = e);
            }
            start() {
                this.element.addEventListener("click", this.clickBubbled), document.addEventListener("turbolinks:click", this.linkClicked), document.addEventListener("turbolinks:before-visit", this.willVisit);
            }
            stop() {
                this.element.removeEventListener("click", this.clickBubbled), document.removeEventListener("turbolinks:click", this.linkClicked), document.removeEventListener("turbolinks:before-visit", this.willVisit);
            }
            respondsToEventTarget(t) {
                const e = t instanceof Element ? t : t instanceof Node ? t.parentElement : null;
                return e && e.closest("turbolinks-frame, html") == this.element;
            }
        }
        class Z {
            constructor(t, e) {
                (this.delegate = void 0),
                    (this.element = void 0),
                    (this.submitBubbled = (t) => {
                        if (t.target instanceof HTMLFormElement) {
                            const e = t.target;
                            this.delegate.shouldInterceptFormSubmission(e) && (t.preventDefault(), t.stopImmediatePropagation(), this.delegate.formSubmissionIntercepted(e));
                        }
                    }),
                    (this.delegate = t),
                    (this.element = e);
            }
            start() {
                this.element.addEventListener("submit", this.submitBubbled);
            }
            stop() {
                this.element.removeEventListener("submit", this.submitBubbled);
            }
        }
        customElements.define("turbolinks-frame", J),
            new (class {
                constructor(t) {
                    (this.element = void 0), (this.linkInterceptor = void 0), (this.formInterceptor = void 0), (this.element = t), (this.linkInterceptor = new X(this, t)), (this.formInterceptor = new Z(this, t));
                }
                start() {
                    this.linkInterceptor.start(), this.formInterceptor.start();
                }
                stop() {
                    this.linkInterceptor.stop(), this.formInterceptor.stop();
                }
                shouldInterceptLinkClick(t, e) {
                    return this.shouldRedirect(t);
                }
                linkClickIntercepted(t, e) {
                    const n = this.findFrameElement(t);
                    n && (n.src = e);
                }
                shouldInterceptFormSubmission(t) {
                    return this.shouldRedirect(t);
                }
                formSubmissionIntercepted(t) {
                    const e = this.findFrameElement(t);
                    e && e.formSubmissionIntercepted(t);
                }
                shouldRedirect(t) {
                    const e = this.findFrameElement(t);
                    return !!e && e != t.closest("turbolinks-frame");
                }
                findFrameElement(t) {
                    const e = t.getAttribute("data-turbolinks-frame");
                    if (e && "top" != e) {
                        const t = this.element.querySelector("#" + e + ":not([disabled])");
                        if (t instanceof J) return t;
                    }
                }
            })(document.documentElement).start();
    },
]);
//# sourceMappingURL=main-946777828d753a91518c.js.map
